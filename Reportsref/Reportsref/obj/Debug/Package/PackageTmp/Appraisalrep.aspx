﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Reportsref._Default" %>

<%@ Register assembly="DevExpress.XtraReports.v11.2.Web, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>

<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.2, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridLookup" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.2, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <p>
        <table class="style1">
            <tr>
                <td width="500">
                    <dx:ASPxLabel ID="ASPxLabel10" runat="server" style="font-weight: 700" 
                        Text="Performance Appraisal Summary Report Options">
                    </dx:ASPxLabel>
                </td>
                <td width="600" style="width: 300px">
                    &nbsp;</td>
                <td>
                    <dx:ASPxLabel ID="ASPxLabel9" runat="server" style="font-weight: 700" 
                        Text="Marks Options">
                    </dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td width="600" colspan="2">
                    <table width="500">
                        <tr>
                            <td style="border: thin none #008080">
                                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Branch">
                                </dx:ASPxLabel>
                            </td>
                            <td style="border: thin none #008080">
                                <dx:ASPxGridLookup ID="lkbranchcode" runat="server" AutoGenerateColumns="False" 
                                    AutoResizeWithContainer="True" DataSourceID="Branchcodesource" 
                                    DisplayFormatString="{0}" HorizontalAlign="Left" KeyFieldName="cbrncode" 
                                    ondatabound="lkbranchcode_DataBound" style="text-align: left" 
                                    TextFormatString="{0}" Width="350px" AutoPostBack="True">
<GridViewProperties>
<SettingsBehavior AllowFocusedRow="True" AllowSelectSingleRowOnly="True"></SettingsBehavior>
</GridViewProperties>
                                    <Columns>
                                        <dx:GridViewDataTextColumn FieldName="Column1" ReadOnly="True" VisibleIndex="0">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="cbrncode" ReadOnly="True" Visible="False" 
                                            VisibleIndex="1">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:ASPxGridLookup>
                                <asp:SqlDataSource ID="Branchcodesource" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:MyXtraReport %>" 
                                    SelectCommand="SELECT REPLACE([cbrndesc], ' ', ''), [cbrncode] FROM [cbranch]">
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                        <tr>
                            <td style="border: thin none #008080">
                                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Appraisal Period">
                                </dx:ASPxLabel>
                            </td>
                            <td style="border: thin none #008080">
                                <dx:ASPxGridLookup ID="lkappraisalperiod" runat="server" 
                                    AutoGenerateColumns="False" DataSourceID="AppraisalPeriodSource" 
                                    KeyFieldName="cprdperiodpe" 
                                    TextFormatString="{0}" Width="350px" AutoPostBack="True" OnDataBound="lkappraisalperiod_DataBound">
<GridViewProperties>
<SettingsBehavior AllowFocusedRow="True" AllowSelectSingleRowOnly="True"></SettingsBehavior>
</GridViewProperties>
                                    <Columns>
                                        <dx:GridViewDataTextColumn FieldName="cprdperiodpe" ReadOnly="True" 
                                            Visible="False" VisibleIndex="0">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="cprdcode" VisibleIndex="1">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="cprddesc" VisibleIndex="2">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn FieldName="dprdstart" VisibleIndex="3">
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataDateColumn FieldName="dprdend" VisibleIndex="4">
                                        </dx:GridViewDataDateColumn>
                                    </Columns>
                                </dx:ASPxGridLookup>
                                <asp:SqlDataSource ID="AppraisalPeriodSource" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:MyXtraReport %>" 
                                    SelectCommand="SELECT [cprdperiodpe],[cprdcode], [cprddesc], [dprdstart], [dprdend]  FROM [cprdpe]" ProviderName="<%$ ConnectionStrings:MyXtraReport.ProviderName %>">
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                        <tr>
                            <td style="border: thin none #008080">
                                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Mark Type">
                                </dx:ASPxLabel>
                            </td>
                            <td style="border: thin none #008080">
                                <dx:ASPxComboBox ID="cbmarktyp" runat="server" SelectedIndex="0" Width="350px">
                                    <Items>
                                        <dx:ListEditItem Selected="True" Text="System Calculated Marks" Value="1" />
                                        <dx:ListEditItem Text="Moderated Marks by Branch" Value="2" />
                                        <dx:ListEditItem Text="Moderated Marks by University" Value="3" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="border: thin none #008080">
                                <dx:ASPxLabel ID="ASPxLabel12" runat="server" Text="Analysis Date">
                                </dx:ASPxLabel>
                            </td>
                            <td style="border: thin none #008080">
                                <dx:ASPxDateEdit ID="dtAnalysis" runat="server" Date="01/10/2014 10:22:26" 
                                    DisplayFormatString="dd/MM/yyyy" EditFormatString="dd/MM/yyyy">
                                </dx:ASPxDateEdit>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table width="300">
                        <tr>
                            <td>
                                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="OUTSTANDING">
                                </dx:ASPxLabel>
                            </td>
                            <td>
                                <dx:ASPxSpinEdit ID="spnlvl1" runat="server" Height="21px" Number="90" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="GOOD">
                                </dx:ASPxLabel>
                            </td>
                            <td>
                                <dx:ASPxSpinEdit ID="spnlvl2" runat="server" Height="21px" Number="80" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="SATISFACTORY">
                                </dx:ASPxLabel>
                            </td>
                            <td>
                                <dx:ASPxSpinEdit ID="spnlvl3" runat="server" Height="21px" Number="70" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="UNSATISFACTORY">
                                </dx:ASPxLabel>
                            </td>
                            <td>
                                <dx:ASPxSpinEdit ID="spnlvl4" runat="server" Height="21px" Number="50" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="POOR">
                                </dx:ASPxLabel>
                            </td>
                            <td>
                                <dx:ASPxSpinEdit ID="spnlvl5" runat="server" Height="21px" Number="0" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </p>
    <p>
        <table width="500">
            <tr>
                <td>
                    <dx:ASPxLabel ID="ASPxLabel11" runat="server" Text="Reports Selection">
                    </dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxComboBox ID="rptselect" runat="server" SelectedIndex="0" Width="350px">
                        <Items>
                            <dx:ListEditItem Selected="True" Text="Perfromance Appraisal Statistic Charts" 
                                Value="frmrptPerformanceAppraisalStatisticCharts.aspx" />
                            <dx:ListEditItem Text="Performance Appraisal Statistic" 
                                Value="frmrptPerformanceAppraisalStatistic.aspx" />
                            <dx:ListEditItem Text="Performance Appraisal Summary" 
                                Value="frmrptAppraisalSummaryEvaluation.aspx" />
                            <dx:ListEditItem Text="Performance Appraisal Summary List" 
                                Value="frmrptAppraisalSummaryofEvaluation.aspx" />
                            <dx:ListEditItem Text="Performance Appraisal" 
                                Value="frmrptPerformanceAppraisal.aspx" />
                        </Items>
                    </dx:ASPxComboBox>
                </td>
            </tr>
        </table>
    </p>
    <p>
        <dx:ASPxButton ID="ASPxButton1" runat="server" onclick="ASPxButton1_Click" 
            Text="Preview Report" Width="167px">
        </dx:ASPxButton>
    </p>
    </form>
</body>
</html>
