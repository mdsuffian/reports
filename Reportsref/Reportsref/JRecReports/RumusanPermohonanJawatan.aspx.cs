﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReportsLibrary;

namespace Reportsref.JRecReports
{
    public partial class RumusanPermohonanJawatan : System.Web.UI.Page
    {

        public static string refid = "0000000146";
        public static string essdb = "LHDN";

        protected void Page_Load(object sender, EventArgs e)
        {
            HttpCookie ResumeIdcookie = Request.Cookies["Resume"];

                       

            // Read the cookie information and display it.
            if (ResumeIdcookie != null )
            {
                refid = ResumeIdcookie.Values["Id"].ToString();
                essdb = ResumeIdcookie.Values["EssServer"].ToString();
            }

            Rptcvrep newrep = new Rptcvrep(essdb, refid);
            myrepviewer.Report = newrep;
           
        }
    }
}