﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReportsLibrary;

using System.Net;
using System.Text;
using System.IO;

namespace Reportsref.JRecReports
{
    public partial class cvrep : System.Web.UI.Page
    {

        public static string refid;// = "0000000146";
        public static string essdb ;//= "LHDN";

        protected void Page_Load(object sender, EventArgs e)
        {



            refid = Request.QueryString["Id"] == null ? "" : Request.QueryString["Id"].ToString();
            essdb = Request.QueryString["EssServer"] == null ? "" : Request.QueryString["EssServer"].ToString();



            //HttpCookie ResumeIdcookie = Request.Cookies["Resume"];



            // Read the cookie information and display it.
            //if (ResumeIdcookie != null )
            //{
            //refid = ResumeIdcookie.Values["Id"].ToString();
            //essdb = ResumeIdcookie.Values["EssServer"].ToString();
            //}

            Rptcvrep newrep = new Rptcvrep(essdb, refid);
            //myrepviewer.Report = newrep;




            string fname = AppDomain.CurrentDomain.BaseDirectory + Guid.NewGuid().ToString() + ".pdf";

            newrep.ExportToPdf(fname);

            if (File.Exists(fname))
            {

                byte[] buffer;

                using (FileStream fileStream = new FileStream(fname, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite, 512, FileOptions.DeleteOnClose))
                using (BinaryReader reader = new BinaryReader(fileStream))
                {
                    buffer = reader.ReadBytes((int)reader.BaseStream.Length);
                }


                Context.Response.ContentType = "application/pdf";
                Context.Response.AddHeader("Content-Length", buffer.Length.ToString());
                Context.Response.BinaryWrite(buffer);
                Context.Response.End();


            }


        }
    }
}