﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReportsLibrary;

namespace Reportsref
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }



        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect(@"/AppraisalRpt/reports/" + rptselect.Value.ToString() + "?BranchCd=" + lkbranchcode.Value.ToString() +
                "&Apprprd="+lkappraisalperiod.Value.ToString()+
                "&MarkTyp="+cbmarktyp.Value.ToString()+
                "&Lvl1="+spnlvl1.Value.ToString()+
                "&Lvl2="+spnlvl2.Value.ToString()+
                "&Lvl3="+spnlvl3.Value.ToString()+
                "&Lvl4="+spnlvl4.Value.ToString()+
                "&Lvl5="+spnlvl5.Value.ToString()+
                "&DTAnalyse="+dtAnalysis.Value.ToString()
            );
        }

        protected void lkbranchcode_DataBound(object sender, EventArgs e)
        {
            if (lkbranchcode.GridView.VisibleRowCount > 0)
            {
                if (lkbranchcode.Text == "")
                {
                    lkbranchcode.GridView.Selection.SelectRow(0);
                }
            }
        }

        protected void lkappraisalperiod_DataBound(object sender, EventArgs e)
        {
            if (lkappraisalperiod.GridView.VisibleRowCount > 0)
            {
                if (lkappraisalperiod.Text == "")
                {
                    lkappraisalperiod.GridView.Selection.SelectRow(0);
                }
            }
        }

    }
}