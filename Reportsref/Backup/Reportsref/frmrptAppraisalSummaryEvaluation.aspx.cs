﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReportsLibrary;

namespace Reportsref
{
    public partial class frmrptAppraisalSummaryEvaluation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string BranchCd = Request.QueryString["BranchCd"] == null ? "" : Request.QueryString["BranchCd"].ToString();
            string Apprprd = Request.QueryString["Apprprd"] == null ? "" : Request.QueryString["Apprprd"].ToString();
            string MarkTyp = Request.QueryString["MarkTyp"] == null ? "" : Request.QueryString["MarkTyp"].ToString();

            int Lvl1 = Request.QueryString["Lvl1"] == null ? 0 : Convert.ToInt32(Request.QueryString["Lvl1"].ToString());
            int Lvl2 = Request.QueryString["Lvl2"] == null ? 0 : Convert.ToInt32(Request.QueryString["Lvl2"].ToString());
            int Lvl3 = Request.QueryString["Lvl3"] == null ? 0 : Convert.ToInt32(Request.QueryString["Lvl3"].ToString());
            int Lvl4 = Request.QueryString["Lvl4"] == null ? 0 : Convert.ToInt32(Request.QueryString["Lvl4"].ToString());
            int Lvl5 = Request.QueryString["Lvl5"] == null ? 0 : Convert.ToInt32(Request.QueryString["Lvl5"].ToString());


            RptAppraisalSummaryEvaluation newrep = new RptAppraisalSummaryEvaluation(BranchCd , Apprprd , MarkTyp);
            ReportViewer1.Report = newrep;
        }
    }
}