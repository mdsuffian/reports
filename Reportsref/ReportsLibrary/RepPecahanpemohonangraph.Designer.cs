﻿namespace ReportsLibrary
{
    partial class RepPecahanpemohonangraph
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RepPecahanpemohonangraph));
            DevExpress.XtraCharts.SwiftPlotDiagram swiftPlotDiagram2 = new DevExpress.XtraCharts.SwiftPlotDiagram();
            DevExpress.XtraCharts.Series series5 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SeriesPoint seriesPoint25 = new DevExpress.XtraCharts.SeriesPoint("18-25", new object[] {
            ((object)(2D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint26 = new DevExpress.XtraCharts.SeriesPoint("26-30", new object[] {
            ((object)(3D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint27 = new DevExpress.XtraCharts.SeriesPoint("31-35", new object[] {
            ((object)(4D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint28 = new DevExpress.XtraCharts.SeriesPoint("36-40", new object[] {
            ((object)(9D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint29 = new DevExpress.XtraCharts.SeriesPoint("41-45", new object[] {
            ((object)(1D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint30 = new DevExpress.XtraCharts.SeriesPoint("46-50", new object[] {
            ((object)(5D))});
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView6 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.Series series6 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SeriesPoint seriesPoint31 = new DevExpress.XtraCharts.SeriesPoint("18-25", new object[] {
            ((object)(6D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint32 = new DevExpress.XtraCharts.SeriesPoint("26-30", new object[] {
            ((object)(9D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint33 = new DevExpress.XtraCharts.SeriesPoint("31-35", new object[] {
            ((object)(8D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint34 = new DevExpress.XtraCharts.SeriesPoint("36-40", new object[] {
            ((object)(7D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint35 = new DevExpress.XtraCharts.SeriesPoint("41-45", new object[] {
            ((object)(5D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint36 = new DevExpress.XtraCharts.SeriesPoint("46-50", new object[] {
            ((object)(3D))});
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView7 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.Series series7 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SeriesPoint seriesPoint37 = new DevExpress.XtraCharts.SeriesPoint("18-25", new object[] {
            ((object)(2D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint38 = new DevExpress.XtraCharts.SeriesPoint("26-30", new object[] {
            ((object)(5D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint39 = new DevExpress.XtraCharts.SeriesPoint("31-35", new object[] {
            ((object)(2D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint40 = new DevExpress.XtraCharts.SeriesPoint("36-40", new object[] {
            ((object)(3D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint41 = new DevExpress.XtraCharts.SeriesPoint("41-45", new object[] {
            ((object)(4D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint42 = new DevExpress.XtraCharts.SeriesPoint("46-50", new object[] {
            ((object)(1D))});
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView8 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.Series series8 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SeriesPoint seriesPoint43 = new DevExpress.XtraCharts.SeriesPoint("18-25", new object[] {
            ((object)(2D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint44 = new DevExpress.XtraCharts.SeriesPoint("26-30", new object[] {
            ((object)(1D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint45 = new DevExpress.XtraCharts.SeriesPoint("31-35", new object[] {
            ((object)(3D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint46 = new DevExpress.XtraCharts.SeriesPoint("36-40", new object[] {
            ((object)(1D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint47 = new DevExpress.XtraCharts.SeriesPoint("41-45", new object[] {
            ((object)(4D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint48 = new DevExpress.XtraCharts.SeriesPoint("46-50", new object[] {
            ((object)(1D))});
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView9 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView10 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.ChartTitle chartTitle2 = new DevExpress.XtraCharts.ChartTitle();
            DevExpress.XtraCharts.SwiftPlotDiagram swiftPlotDiagram3 = new DevExpress.XtraCharts.SwiftPlotDiagram();
            DevExpress.XtraCharts.Series series9 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SeriesPoint seriesPoint49 = new DevExpress.XtraCharts.SeriesPoint("18-25", new object[] {
            ((object)(3D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint50 = new DevExpress.XtraCharts.SeriesPoint("26-30", new object[] {
            ((object)(5D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint51 = new DevExpress.XtraCharts.SeriesPoint("31-35", new object[] {
            ((object)(3D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint52 = new DevExpress.XtraCharts.SeriesPoint("36-40", new object[] {
            ((object)(3D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint53 = new DevExpress.XtraCharts.SeriesPoint("41-45", new object[] {
            ((object)(1D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint54 = new DevExpress.XtraCharts.SeriesPoint("46-50", new object[] {
            ((object)(2D))});
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView11 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.Series series10 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SeriesPoint seriesPoint55 = new DevExpress.XtraCharts.SeriesPoint("18-25", new object[] {
            ((object)(6D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint56 = new DevExpress.XtraCharts.SeriesPoint("26-30", new object[] {
            ((object)(9D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint57 = new DevExpress.XtraCharts.SeriesPoint("31-35", new object[] {
            ((object)(8D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint58 = new DevExpress.XtraCharts.SeriesPoint("36-40", new object[] {
            ((object)(7D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint59 = new DevExpress.XtraCharts.SeriesPoint("41-45", new object[] {
            ((object)(6D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint60 = new DevExpress.XtraCharts.SeriesPoint("46-50", new object[] {
            ((object)(8D))});
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView12 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.Series series11 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SeriesPoint seriesPoint61 = new DevExpress.XtraCharts.SeriesPoint("18-25", new object[] {
            ((object)(9D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint62 = new DevExpress.XtraCharts.SeriesPoint("26-30", new object[] {
            ((object)(8D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint63 = new DevExpress.XtraCharts.SeriesPoint("31-35", new object[] {
            ((object)(7D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint64 = new DevExpress.XtraCharts.SeriesPoint("36-40", new object[] {
            ((object)(8D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint65 = new DevExpress.XtraCharts.SeriesPoint("41-45", new object[] {
            ((object)(9D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint66 = new DevExpress.XtraCharts.SeriesPoint("46-50", new object[] {
            ((object)(8D))});
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView13 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.Series series12 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SeriesPoint seriesPoint67 = new DevExpress.XtraCharts.SeriesPoint("18-25", new object[] {
            ((object)(5D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint68 = new DevExpress.XtraCharts.SeriesPoint("26-30", new object[] {
            ((object)(7D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint69 = new DevExpress.XtraCharts.SeriesPoint("31-35", new object[] {
            ((object)(6D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint70 = new DevExpress.XtraCharts.SeriesPoint("36-40", new object[] {
            ((object)(1D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint71 = new DevExpress.XtraCharts.SeriesPoint("41-45", new object[] {
            ((object)(3D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint72 = new DevExpress.XtraCharts.SeriesPoint("46-50", new object[] {
            ((object)(4D))});
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView14 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView15 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.ChartTitle chartTitle3 = new DevExpress.XtraCharts.ChartTitle();
            DevExpress.XtraCharts.SwiftPlotDiagram swiftPlotDiagram4 = new DevExpress.XtraCharts.SwiftPlotDiagram();
            DevExpress.XtraCharts.Series series13 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SeriesPoint seriesPoint73 = new DevExpress.XtraCharts.SeriesPoint("18-25", new object[] {
            ((object)(3D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint74 = new DevExpress.XtraCharts.SeriesPoint("26-30", new object[] {
            ((object)(4D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint75 = new DevExpress.XtraCharts.SeriesPoint("31-35", new object[] {
            ((object)(7D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint76 = new DevExpress.XtraCharts.SeriesPoint("36-40", new object[] {
            ((object)(5D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint77 = new DevExpress.XtraCharts.SeriesPoint("41-45", new object[] {
            ((object)(6D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint78 = new DevExpress.XtraCharts.SeriesPoint("46-50", new object[] {
            ((object)(2D))});
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView16 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.Series series14 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SeriesPoint seriesPoint79 = new DevExpress.XtraCharts.SeriesPoint("18-25", new object[] {
            ((object)(9D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint80 = new DevExpress.XtraCharts.SeriesPoint("26-30", new object[] {
            ((object)(2D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint81 = new DevExpress.XtraCharts.SeriesPoint("31-35", new object[] {
            ((object)(1D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint82 = new DevExpress.XtraCharts.SeriesPoint("36-40", new object[] {
            ((object)(5D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint83 = new DevExpress.XtraCharts.SeriesPoint("41-45", new object[] {
            ((object)(6D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint84 = new DevExpress.XtraCharts.SeriesPoint("46-50", new object[] {
            ((object)(8D))});
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView17 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.Series series15 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SeriesPoint seriesPoint85 = new DevExpress.XtraCharts.SeriesPoint("18-25", new object[] {
            ((object)(3D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint86 = new DevExpress.XtraCharts.SeriesPoint("26-30", new object[] {
            ((object)(7D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint87 = new DevExpress.XtraCharts.SeriesPoint("31-35", new object[] {
            ((object)(6D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint88 = new DevExpress.XtraCharts.SeriesPoint("36-40", new object[] {
            ((object)(8D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint89 = new DevExpress.XtraCharts.SeriesPoint("41-45", new object[] {
            ((object)(2D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint90 = new DevExpress.XtraCharts.SeriesPoint("46-50", new object[] {
            ((object)(1D))});
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView18 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.Series series16 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SeriesPoint seriesPoint91 = new DevExpress.XtraCharts.SeriesPoint("18-25", new object[] {
            ((object)(5D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint92 = new DevExpress.XtraCharts.SeriesPoint("26-30", new object[] {
            ((object)(7D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint93 = new DevExpress.XtraCharts.SeriesPoint("31-35", new object[] {
            ((object)(6D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint94 = new DevExpress.XtraCharts.SeriesPoint("36-40", new object[] {
            ((object)(1D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint95 = new DevExpress.XtraCharts.SeriesPoint("41-45", new object[] {
            ((object)(3D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint96 = new DevExpress.XtraCharts.SeriesPoint("46-50", new object[] {
            ((object)(4D))});
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView19 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView20 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.ChartTitle chartTitle4 = new DevExpress.XtraCharts.ChartTitle();
            DevExpress.XtraCharts.SwiftPlotDiagram swiftPlotDiagram5 = new DevExpress.XtraCharts.SwiftPlotDiagram();
            DevExpress.XtraCharts.Series series17 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SeriesPoint seriesPoint97 = new DevExpress.XtraCharts.SeriesPoint("18-25", new object[] {
            ((object)(3D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint98 = new DevExpress.XtraCharts.SeriesPoint("26-30", new object[] {
            ((object)(2D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint99 = new DevExpress.XtraCharts.SeriesPoint("31-35", new object[] {
            ((object)(1D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint100 = new DevExpress.XtraCharts.SeriesPoint("36-40", new object[] {
            ((object)(3D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint101 = new DevExpress.XtraCharts.SeriesPoint("41-45", new object[] {
            ((object)(4D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint102 = new DevExpress.XtraCharts.SeriesPoint("46-50", new object[] {
            ((object)(5D))});
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView21 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.Series series18 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SeriesPoint seriesPoint103 = new DevExpress.XtraCharts.SeriesPoint("18-25", new object[] {
            ((object)(3D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint104 = new DevExpress.XtraCharts.SeriesPoint("26-30", new object[] {
            ((object)(1D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint105 = new DevExpress.XtraCharts.SeriesPoint("31-35", new object[] {
            ((object)(6D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint106 = new DevExpress.XtraCharts.SeriesPoint("36-40", new object[] {
            ((object)(4D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint107 = new DevExpress.XtraCharts.SeriesPoint("41-45", new object[] {
            ((object)(2D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint108 = new DevExpress.XtraCharts.SeriesPoint("46-50", new object[] {
            ((object)(5D))});
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView22 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.Series series19 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SeriesPoint seriesPoint109 = new DevExpress.XtraCharts.SeriesPoint("18-25", new object[] {
            ((object)(5D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint110 = new DevExpress.XtraCharts.SeriesPoint("26-30", new object[] {
            ((object)(6D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint111 = new DevExpress.XtraCharts.SeriesPoint("31-35", new object[] {
            ((object)(3D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint112 = new DevExpress.XtraCharts.SeriesPoint("36-40", new object[] {
            ((object)(1D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint113 = new DevExpress.XtraCharts.SeriesPoint("41-45", new object[] {
            ((object)(4D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint114 = new DevExpress.XtraCharts.SeriesPoint("46-50", new object[] {
            ((object)(2D))});
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView23 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.Series series20 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SeriesPoint seriesPoint115 = new DevExpress.XtraCharts.SeriesPoint("18-25", new object[] {
            ((object)(2D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint116 = new DevExpress.XtraCharts.SeriesPoint("26-30", new object[] {
            ((object)(3D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint117 = new DevExpress.XtraCharts.SeriesPoint("31-35", new object[] {
            ((object)(6D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint118 = new DevExpress.XtraCharts.SeriesPoint("36-40", new object[] {
            ((object)(1D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint119 = new DevExpress.XtraCharts.SeriesPoint("41-45", new object[] {
            ((object)(4D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint120 = new DevExpress.XtraCharts.SeriesPoint("46-50", new object[] {
            ((object)(2D))});
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView24 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView25 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.ChartTitle chartTitle5 = new DevExpress.XtraCharts.ChartTitle();
            DevExpress.XtraCharts.SwiftPlotDiagram swiftPlotDiagram1 = new DevExpress.XtraCharts.SwiftPlotDiagram();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SeriesPoint seriesPoint1 = new DevExpress.XtraCharts.SeriesPoint("18-25", new object[] {
            ((object)(1D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint2 = new DevExpress.XtraCharts.SeriesPoint("26-30", new object[] {
            ((object)(2D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint3 = new DevExpress.XtraCharts.SeriesPoint("31-35", new object[] {
            ((object)(3D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint4 = new DevExpress.XtraCharts.SeriesPoint("36-40", new object[] {
            ((object)(4D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint5 = new DevExpress.XtraCharts.SeriesPoint("41-45", new object[] {
            ((object)(5D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint6 = new DevExpress.XtraCharts.SeriesPoint("46-50", new object[] {
            ((object)(6D))});
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView1 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SeriesPoint seriesPoint7 = new DevExpress.XtraCharts.SeriesPoint("18-25", new object[] {
            ((object)(6D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint8 = new DevExpress.XtraCharts.SeriesPoint("26-30", new object[] {
            ((object)(5D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint9 = new DevExpress.XtraCharts.SeriesPoint("31-35", new object[] {
            ((object)(4D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint10 = new DevExpress.XtraCharts.SeriesPoint("36-40", new object[] {
            ((object)(3D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint11 = new DevExpress.XtraCharts.SeriesPoint("41-45", new object[] {
            ((object)(2D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint12 = new DevExpress.XtraCharts.SeriesPoint("46-50", new object[] {
            ((object)(1D))});
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView2 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.Series series3 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SeriesPoint seriesPoint13 = new DevExpress.XtraCharts.SeriesPoint("18-25", new object[] {
            ((object)(8D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint14 = new DevExpress.XtraCharts.SeriesPoint("26-30", new object[] {
            ((object)(9D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint15 = new DevExpress.XtraCharts.SeriesPoint("31-35", new object[] {
            ((object)(10D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint16 = new DevExpress.XtraCharts.SeriesPoint("36-40", new object[] {
            ((object)(5D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint17 = new DevExpress.XtraCharts.SeriesPoint("41-45", new object[] {
            ((object)(2D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint18 = new DevExpress.XtraCharts.SeriesPoint("46-50", new object[] {
            ((object)(1D))});
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView3 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.Series series4 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SeriesPoint seriesPoint19 = new DevExpress.XtraCharts.SeriesPoint("18-25", new object[] {
            ((object)(3D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint20 = new DevExpress.XtraCharts.SeriesPoint("26-30", new object[] {
            ((object)(5D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint21 = new DevExpress.XtraCharts.SeriesPoint("31-35", new object[] {
            ((object)(8D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint22 = new DevExpress.XtraCharts.SeriesPoint("36-40", new object[] {
            ((object)(1D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint23 = new DevExpress.XtraCharts.SeriesPoint("41-45", new object[] {
            ((object)(6D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint24 = new DevExpress.XtraCharts.SeriesPoint("46-50", new object[] {
            ((object)(9D))});
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView4 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView5 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.ChartTitle chartTitle1 = new DevExpress.XtraCharts.ChartTitle();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrChart2 = new DevExpress.XtraReports.UI.XRChart();
            this.DetailReport1 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrChart3 = new DevExpress.XtraReports.UI.XRChart();
            this.DetailReport2 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail3 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrChart4 = new DevExpress.XtraReports.UI.XRChart();
            this.DetailReport3 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail4 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrChart5 = new DevExpress.XtraReports.UI.XRChart();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrChart1 = new DevExpress.XtraReports.UI.XRChart();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotDiagram2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotDiagram3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotDiagram4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotDiagram5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.BorderColor = System.Drawing.Color.LightGray;
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel3,
            this.xrTable1,
            this.xrChart1});
            this.Detail.HeightF = 742.4167F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseBorderColor = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 20.83333F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 41.66667F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(476.2499F, 0F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(107.2917F, 71.54167F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(218.5417F, 71.54167F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(614.5833F, 23F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "LEMBAGA HASIL DALAM NEGERI MALAYSIA";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1});
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrChart2});
            this.Detail1.HeightF = 587.9167F;
            this.Detail1.Name = "Detail1";
            // 
            // xrChart2
            // 
            this.xrChart2.AppearanceNameSerializable = "Pastel Kit";
            this.xrChart2.BorderColor = System.Drawing.Color.LightGray;
            this.xrChart2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            swiftPlotDiagram2.AxisX.Interlaced = true;
            swiftPlotDiagram2.AxisX.Range.SideMarginsEnabled = true;
            swiftPlotDiagram2.AxisX.Title.Text = "Umur";
            swiftPlotDiagram2.AxisX.Title.Visible = true;
            swiftPlotDiagram2.AxisX.VisibleInPanesSerializable = "-1";
            swiftPlotDiagram2.AxisY.Range.SideMarginsEnabled = true;
            swiftPlotDiagram2.AxisY.Title.Text = "Jumlah Pemohon";
            swiftPlotDiagram2.AxisY.Title.Visible = true;
            swiftPlotDiagram2.AxisY.VisibleInPanesSerializable = "-1";
            swiftPlotDiagram2.DefaultPane.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Solid;
            this.xrChart2.Diagram = swiftPlotDiagram2;
            this.xrChart2.EmptyChartText.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.xrChart2.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrChart2.Name = "xrChart2";
            series5.Name = "GRED A";
            series5.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint25,
            seriesPoint26,
            seriesPoint27,
            seriesPoint28,
            seriesPoint29,
            seriesPoint30});
            swiftPlotSeriesView6.Antialiasing = true;
            swiftPlotSeriesView6.LineStyle.Thickness = 2;
            series5.View = swiftPlotSeriesView6;
            series6.Name = "GRED B";
            series6.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint31,
            seriesPoint32,
            seriesPoint33,
            seriesPoint34,
            seriesPoint35,
            seriesPoint36});
            swiftPlotSeriesView7.LineStyle.Thickness = 2;
            series6.View = swiftPlotSeriesView7;
            series7.Name = "GRED C";
            series7.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint37,
            seriesPoint38,
            seriesPoint39,
            seriesPoint40,
            seriesPoint41,
            seriesPoint42});
            swiftPlotSeriesView8.LineStyle.Thickness = 2;
            series7.View = swiftPlotSeriesView8;
            series8.Name = "GRED D";
            series8.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint43,
            seriesPoint44,
            seriesPoint45,
            seriesPoint46,
            seriesPoint47,
            seriesPoint48});
            swiftPlotSeriesView9.LineStyle.Thickness = 2;
            series8.View = swiftPlotSeriesView9;
            this.xrChart2.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series5,
        series6,
        series7,
        series8};
            this.xrChart2.SeriesTemplate.View = swiftPlotSeriesView10;
            this.xrChart2.SizeF = new System.Drawing.SizeF(1051F, 587.9167F);
            this.xrChart2.StylePriority.UseBorderColor = false;
            this.xrChart2.StylePriority.UseBorders = false;
            chartTitle2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartTitle2.Text = "Pecahan Permohonan Berbangsa Melayu dengan BM Kepujian Peringkat PMR Lulus Mengik" +
    "ut Had Umur";
            this.xrChart2.Titles.AddRange(new DevExpress.XtraCharts.ChartTitle[] {
            chartTitle2});
            // 
            // DetailReport1
            // 
            this.DetailReport1.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2,
            this.DetailReport2});
            this.DetailReport1.Level = 1;
            this.DetailReport1.Name = "DetailReport1";
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrChart3});
            this.Detail2.HeightF = 587.9167F;
            this.Detail2.Name = "Detail2";
            // 
            // xrChart3
            // 
            this.xrChart3.AppearanceNameSerializable = "Pastel Kit";
            this.xrChart3.BorderColor = System.Drawing.Color.LightGray;
            this.xrChart3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            swiftPlotDiagram3.AxisX.Interlaced = true;
            swiftPlotDiagram3.AxisX.Range.SideMarginsEnabled = true;
            swiftPlotDiagram3.AxisX.Title.Text = "Umur";
            swiftPlotDiagram3.AxisX.Title.Visible = true;
            swiftPlotDiagram3.AxisX.VisibleInPanesSerializable = "-1";
            swiftPlotDiagram3.AxisY.Range.SideMarginsEnabled = true;
            swiftPlotDiagram3.AxisY.Title.Text = "Jumlah Pemohon";
            swiftPlotDiagram3.AxisY.Title.Visible = true;
            swiftPlotDiagram3.AxisY.VisibleInPanesSerializable = "-1";
            swiftPlotDiagram3.DefaultPane.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Solid;
            this.xrChart3.Diagram = swiftPlotDiagram3;
            this.xrChart3.EmptyChartText.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.xrChart3.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrChart3.Name = "xrChart3";
            series9.Name = "GRED A";
            series9.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint49,
            seriesPoint50,
            seriesPoint51,
            seriesPoint52,
            seriesPoint53,
            seriesPoint54});
            swiftPlotSeriesView11.Antialiasing = true;
            swiftPlotSeriesView11.LineStyle.Thickness = 2;
            series9.View = swiftPlotSeriesView11;
            series10.Name = "GRED B";
            series10.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint55,
            seriesPoint56,
            seriesPoint57,
            seriesPoint58,
            seriesPoint59,
            seriesPoint60});
            swiftPlotSeriesView12.LineStyle.Thickness = 2;
            series10.View = swiftPlotSeriesView12;
            series11.Name = "GRED C";
            series11.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint61,
            seriesPoint62,
            seriesPoint63,
            seriesPoint64,
            seriesPoint65,
            seriesPoint66});
            swiftPlotSeriesView13.LineStyle.Thickness = 2;
            series11.View = swiftPlotSeriesView13;
            series12.Name = "GRED D";
            series12.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint67,
            seriesPoint68,
            seriesPoint69,
            seriesPoint70,
            seriesPoint71,
            seriesPoint72});
            swiftPlotSeriesView14.LineStyle.Thickness = 2;
            series12.View = swiftPlotSeriesView14;
            this.xrChart3.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series9,
        series10,
        series11,
        series12};
            this.xrChart3.SeriesTemplate.View = swiftPlotSeriesView15;
            this.xrChart3.SizeF = new System.Drawing.SizeF(1051F, 587.9167F);
            this.xrChart3.StylePriority.UseBorderColor = false;
            this.xrChart3.StylePriority.UseBorders = false;
            chartTitle3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartTitle3.Text = "Pecahan Permohonan Berbangsa Cina dengan BM Kepujian Peringkat PMR Lulus Mengikut" +
    " Had Umur";
            this.xrChart3.Titles.AddRange(new DevExpress.XtraCharts.ChartTitle[] {
            chartTitle3});
            // 
            // DetailReport2
            // 
            this.DetailReport2.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail3,
            this.DetailReport3});
            this.DetailReport2.Level = 0;
            this.DetailReport2.Name = "DetailReport2";
            // 
            // Detail3
            // 
            this.Detail3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrChart4});
            this.Detail3.HeightF = 587.9167F;
            this.Detail3.Name = "Detail3";
            // 
            // xrChart4
            // 
            this.xrChart4.AppearanceNameSerializable = "Pastel Kit";
            this.xrChart4.BorderColor = System.Drawing.Color.LightGray;
            this.xrChart4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            swiftPlotDiagram4.AxisX.Interlaced = true;
            swiftPlotDiagram4.AxisX.Range.SideMarginsEnabled = true;
            swiftPlotDiagram4.AxisX.Title.Text = "Umur";
            swiftPlotDiagram4.AxisX.Title.Visible = true;
            swiftPlotDiagram4.AxisX.VisibleInPanesSerializable = "-1";
            swiftPlotDiagram4.AxisY.Range.SideMarginsEnabled = true;
            swiftPlotDiagram4.AxisY.Title.Text = "Jumlah Pemohon";
            swiftPlotDiagram4.AxisY.Title.Visible = true;
            swiftPlotDiagram4.AxisY.VisibleInPanesSerializable = "-1";
            swiftPlotDiagram4.DefaultPane.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Solid;
            this.xrChart4.Diagram = swiftPlotDiagram4;
            this.xrChart4.EmptyChartText.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.xrChart4.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrChart4.Name = "xrChart4";
            series13.Name = "GRED A";
            series13.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint73,
            seriesPoint74,
            seriesPoint75,
            seriesPoint76,
            seriesPoint77,
            seriesPoint78});
            swiftPlotSeriesView16.Antialiasing = true;
            swiftPlotSeriesView16.LineStyle.Thickness = 2;
            series13.View = swiftPlotSeriesView16;
            series14.Name = "GRED B";
            series14.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint79,
            seriesPoint80,
            seriesPoint81,
            seriesPoint82,
            seriesPoint83,
            seriesPoint84});
            swiftPlotSeriesView17.LineStyle.Thickness = 2;
            series14.View = swiftPlotSeriesView17;
            series15.Name = "GRED C";
            series15.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint85,
            seriesPoint86,
            seriesPoint87,
            seriesPoint88,
            seriesPoint89,
            seriesPoint90});
            swiftPlotSeriesView18.LineStyle.Thickness = 2;
            series15.View = swiftPlotSeriesView18;
            series16.Name = "GRED D";
            series16.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint91,
            seriesPoint92,
            seriesPoint93,
            seriesPoint94,
            seriesPoint95,
            seriesPoint96});
            swiftPlotSeriesView19.LineStyle.Thickness = 2;
            series16.View = swiftPlotSeriesView19;
            this.xrChart4.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series13,
        series14,
        series15,
        series16};
            this.xrChart4.SeriesTemplate.View = swiftPlotSeriesView20;
            this.xrChart4.SizeF = new System.Drawing.SizeF(1051F, 587.9167F);
            this.xrChart4.StylePriority.UseBorderColor = false;
            this.xrChart4.StylePriority.UseBorders = false;
            chartTitle4.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartTitle4.Text = "Pecahan Permohonan Berbangsa India dengan BM Kepujian Peringkat PMR Lulus Mengiku" +
    "t Had Umur";
            this.xrChart4.Titles.AddRange(new DevExpress.XtraCharts.ChartTitle[] {
            chartTitle4});
            // 
            // DetailReport3
            // 
            this.DetailReport3.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail4});
            this.DetailReport3.Level = 0;
            this.DetailReport3.Name = "DetailReport3";
            // 
            // Detail4
            // 
            this.Detail4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrChart5});
            this.Detail4.HeightF = 587.9167F;
            this.Detail4.Name = "Detail4";
            // 
            // xrChart5
            // 
            this.xrChart5.AppearanceNameSerializable = "Pastel Kit";
            this.xrChart5.BorderColor = System.Drawing.Color.LightGray;
            this.xrChart5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            swiftPlotDiagram5.AxisX.Interlaced = true;
            swiftPlotDiagram5.AxisX.Range.SideMarginsEnabled = true;
            swiftPlotDiagram5.AxisX.Title.Text = "Umur";
            swiftPlotDiagram5.AxisX.Title.Visible = true;
            swiftPlotDiagram5.AxisX.VisibleInPanesSerializable = "-1";
            swiftPlotDiagram5.AxisY.Range.SideMarginsEnabled = true;
            swiftPlotDiagram5.AxisY.Title.Text = "Jumlah Pemohon";
            swiftPlotDiagram5.AxisY.Title.Visible = true;
            swiftPlotDiagram5.AxisY.VisibleInPanesSerializable = "-1";
            swiftPlotDiagram5.DefaultPane.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Solid;
            this.xrChart5.Diagram = swiftPlotDiagram5;
            this.xrChart5.EmptyChartText.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.xrChart5.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrChart5.Name = "xrChart5";
            series17.Name = "GRED A";
            series17.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint97,
            seriesPoint98,
            seriesPoint99,
            seriesPoint100,
            seriesPoint101,
            seriesPoint102});
            swiftPlotSeriesView21.Antialiasing = true;
            swiftPlotSeriesView21.LineStyle.Thickness = 2;
            series17.View = swiftPlotSeriesView21;
            series18.Name = "GRED B";
            series18.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint103,
            seriesPoint104,
            seriesPoint105,
            seriesPoint106,
            seriesPoint107,
            seriesPoint108});
            swiftPlotSeriesView22.LineStyle.Thickness = 2;
            series18.View = swiftPlotSeriesView22;
            series19.Name = "GRED C";
            series19.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint109,
            seriesPoint110,
            seriesPoint111,
            seriesPoint112,
            seriesPoint113,
            seriesPoint114});
            swiftPlotSeriesView23.LineStyle.Thickness = 2;
            series19.View = swiftPlotSeriesView23;
            series20.Name = "GRED D";
            series20.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint115,
            seriesPoint116,
            seriesPoint117,
            seriesPoint118,
            seriesPoint119,
            seriesPoint120});
            swiftPlotSeriesView24.LineStyle.Thickness = 2;
            series20.View = swiftPlotSeriesView24;
            this.xrChart5.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series17,
        series18,
        series19,
        series20};
            this.xrChart5.SeriesTemplate.View = swiftPlotSeriesView25;
            this.xrChart5.SizeF = new System.Drawing.SizeF(1051F, 587.9167F);
            this.xrChart5.StylePriority.UseBorderColor = false;
            this.xrChart5.StylePriority.UseBorders = false;
            chartTitle5.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartTitle5.Text = "Pecahan Permohonan Berbangsa Lain-lain dengan BM Kepujian Peringkat PMR Lulus Men" +
    "gikut Had Umur";
            this.xrChart5.Titles.AddRange(new DevExpress.XtraCharts.ChartTitle[] {
            chartTitle5});
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel2,
            this.xrPictureBox1,
            this.xrLabel1});
            this.PageHeader.HeightF = 117.5417F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.HeightF = 33.00001F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrChart1
            // 
            this.xrChart1.AppearanceNameSerializable = "Pastel Kit";
            this.xrChart1.BorderColor = System.Drawing.Color.LightGray;
            this.xrChart1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            swiftPlotDiagram1.AxisX.Interlaced = true;
            swiftPlotDiagram1.AxisX.Range.SideMarginsEnabled = true;
            swiftPlotDiagram1.AxisX.Title.Text = "Umur";
            swiftPlotDiagram1.AxisX.Title.Visible = true;
            swiftPlotDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            swiftPlotDiagram1.AxisY.Range.SideMarginsEnabled = true;
            swiftPlotDiagram1.AxisY.Title.Text = "Jumlah Pemohon";
            swiftPlotDiagram1.AxisY.Title.Visible = true;
            swiftPlotDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            swiftPlotDiagram1.DefaultPane.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Solid;
            this.xrChart1.Diagram = swiftPlotDiagram1;
            this.xrChart1.EmptyChartText.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.xrChart1.LocationFloat = new DevExpress.Utils.PointFloat(5.000051F, 200.3333F);
            this.xrChart1.Name = "xrChart1";
            series1.Name = "GRED A";
            series1.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint1,
            seriesPoint2,
            seriesPoint3,
            seriesPoint4,
            seriesPoint5,
            seriesPoint6});
            swiftPlotSeriesView1.Antialiasing = true;
            swiftPlotSeriesView1.LineStyle.Thickness = 2;
            series1.View = swiftPlotSeriesView1;
            series2.Name = "GRED B";
            series2.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint7,
            seriesPoint8,
            seriesPoint9,
            seriesPoint10,
            seriesPoint11,
            seriesPoint12});
            swiftPlotSeriesView2.LineStyle.Thickness = 2;
            series2.View = swiftPlotSeriesView2;
            series3.Name = "GRED C";
            series3.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint13,
            seriesPoint14,
            seriesPoint15,
            seriesPoint16,
            seriesPoint17,
            seriesPoint18});
            swiftPlotSeriesView3.LineStyle.Thickness = 2;
            series3.View = swiftPlotSeriesView3;
            series4.Name = "GRED D";
            series4.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint19,
            seriesPoint20,
            seriesPoint21,
            seriesPoint22,
            seriesPoint23,
            seriesPoint24});
            swiftPlotSeriesView4.LineStyle.Thickness = 2;
            series4.View = swiftPlotSeriesView4;
            this.xrChart1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1,
        series2,
        series3,
        series4};
            this.xrChart1.SeriesTemplate.View = swiftPlotSeriesView5;
            this.xrChart1.SizeF = new System.Drawing.SizeF(1051F, 445.2083F);
            this.xrChart1.StylePriority.UseBorderColor = false;
            this.xrChart1.StylePriority.UseBorders = false;
            chartTitle1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartTitle1.Text = "Pecahan Permohonan dengan BM Kepujian Peringkat PMR Lulus Mengikut Had Umur";
            this.xrChart1.Titles.AddRange(new DevExpress.XtraCharts.ChartTitle[] {
            chartTitle1});
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(144.5417F, 94.54168F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(794.7916F, 23F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "STATISTIK PERMOHONAN JAWATAN PEMBANTU AM ( PEMANDU KENDERAAN) GRED 1";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(217.5417F, 44.12498F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4,
            this.xrTableRow1,
            this.xrTableRow3,
            this.xrTableRow2,
            this.xrTableRow5});
            this.xrTable1.SizeF = new System.Drawing.SizeF(654.9584F, 124.2188F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell3});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "A";
            this.xrTableCell1.Weight = 1D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "30";
            this.xrTableCell2.Weight = 1D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "23";
            this.xrTableCell3.Weight = 1D;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell6});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "C";
            this.xrTableCell4.Weight = 1D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "8";
            this.xrTableCell5.Weight = 1D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "3";
            this.xrTableCell6.Weight = 1D;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell9});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Text = "B";
            this.xrTableCell7.Weight = 1D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Text = "25";
            this.xrTableCell8.Weight = 1D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Text = "16";
            this.xrTableCell9.Weight = 1D;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.BackColor = System.Drawing.Color.LightBlue;
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell11,
            this.xrTableCell12});
            this.xrTableRow4.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.StylePriority.UseBackColor = false;
            this.xrTableRow4.StylePriority.UseFont = false;
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Text = "GRED";
            this.xrTableCell10.Weight = 1D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Text = "Permohonan yang tidak lengkap";
            this.xrTableCell11.Weight = 1D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Text = "Jumlah Pemohon";
            this.xrTableCell12.Weight = 1D;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(144.5417F, 16.5F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(794.7916F, 23F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "STATISTIK KESELURUHAN PERMOHONAN";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell15});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Text = "D";
            this.xrTableCell13.Weight = 1D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Text = "25";
            this.xrTableCell14.Weight = 1D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Text = "14";
            this.xrTableCell15.Weight = 1D;
            // 
            // RepPecahanpemohonangraph
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.DetailReport,
            this.DetailReport1,
            this.PageHeader,
            this.PageFooter});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(18, 16, 21, 42);
            this.PageHeight = 850;
            this.PageWidth = 1100;
            this.Version = "11.2";
            ((System.ComponentModel.ISupportInitialize)(swiftPlotDiagram2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotDiagram3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotDiagram4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotDiagram5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRChart xrChart2;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport1;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.XRChart xrChart3;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport2;
        private DevExpress.XtraReports.UI.DetailBand Detail3;
        private DevExpress.XtraReports.UI.XRChart xrChart4;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport3;
        private DevExpress.XtraReports.UI.DetailBand Detail4;
        private DevExpress.XtraReports.UI.XRChart xrChart5;
        private DevExpress.XtraReports.UI.XRChart xrChart1;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
    }
}
