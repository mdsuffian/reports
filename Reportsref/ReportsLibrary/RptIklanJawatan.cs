﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace ReportsLibrary
{
    public partial class RptIklanJawatan : DevExpress.XtraReports.UI.XtraReport
    {
        public RptIklanJawatan(string ESSDBNAME, string RefID)
        {
            InitializeComponent();
            maklumatIklanJawatanTableAdapter.Connection.ConnectionString = RefLib.JobPortalconnstr;

            maklumatIklanJawatanTableAdapter.Fill(repDataset1.maklumatIklanJawatan, ESSDBNAME, RefID);
        }

    }
}
