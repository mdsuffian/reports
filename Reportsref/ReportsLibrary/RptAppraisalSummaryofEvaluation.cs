﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace ReportsLibrary
{
    public partial class RptAppraisalSummaryofEvaluation : DevExpress.XtraReports.UI.XtraReport
    {
        public RptAppraisalSummaryofEvaluation(string CdBranch, string PrdApprsl, string TypMark, int Lvl1, int Lvl2, int Lvl3, int Lvl4, int Lvl5, string DTAnalyse)
        {
            InitializeComponent();

            fGetPfmHCTableAdapter.Connection.ConnectionString = RefLib.Repconnstr;
            noStaffbasedongrpTableAdapter.Connection.ConnectionString = RefLib.Repconnstr;

            fGetPfmHCTableAdapter.Fill(repDataset1.fGetPfmHC, CdBranch, PrdApprsl, TypMark, Lvl5, Lvl4, Lvl3, Lvl2, Lvl1,DTAnalyse);
            noStaffbasedongrpTableAdapter.Fill(repDataset1.NoStaffbasedongrp, CdBranch, PrdApprsl, TypMark, Lvl1, Lvl2, Lvl3, Lvl4, Lvl5,DTAnalyse);
            
        }

    }
}
