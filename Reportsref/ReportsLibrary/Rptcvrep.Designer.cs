﻿namespace ReportsLibrary
{
    partial class Rptcvrep
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Rptcvrep));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel93 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel92 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel91 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel84 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel85 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel83 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel81 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel82 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel89 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel90 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel88 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel86 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel87 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel80 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel73 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel74 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel72 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel70 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel71 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel78 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel79 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel77 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel75 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel76 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel63 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel64 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel61 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel62 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel65 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel68 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel69 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel66 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel67 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel55 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel53 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel56 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel59 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel60 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel57 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel58 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel50 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel44 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.XRLjawatandata = new DevExpress.XtraReports.UI.XRLabel();
            this.XRLtarikhiklandata = new DevExpress.XtraReports.UI.XRLabel();
            this.XRLjawatan2 = new DevExpress.XtraReports.UI.XRLabel();
            this.XRLtarikhiklan2 = new DevExpress.XtraReports.UI.XRLabel();
            this.XRLjawatan = new DevExpress.XtraReports.UI.XRLabel();
            this.XRLtarikhiklan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox2 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel94 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel96 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel100 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel103 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel102 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel99 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel101 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel95 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel98 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel97 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel109 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel110 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel111 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel108 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel105 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel107 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel114 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel115 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel113 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel116 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel118 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel117 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLabel128 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Maklumat_Waris = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.maklumatWarisTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.MaklumatWarisTableAdapter();
            this.repDataset1 = new ReportsLibrary.RepDataset();
            this.Maklumat_KesihatanFizikal = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel104 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel305 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel136 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCheckBox2 = new DevExpress.XtraReports.UI.XRCheckBox();
            this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel135 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel134 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel133 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel132 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCheckBox1 = new DevExpress.XtraReports.UI.XRCheckBox();
            this.xrLabel131 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel130 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel129 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine6 = new DevExpress.XtraReports.UI.XRLine();
            this.GroupFooter2 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.pGetCVPhysicalHealthinfoTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.pGetCVPhysicalHealthinfoTableAdapter();
            this.Kemahiran_Bahasa = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail3 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrCheckBox5 = new DevExpress.XtraReports.UI.XRCheckBox();
            this.xrCheckBox3 = new DevExpress.XtraReports.UI.XRCheckBox();
            this.xrLabel142 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel141 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCheckBox7 = new DevExpress.XtraReports.UI.XRCheckBox();
            this.GroupHeader3 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel140 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel139 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel138 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel137 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine7 = new DevExpress.XtraReports.UI.XRLine();
            this.GroupFooter3 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.maklumatKemahiranBahasaTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.maklumatKemahiranBahasaTableAdapter();
            this.noStaffbasedongrpTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.NoStaffbasedongrpTableAdapter();
            this.Maklumat_Persekolahan = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail4 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader4 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine8 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel144 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter4 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.maklumatSekolahTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.maklumatSekolahTableAdapter();
            this.xrLabel145 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel146 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel147 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel148 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel149 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel150 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel151 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel152 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel153 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel154 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel155 = new DevExpress.XtraReports.UI.XRLabel();
            this.Keputusan_Peperiksaan = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail5 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel124 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel123 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel125 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel127 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel126 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel119 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel112 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel120 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel122 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel121 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader5 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.GroupFooter5 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.maklumatPeperiksaanTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.maklumatPeperiksaanTableAdapter();
            this.Maklumat_Institusi_Pengajian = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail6 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader6 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel156 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine9 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter6 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.maklumatIPTTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.maklumatIPTTableAdapter();
            this.Maklumat_Badan_Profesional = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail7 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader7 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel157 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine10 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter7 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.maklumatBadanProTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.maklumatBadanProTableAdapter();
            this.Maklumat_KPSL = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail8 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader8 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel158 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine11 = new DevExpress.XtraReports.UI.XRLine();
            this.GroupFooter8 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.Maklumat_Sijil_Kemahiran = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail9 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable12 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader9 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel159 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine12 = new DevExpress.XtraReports.UI.XRLine();
            this.GroupFooter9 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.maklumatsijilkemahiranTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.maklumatsijilkemahiranTableAdapter();
            this.Maklumat_Kemahiran_Komputer = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail10 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable14 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader10 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable13 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel160 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine13 = new DevExpress.XtraReports.UI.XRLine();
            this.GroupFooter10 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.maklumatKemahiranKomputerTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.maklumatKemahiranKomputerTableAdapter();
            this.Maklumat_Kegiatan_Luar = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail11 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable16 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader11 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLine14 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel161 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable15 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter11 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTable17 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.maklumatKegiatanLuarTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.maklumatKegiatanLuarTableAdapter();
            this.Maklumat_Pekerjaan = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail12 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel176 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel143 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel208 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel209 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel210 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel205 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel206 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel207 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel204 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel203 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel202 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel199 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel198 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel201 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel200 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel197 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel194 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel193 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel196 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel195 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel190 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel189 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel192 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel191 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel188 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel185 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel184 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel187 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel186 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel182 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel175 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel177 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel174 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel173 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel172 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel168 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel167 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel166 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel165 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel164 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel163 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader12 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLine15 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel162 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter12 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.maklumatPekerjaanTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.maklumatPekerjaanTableAdapter();
            this.LainLain_Maklumat = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail13 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrCheckBox11 = new DevExpress.XtraReports.UI.XRCheckBox();
            this.xrCheckBox10 = new DevExpress.XtraReports.UI.XRCheckBox();
            this.xrLabel214 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel213 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader13 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLine16 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel211 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel212 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel169 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel170 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter13 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.lainlainMaklumatTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.LainlainMaklumatTableAdapter();
            this.Rujukan = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail14 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel261 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel260 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel259 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel256 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel257 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel258 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel253 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel254 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel255 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel252 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel251 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel250 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel234 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel233 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel232 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel249 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel248 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel247 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel231 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel230 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel228 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel237 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel236 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel229 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel226 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel227 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel235 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader14 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLine17 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel225 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter14 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.maklumatrujukanTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.maklumatrujukanTableAdapter();
            this.Pengakuan_Pemohon = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail15 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel300 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel299 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel298 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel297 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel296 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel295 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel294 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel293 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel291 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel290 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel289 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel288 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel287 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel286 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel285 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel284 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel283 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel282 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel281 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel280 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel279 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel278 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel277 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel276 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel275 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel274 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel273 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel272 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel271 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel270 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel269 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel268 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel267 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel266 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel292 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader15 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLine18 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel265 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter15 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.Maklumat_Kecacatan = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail16 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel106 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel306 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel301 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCheckBox22 = new DevExpress.XtraReports.UI.XRCheckBox();
            this.GroupHeader16 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCheckBox21 = new DevExpress.XtraReports.UI.XRCheckBox();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter16 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrLabel319 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel318 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel317 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel316 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel315 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel314 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel313 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel312 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCheckBox27 = new DevExpress.XtraReports.UI.XRCheckBox();
            this.xrLabel311 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCheckBox26 = new DevExpress.XtraReports.UI.XRCheckBox();
            this.xrLabel310 = new DevExpress.XtraReports.UI.XRLabel();
            this.maklumatkecacatanTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.maklumatkecacatanTableAdapter();
            this.Maklumat_Pasangan = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail17 = new DevExpress.XtraReports.UI.DetailBand();
            this.GroupHeader17 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.GroupFooter17 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.FullAddress = new DevExpress.XtraReports.UI.CalculatedField();
            this.OtherFullAddress = new DevExpress.XtraReports.UI.CalculatedField();
            this.Tanda = new DevExpress.XtraReports.UI.CalculatedField();
            this.NonSmoker = new DevExpress.XtraReports.UI.CalculatedField();
            this.maklumatPasanganTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.MaklumatPasanganTableAdapter();
            this.maklumatfizikalTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.maklumatfizikalTableAdapter();
            this.TandaKecacatan = new DevExpress.XtraReports.UI.CalculatedField();
            this.BahasaLemah = new DevExpress.XtraReports.UI.CalculatedField();
            this.BahasaSederhana = new DevExpress.XtraReports.UI.CalculatedField();
            this.BahasaBaik = new DevExpress.XtraReports.UI.CalculatedField();
            this.TahunSekolah = new DevExpress.XtraReports.UI.CalculatedField();
            this.maklumatHobiTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.maklumatHobiTableAdapter();
            this.LainYa = new DevExpress.XtraReports.UI.CalculatedField();
            this.LainTidak = new DevExpress.XtraReports.UI.CalculatedField();
            this.pGetCVPersonalInfoTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.pGetCVPersonalInfoTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repDataset1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel93,
            this.xrLabel92,
            this.xrLabel91,
            this.xrLabel84,
            this.xrLabel85,
            this.xrLabel83,
            this.xrLabel81,
            this.xrLabel82,
            this.xrLabel89,
            this.xrLabel90,
            this.xrLabel88,
            this.xrLabel86,
            this.xrLabel87,
            this.xrLabel80,
            this.xrLabel73,
            this.xrLabel74,
            this.xrLabel72,
            this.xrLabel70,
            this.xrLabel71,
            this.xrLabel78,
            this.xrLabel79,
            this.xrLabel77,
            this.xrLabel75,
            this.xrLabel76,
            this.xrLabel63,
            this.xrLabel64,
            this.xrLabel61,
            this.xrLabel62,
            this.xrLabel65,
            this.xrLabel68,
            this.xrLabel69,
            this.xrLabel66,
            this.xrLabel67,
            this.xrLabel54,
            this.xrLabel55,
            this.xrLabel52,
            this.xrLabel53,
            this.xrLabel56,
            this.xrLabel59,
            this.xrLabel60,
            this.xrLabel57,
            this.xrLabel58,
            this.xrLabel49,
            this.xrLabel48,
            this.xrLabel51,
            this.xrLabel50,
            this.xrLabel47,
            this.xrLabel44,
            this.xrLabel43,
            this.xrLabel46,
            this.xrLabel45,
            this.xrLabel42,
            this.xrLabel41,
            this.xrLabel38,
            this.xrLabel35,
            this.xrLabel34,
            this.xrLabel36,
            this.xrLabel31,
            this.xrLabel30,
            this.xrLabel33,
            this.xrLabel32,
            this.xrLabel29,
            this.xrLabel28,
            this.xrLabel22,
            this.xrLabel23,
            this.xrLabel24,
            this.xrLabel19,
            this.xrLabel20,
            this.xrLabel21,
            this.xrLabel18,
            this.xrLabel17,
            this.xrLabel16,
            this.xrLabel15,
            this.xrLabel14,
            this.xrLabel13,
            this.xrLabel12,
            this.xrLabel11,
            this.xrLabel10,
            this.xrLine3,
            this.xrLabel9,
            this.XRLjawatandata,
            this.XRLtarikhiklandata,
            this.XRLjawatan2,
            this.XRLtarikhiklan2,
            this.XRLjawatan,
            this.XRLtarikhiklan,
            this.xrPictureBox2});
            this.Detail.HeightF = 872.4018F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel93
            // 
            this.xrLabel93.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel93.LocationFloat = new DevExpress.Utils.PointFloat(24.99991F, 849.4016F);
            this.xrLabel93.Multiline = true;
            this.xrLabel93.Name = "xrLabel93";
            this.xrLabel93.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel93.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel93.StylePriority.UseFont = false;
            this.xrLabel93.StylePriority.UseTextAlignment = false;
            this.xrLabel93.Text = "Jentera Berat";
            this.xrLabel93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel93.Visible = false;
            // 
            // xrLabel92
            // 
            this.xrLabel92.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel92.LocationFloat = new DevExpress.Utils.PointFloat(310.6247F, 849.4018F);
            this.xrLabel92.Multiline = true;
            this.xrLabel92.Name = "xrLabel92";
            this.xrLabel92.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel92.SizeF = new System.Drawing.SizeF(329.3749F, 23F);
            this.xrLabel92.StylePriority.UseFont = false;
            this.xrLabel92.StylePriority.UseTextAlignment = false;
            this.xrLabel92.Text = "TIADA";
            this.xrLabel92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel92.Visible = false;
            // 
            // xrLabel91
            // 
            this.xrLabel91.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel91.LocationFloat = new DevExpress.Utils.PointFloat(282.8123F, 849.4016F);
            this.xrLabel91.Name = "xrLabel91";
            this.xrLabel91.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel91.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel91.StylePriority.UseFont = false;
            this.xrLabel91.StylePriority.UseTextAlignment = false;
            this.xrLabel91.Text = ":";
            this.xrLabel91.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel91.Visible = false;
            // 
            // xrLabel84
            // 
            this.xrLabel84.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel84.LocationFloat = new DevExpress.Utils.PointFloat(24.99991F, 803.4016F);
            this.xrLabel84.Multiline = true;
            this.xrLabel84.Name = "xrLabel84";
            this.xrLabel84.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel84.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel84.StylePriority.UseFont = false;
            this.xrLabel84.StylePriority.UseTextAlignment = false;
            this.xrLabel84.Text = "Lesen Khas : GDL";
            this.xrLabel84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel84.Visible = false;
            // 
            // xrLabel85
            // 
            this.xrLabel85.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel85.LocationFloat = new DevExpress.Utils.PointFloat(282.8124F, 803.4016F);
            this.xrLabel85.Name = "xrLabel85";
            this.xrLabel85.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel85.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel85.StylePriority.UseFont = false;
            this.xrLabel85.StylePriority.UseTextAlignment = false;
            this.xrLabel85.Text = ":";
            this.xrLabel85.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel85.Visible = false;
            // 
            // xrLabel83
            // 
            this.xrLabel83.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel83.LocationFloat = new DevExpress.Utils.PointFloat(24.99991F, 826.4016F);
            this.xrLabel83.Multiline = true;
            this.xrLabel83.Name = "xrLabel83";
            this.xrLabel83.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel83.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel83.StylePriority.UseFont = false;
            this.xrLabel83.StylePriority.UseTextAlignment = false;
            this.xrLabel83.Text = "PSV";
            this.xrLabel83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel83.Visible = false;
            // 
            // xrLabel81
            // 
            this.xrLabel81.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pGetCVPersonalInfo.MaritialStatus")});
            this.xrLabel81.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel81.LocationFloat = new DevExpress.Utils.PointFloat(310.6248F, 711.4016F);
            this.xrLabel81.Multiline = true;
            this.xrLabel81.Name = "xrLabel81";
            this.xrLabel81.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel81.SizeF = new System.Drawing.SizeF(329.3749F, 23.00003F);
            this.xrLabel81.StylePriority.UseFont = false;
            this.xrLabel81.StylePriority.UseTextAlignment = false;
            this.xrLabel81.Text = "KAHWIN";
            this.xrLabel81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel82
            // 
            this.xrLabel82.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel82.LocationFloat = new DevExpress.Utils.PointFloat(310.6248F, 826.4019F);
            this.xrLabel82.Multiline = true;
            this.xrLabel82.Name = "xrLabel82";
            this.xrLabel82.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel82.SizeF = new System.Drawing.SizeF(329.3749F, 23F);
            this.xrLabel82.StylePriority.UseFont = false;
            this.xrLabel82.StylePriority.UseTextAlignment = false;
            this.xrLabel82.Text = "TIADA";
            this.xrLabel82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel82.Visible = false;
            // 
            // xrLabel89
            // 
            this.xrLabel89.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel89.LocationFloat = new DevExpress.Utils.PointFloat(24.99991F, 780.4016F);
            this.xrLabel89.Multiline = true;
            this.xrLabel89.Name = "xrLabel89";
            this.xrLabel89.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel89.SizeF = new System.Drawing.SizeF(242.2917F, 23F);
            this.xrLabel89.StylePriority.UseFont = false;
            this.xrLabel89.StylePriority.UseTextAlignment = false;
            this.xrLabel89.Text = "Tarikh Tamat Tempoh";
            this.xrLabel89.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel90
            // 
            this.xrLabel90.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pGetCVPersonalInfo.LicExpiry", "{0:dd/MM/yyyy}")});
            this.xrLabel90.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel90.LocationFloat = new DevExpress.Utils.PointFloat(310.6248F, 780.4017F);
            this.xrLabel90.Multiline = true;
            this.xrLabel90.Name = "xrLabel90";
            this.xrLabel90.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel90.SizeF = new System.Drawing.SizeF(329.3749F, 23.00003F);
            this.xrLabel90.StylePriority.UseFont = false;
            this.xrLabel90.StylePriority.UseTextAlignment = false;
            this.xrLabel90.Text = "10/6/2014";
            this.xrLabel90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel88
            // 
            this.xrLabel88.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel88.LocationFloat = new DevExpress.Utils.PointFloat(282.8125F, 780.4016F);
            this.xrLabel88.Name = "xrLabel88";
            this.xrLabel88.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel88.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel88.StylePriority.UseFont = false;
            this.xrLabel88.StylePriority.UseTextAlignment = false;
            this.xrLabel88.Text = ":";
            this.xrLabel88.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel86
            // 
            this.xrLabel86.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel86.LocationFloat = new DevExpress.Utils.PointFloat(282.8124F, 826.4016F);
            this.xrLabel86.Name = "xrLabel86";
            this.xrLabel86.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel86.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel86.StylePriority.UseFont = false;
            this.xrLabel86.StylePriority.UseTextAlignment = false;
            this.xrLabel86.Text = ":";
            this.xrLabel86.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel86.Visible = false;
            // 
            // xrLabel87
            // 
            this.xrLabel87.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel87.LocationFloat = new DevExpress.Utils.PointFloat(310.6249F, 803.4019F);
            this.xrLabel87.Multiline = true;
            this.xrLabel87.Name = "xrLabel87";
            this.xrLabel87.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel87.SizeF = new System.Drawing.SizeF(329.3749F, 23.00006F);
            this.xrLabel87.StylePriority.UseFont = false;
            this.xrLabel87.StylePriority.UseTextAlignment = false;
            this.xrLabel87.Text = "TIADA";
            this.xrLabel87.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel87.Visible = false;
            // 
            // xrLabel80
            // 
            this.xrLabel80.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel80.LocationFloat = new DevExpress.Utils.PointFloat(24.99991F, 711.4016F);
            this.xrLabel80.Multiline = true;
            this.xrLabel80.Name = "xrLabel80";
            this.xrLabel80.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel80.SizeF = new System.Drawing.SizeF(242.2917F, 23F);
            this.xrLabel80.StylePriority.UseFont = false;
            this.xrLabel80.StylePriority.UseTextAlignment = false;
            this.xrLabel80.Text = "Taraf Perkahwinan";
            this.xrLabel80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel73
            // 
            this.xrLabel73.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pGetCVPersonalInfo.cVehLicence")});
            this.xrLabel73.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel73.LocationFloat = new DevExpress.Utils.PointFloat(310.6248F, 757.4017F);
            this.xrLabel73.Multiline = true;
            this.xrLabel73.Name = "xrLabel73";
            this.xrLabel73.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel73.SizeF = new System.Drawing.SizeF(329.3749F, 23F);
            this.xrLabel73.StylePriority.UseFont = false;
            this.xrLabel73.StylePriority.UseTextAlignment = false;
            this.xrLabel73.Text = "B2,D";
            this.xrLabel73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel74
            // 
            this.xrLabel74.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel74.LocationFloat = new DevExpress.Utils.PointFloat(24.99991F, 757.4016F);
            this.xrLabel74.Multiline = true;
            this.xrLabel74.Name = "xrLabel74";
            this.xrLabel74.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel74.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel74.StylePriority.UseFont = false;
            this.xrLabel74.StylePriority.UseTextAlignment = false;
            this.xrLabel74.Text = "Kelas Lesen";
            this.xrLabel74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel72
            // 
            this.xrLabel72.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pGetCVPersonalInfo.State")});
            this.xrLabel72.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel72.LocationFloat = new DevExpress.Utils.PointFloat(310.6247F, 687.8755F);
            this.xrLabel72.Multiline = true;
            this.xrLabel72.Name = "xrLabel72";
            this.xrLabel72.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel72.SizeF = new System.Drawing.SizeF(329.3749F, 23F);
            this.xrLabel72.StylePriority.UseFont = false;
            this.xrLabel72.StylePriority.UseTextAlignment = false;
            this.xrLabel72.Text = "JOHOR";
            this.xrLabel72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel70
            // 
            this.xrLabel70.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel70.LocationFloat = new DevExpress.Utils.PointFloat(282.8123F, 687.8752F);
            this.xrLabel70.Name = "xrLabel70";
            this.xrLabel70.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel70.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel70.StylePriority.UseFont = false;
            this.xrLabel70.StylePriority.UseTextAlignment = false;
            this.xrLabel70.Text = ":";
            this.xrLabel70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel71
            // 
            this.xrLabel71.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel71.LocationFloat = new DevExpress.Utils.PointFloat(24.99991F, 687.8752F);
            this.xrLabel71.Multiline = true;
            this.xrLabel71.Name = "xrLabel71";
            this.xrLabel71.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel71.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel71.StylePriority.UseFont = false;
            this.xrLabel71.StylePriority.UseTextAlignment = false;
            this.xrLabel71.Text = "Daerah / Negeri Asal";
            this.xrLabel71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel78
            // 
            this.xrLabel78.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pGetCVPersonalInfo.MilikiKenderaan")});
            this.xrLabel78.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel78.LocationFloat = new DevExpress.Utils.PointFloat(310.6249F, 734.4016F);
            this.xrLabel78.Multiline = true;
            this.xrLabel78.Name = "xrLabel78";
            this.xrLabel78.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel78.SizeF = new System.Drawing.SizeF(329.3749F, 23.00006F);
            this.xrLabel78.StylePriority.UseFont = false;
            this.xrLabel78.StylePriority.UseTextAlignment = false;
            this.xrLabel78.Text = "ADA";
            this.xrLabel78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel79
            // 
            this.xrLabel79.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel79.LocationFloat = new DevExpress.Utils.PointFloat(282.8125F, 711.4016F);
            this.xrLabel79.Name = "xrLabel79";
            this.xrLabel79.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel79.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel79.StylePriority.UseFont = false;
            this.xrLabel79.StylePriority.UseTextAlignment = false;
            this.xrLabel79.Text = ":";
            this.xrLabel79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel77
            // 
            this.xrLabel77.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel77.LocationFloat = new DevExpress.Utils.PointFloat(282.8124F, 757.4016F);
            this.xrLabel77.Name = "xrLabel77";
            this.xrLabel77.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel77.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel77.StylePriority.UseFont = false;
            this.xrLabel77.StylePriority.UseTextAlignment = false;
            this.xrLabel77.Text = ":";
            this.xrLabel77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel75
            // 
            this.xrLabel75.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel75.LocationFloat = new DevExpress.Utils.PointFloat(24.99991F, 734.4016F);
            this.xrLabel75.Multiline = true;
            this.xrLabel75.Name = "xrLabel75";
            this.xrLabel75.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel75.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel75.StylePriority.UseFont = false;
            this.xrLabel75.StylePriority.UseTextAlignment = false;
            this.xrLabel75.Text = "Memiliki Kenderaan";
            this.xrLabel75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel76
            // 
            this.xrLabel76.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel76.LocationFloat = new DevExpress.Utils.PointFloat(282.8124F, 734.4016F);
            this.xrLabel76.Name = "xrLabel76";
            this.xrLabel76.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel76.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel76.StylePriority.UseFont = false;
            this.xrLabel76.StylePriority.UseTextAlignment = false;
            this.xrLabel76.Text = ":";
            this.xrLabel76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel63
            // 
            this.xrLabel63.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pGetCVPersonalInfo.Religion")});
            this.xrLabel63.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel63.LocationFloat = new DevExpress.Utils.PointFloat(310.6249F, 618.8754F);
            this.xrLabel63.Multiline = true;
            this.xrLabel63.Name = "xrLabel63";
            this.xrLabel63.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel63.SizeF = new System.Drawing.SizeF(329.3749F, 23.00003F);
            this.xrLabel63.StylePriority.UseFont = false;
            this.xrLabel63.StylePriority.UseTextAlignment = false;
            this.xrLabel63.Text = "HINDU";
            this.xrLabel63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel64
            // 
            this.xrLabel64.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel64.LocationFloat = new DevExpress.Utils.PointFloat(25.00014F, 618.8752F);
            this.xrLabel64.Multiline = true;
            this.xrLabel64.Name = "xrLabel64";
            this.xrLabel64.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel64.SizeF = new System.Drawing.SizeF(242.2917F, 23F);
            this.xrLabel64.StylePriority.UseFont = false;
            this.xrLabel64.StylePriority.UseTextAlignment = false;
            this.xrLabel64.Text = "Agama";
            this.xrLabel64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel61
            // 
            this.xrLabel61.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel61.LocationFloat = new DevExpress.Utils.PointFloat(282.8125F, 618.8752F);
            this.xrLabel61.Name = "xrLabel61";
            this.xrLabel61.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel61.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel61.StylePriority.UseFont = false;
            this.xrLabel61.StylePriority.UseTextAlignment = false;
            this.xrLabel61.Text = ":";
            this.xrLabel61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel62
            // 
            this.xrLabel62.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pGetCVPersonalInfo.Race")});
            this.xrLabel62.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel62.LocationFloat = new DevExpress.Utils.PointFloat(310.6251F, 641.8754F);
            this.xrLabel62.Multiline = true;
            this.xrLabel62.Name = "xrLabel62";
            this.xrLabel62.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel62.SizeF = new System.Drawing.SizeF(329.3749F, 23.00006F);
            this.xrLabel62.StylePriority.UseFont = false;
            this.xrLabel62.StylePriority.UseTextAlignment = false;
            this.xrLabel62.Text = "INDIA";
            this.xrLabel62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel65
            // 
            this.xrLabel65.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel65.LocationFloat = new DevExpress.Utils.PointFloat(282.8124F, 664.8752F);
            this.xrLabel65.Name = "xrLabel65";
            this.xrLabel65.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel65.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel65.StylePriority.UseFont = false;
            this.xrLabel65.StylePriority.UseTextAlignment = false;
            this.xrLabel65.Text = ":";
            this.xrLabel65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel68
            // 
            this.xrLabel68.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel68.LocationFloat = new DevExpress.Utils.PointFloat(282.8124F, 641.8751F);
            this.xrLabel68.Name = "xrLabel68";
            this.xrLabel68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel68.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel68.StylePriority.UseFont = false;
            this.xrLabel68.StylePriority.UseTextAlignment = false;
            this.xrLabel68.Text = ":";
            this.xrLabel68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel69
            // 
            this.xrLabel69.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel69.LocationFloat = new DevExpress.Utils.PointFloat(24.99991F, 641.8751F);
            this.xrLabel69.Multiline = true;
            this.xrLabel69.Name = "xrLabel69";
            this.xrLabel69.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel69.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel69.StylePriority.UseFont = false;
            this.xrLabel69.StylePriority.UseTextAlignment = false;
            this.xrLabel69.Text = "Bangsa";
            this.xrLabel69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel66
            // 
            this.xrLabel66.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel66.LocationFloat = new DevExpress.Utils.PointFloat(24.99991F, 664.8752F);
            this.xrLabel66.Multiline = true;
            this.xrLabel66.Name = "xrLabel66";
            this.xrLabel66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel66.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel66.StylePriority.UseFont = false;
            this.xrLabel66.StylePriority.UseTextAlignment = false;
            this.xrLabel66.Text = "Warganegara";
            this.xrLabel66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel67
            // 
            this.xrLabel67.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pGetCVPersonalInfo.Religion")});
            this.xrLabel67.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel67.LocationFloat = new DevExpress.Utils.PointFloat(310.625F, 664.8754F);
            this.xrLabel67.Multiline = true;
            this.xrLabel67.Name = "xrLabel67";
            this.xrLabel67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel67.SizeF = new System.Drawing.SizeF(329.3749F, 23F);
            this.xrLabel67.StylePriority.UseFont = false;
            this.xrLabel67.StylePriority.UseTextAlignment = false;
            this.xrLabel67.Text = "WARGANEGARA";
            this.xrLabel67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel54
            // 
            this.xrLabel54.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pGetCVPersonalInfo.Gender")});
            this.xrLabel54.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(310.6249F, 549.8752F);
            this.xrLabel54.Multiline = true;
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel54.SizeF = new System.Drawing.SizeF(329.3749F, 23.00003F);
            this.xrLabel54.StylePriority.UseFont = false;
            this.xrLabel54.StylePriority.UseTextAlignment = false;
            this.xrLabel54.Text = "LELAKI";
            this.xrLabel54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel55
            // 
            this.xrLabel55.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel55.LocationFloat = new DevExpress.Utils.PointFloat(25.00014F, 549.8752F);
            this.xrLabel55.Multiline = true;
            this.xrLabel55.Name = "xrLabel55";
            this.xrLabel55.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel55.SizeF = new System.Drawing.SizeF(242.2917F, 23F);
            this.xrLabel55.StylePriority.UseFont = false;
            this.xrLabel55.StylePriority.UseTextAlignment = false;
            this.xrLabel55.Text = "Jantina";
            this.xrLabel55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel52
            // 
            this.xrLabel52.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(282.8125F, 549.8752F);
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel52.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel52.StylePriority.UseFont = false;
            this.xrLabel52.StylePriority.UseTextAlignment = false;
            this.xrLabel52.Text = ":";
            this.xrLabel52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel53
            // 
            this.xrLabel53.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pGetCVPersonalInfo.DOB", "{0:dd/MM/yyyy}")});
            this.xrLabel53.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel53.LocationFloat = new DevExpress.Utils.PointFloat(310.6251F, 572.8753F);
            this.xrLabel53.Multiline = true;
            this.xrLabel53.Name = "xrLabel53";
            this.xrLabel53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel53.SizeF = new System.Drawing.SizeF(329.3749F, 23.00006F);
            this.xrLabel53.StylePriority.UseFont = false;
            this.xrLabel53.StylePriority.UseTextAlignment = false;
            this.xrLabel53.Text = "10/6/1984";
            this.xrLabel53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel56
            // 
            this.xrLabel56.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel56.LocationFloat = new DevExpress.Utils.PointFloat(282.8124F, 595.8752F);
            this.xrLabel56.Name = "xrLabel56";
            this.xrLabel56.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel56.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel56.StylePriority.UseFont = false;
            this.xrLabel56.StylePriority.UseTextAlignment = false;
            this.xrLabel56.Text = ":";
            this.xrLabel56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel59
            // 
            this.xrLabel59.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel59.LocationFloat = new DevExpress.Utils.PointFloat(282.8124F, 572.8752F);
            this.xrLabel59.Name = "xrLabel59";
            this.xrLabel59.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel59.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel59.StylePriority.UseFont = false;
            this.xrLabel59.StylePriority.UseTextAlignment = false;
            this.xrLabel59.Text = ":";
            this.xrLabel59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel60
            // 
            this.xrLabel60.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel60.LocationFloat = new DevExpress.Utils.PointFloat(24.99991F, 572.8752F);
            this.xrLabel60.Multiline = true;
            this.xrLabel60.Name = "xrLabel60";
            this.xrLabel60.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel60.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel60.StylePriority.UseFont = false;
            this.xrLabel60.StylePriority.UseTextAlignment = false;
            this.xrLabel60.Text = "Tarikh Lahir";
            this.xrLabel60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel57
            // 
            this.xrLabel57.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel57.LocationFloat = new DevExpress.Utils.PointFloat(24.99991F, 595.8752F);
            this.xrLabel57.Multiline = true;
            this.xrLabel57.Name = "xrLabel57";
            this.xrLabel57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel57.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel57.StylePriority.UseFont = false;
            this.xrLabel57.StylePriority.UseTextAlignment = false;
            this.xrLabel57.Text = "Umur (Tahun)";
            this.xrLabel57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel58
            // 
            this.xrLabel58.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pGetCVPersonalInfo.Age")});
            this.xrLabel58.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel58.LocationFloat = new DevExpress.Utils.PointFloat(310.625F, 595.8754F);
            this.xrLabel58.Multiline = true;
            this.xrLabel58.Name = "xrLabel58";
            this.xrLabel58.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel58.SizeF = new System.Drawing.SizeF(329.3749F, 23F);
            this.xrLabel58.StylePriority.UseFont = false;
            this.xrLabel58.StylePriority.UseTextAlignment = false;
            this.xrLabel58.Text = "29";
            this.xrLabel58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel49
            // 
            this.xrLabel49.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel49.LocationFloat = new DevExpress.Utils.PointFloat(24.99991F, 503.8752F);
            this.xrLabel49.Multiline = true;
            this.xrLabel49.Name = "xrLabel49";
            this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel49.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel49.StylePriority.UseFont = false;
            this.xrLabel49.StylePriority.UseTextAlignment = false;
            this.xrLabel49.Text = "No. Telefon Rumah";
            this.xrLabel49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel48
            // 
            this.xrLabel48.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel48.LocationFloat = new DevExpress.Utils.PointFloat(282.8123F, 503.8752F);
            this.xrLabel48.Name = "xrLabel48";
            this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel48.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel48.StylePriority.UseFont = false;
            this.xrLabel48.StylePriority.UseTextAlignment = false;
            this.xrLabel48.Text = ":";
            this.xrLabel48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel51
            // 
            this.xrLabel51.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pGetCVPersonalInfo.Hphone")});
            this.xrLabel51.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel51.LocationFloat = new DevExpress.Utils.PointFloat(310.6249F, 526.8751F);
            this.xrLabel51.Multiline = true;
            this.xrLabel51.Name = "xrLabel51";
            this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel51.SizeF = new System.Drawing.SizeF(329.3749F, 23F);
            this.xrLabel51.StylePriority.UseFont = false;
            this.xrLabel51.StylePriority.UseTextAlignment = false;
            this.xrLabel51.Text = "0168177724";
            this.xrLabel51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel50
            // 
            this.xrLabel50.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel50.LocationFloat = new DevExpress.Utils.PointFloat(24.99991F, 526.8751F);
            this.xrLabel50.Multiline = true;
            this.xrLabel50.Name = "xrLabel50";
            this.xrLabel50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel50.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel50.StylePriority.UseFont = false;
            this.xrLabel50.StylePriority.UseTextAlignment = false;
            this.xrLabel50.Text = "No. Telefon Bimbit";
            this.xrLabel50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel47
            // 
            this.xrLabel47.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(282.8123F, 526.8751F);
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel47.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel47.StylePriority.UseFont = false;
            this.xrLabel47.StylePriority.UseTextAlignment = false;
            this.xrLabel47.Text = ":";
            this.xrLabel47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel44
            // 
            this.xrLabel44.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel44.LocationFloat = new DevExpress.Utils.PointFloat(24.99991F, 480.8752F);
            this.xrLabel44.Multiline = true;
            this.xrLabel44.Name = "xrLabel44";
            this.xrLabel44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel44.SizeF = new System.Drawing.SizeF(242.2917F, 23F);
            this.xrLabel44.StylePriority.UseFont = false;
            this.xrLabel44.StylePriority.UseTextAlignment = false;
            this.xrLabel44.Text = "Alamat E-Mel";
            this.xrLabel44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel43
            // 
            this.xrLabel43.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pGetCVPersonalInfo.Email")});
            this.xrLabel43.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(310.6248F, 480.8752F);
            this.xrLabel43.Multiline = true;
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(329.3749F, 23.00003F);
            this.xrLabel43.StylePriority.UseFont = false;
            this.xrLabel43.StylePriority.UseTextAlignment = false;
            this.xrLabel43.Text = "murali@smartsys.com.my";
            this.xrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel46
            // 
            this.xrLabel46.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pGetCVPersonalInfo.Telephone")});
            this.xrLabel46.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(310.6249F, 503.8752F);
            this.xrLabel46.Multiline = true;
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel46.SizeF = new System.Drawing.SizeF(329.3749F, 23.00006F);
            this.xrLabel46.StylePriority.UseFont = false;
            this.xrLabel46.StylePriority.UseTextAlignment = false;
            this.xrLabel46.Text = "07890224";
            this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel45
            // 
            this.xrLabel45.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(282.8124F, 480.8752F);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel45.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel45.StylePriority.UseFont = false;
            this.xrLabel45.StylePriority.UseTextAlignment = false;
            this.xrLabel45.Text = ":";
            this.xrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel42
            // 
            this.xrLabel42.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pGetCVPersonalInfo.OtherState")});
            this.xrLabel42.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel42.LocationFloat = new DevExpress.Utils.PointFloat(310.6249F, 457.8752F);
            this.xrLabel42.Multiline = true;
            this.xrLabel42.Name = "xrLabel42";
            this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel42.SizeF = new System.Drawing.SizeF(329.3749F, 23F);
            this.xrLabel42.StylePriority.UseFont = false;
            this.xrLabel42.StylePriority.UseTextAlignment = false;
            this.xrLabel42.Text = "WILAYAH PERSEKUTUAN KUALA LUMPUR";
            this.xrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel41
            // 
            this.xrLabel41.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel41.LocationFloat = new DevExpress.Utils.PointFloat(24.99991F, 457.8752F);
            this.xrLabel41.Multiline = true;
            this.xrLabel41.Name = "xrLabel41";
            this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel41.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel41.StylePriority.UseFont = false;
            this.xrLabel41.StylePriority.UseTextAlignment = false;
            this.xrLabel41.Text = "Negeri";
            this.xrLabel41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel38
            // 
            this.xrLabel38.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(282.8123F, 457.8752F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel38.StylePriority.UseFont = false;
            this.xrLabel38.StylePriority.UseTextAlignment = false;
            this.xrLabel38.Text = ":";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel35
            // 
            this.xrLabel35.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(24.99991F, 434.8752F);
            this.xrLabel35.Multiline = true;
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(242.2917F, 23F);
            this.xrLabel35.StylePriority.UseFont = false;
            this.xrLabel35.StylePriority.UseTextAlignment = false;
            this.xrLabel35.Text = "Poskod";
            this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel34
            // 
            this.xrLabel34.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pGetCVPersonalInfo.OtherPoscode")});
            this.xrLabel34.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(310.6248F, 434.8751F);
            this.xrLabel34.Multiline = true;
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(329.3749F, 23.00003F);
            this.xrLabel34.StylePriority.UseFont = false;
            this.xrLabel34.StylePriority.UseTextAlignment = false;
            this.xrLabel34.Text = "52100";
            this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel36
            // 
            this.xrLabel36.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(282.8124F, 434.8752F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel36.StylePriority.UseFont = false;
            this.xrLabel36.StylePriority.UseTextAlignment = false;
            this.xrLabel36.Text = ":";
            this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel31
            // 
            this.xrLabel31.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pGetCVPersonalInfo.OtherFullAddress")});
            this.xrLabel31.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(310.6249F, 375.4167F);
            this.xrLabel31.Multiline = true;
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(329.3749F, 59.45837F);
            this.xrLabel31.StylePriority.UseFont = false;
            this.xrLabel31.StylePriority.UseTextAlignment = false;
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel30
            // 
            this.xrLabel30.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pGetCVPersonalInfo.State")});
            this.xrLabel30.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(310.625F, 352.4168F);
            this.xrLabel30.Multiline = true;
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(329.3749F, 23F);
            this.xrLabel30.StylePriority.UseFont = false;
            this.xrLabel30.StylePriority.UseTextAlignment = false;
            this.xrLabel30.Text = "WILAYAH PERSEKUTUAN KUALA LUMPUR";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel33
            // 
            this.xrLabel33.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(282.8124F, 375.4167F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel33.StylePriority.UseFont = false;
            this.xrLabel33.StylePriority.UseTextAlignment = false;
            this.xrLabel33.Text = ":";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel32
            // 
            this.xrLabel32.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(24.99991F, 375.4167F);
            this.xrLabel32.Multiline = true;
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel32.StylePriority.UseFont = false;
            this.xrLabel32.StylePriority.UseTextAlignment = false;
            this.xrLabel32.Text = "Alamat Tetap (Jika Berlainan)";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel29
            // 
            this.xrLabel29.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(24.99991F, 352.4168F);
            this.xrLabel29.Multiline = true;
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel29.StylePriority.UseFont = false;
            this.xrLabel29.StylePriority.UseTextAlignment = false;
            this.xrLabel29.Text = "Negeri";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel28
            // 
            this.xrLabel28.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(282.8124F, 352.4168F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel28.StylePriority.UseFont = false;
            this.xrLabel28.StylePriority.UseTextAlignment = false;
            this.xrLabel28.Text = ":";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(282.8125F, 329.4168F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel22.StylePriority.UseFont = false;
            this.xrLabel22.StylePriority.UseTextAlignment = false;
            this.xrLabel22.Text = ":";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(25.00014F, 329.4168F);
            this.xrLabel23.Multiline = true;
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(242.2917F, 23F);
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "Poskod";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel24
            // 
            this.xrLabel24.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pGetCVPersonalInfo.Poscode")});
            this.xrLabel24.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(310.6249F, 329.4167F);
            this.xrLabel24.Multiline = true;
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(329.3749F, 23.00003F);
            this.xrLabel24.StylePriority.UseFont = false;
            this.xrLabel24.StylePriority.UseTextAlignment = false;
            this.xrLabel24.Text = "52100";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(282.8125F, 286.6249F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = ":";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(24.99991F, 286.6249F);
            this.xrLabel20.Multiline = true;
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "Alamat Surat Menyurat";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel21
            // 
            this.xrLabel21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pGetCVPersonalInfo.FullAddress")});
            this.xrLabel21.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(310.6249F, 286.6251F);
            this.xrLabel21.Multiline = true;
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(329.3749F, 42.79169F);
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel18
            // 
            this.xrLabel18.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pGetCVPersonalInfo.OtherIC")});
            this.xrLabel18.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(310.625F, 263.625F);
            this.xrLabel18.Multiline = true;
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(329.3749F, 23.00003F);
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(25.00014F, 263.625F);
            this.xrLabel17.Multiline = true;
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "No. KP Lama / Polis / Tentera";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(282.8125F, 263.625F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = ":";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel15
            // 
            this.xrLabel15.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pGetCVPersonalInfo.NRIC")});
            this.xrLabel15.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(310.6251F, 240.625F);
            this.xrLabel15.Multiline = true;
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(329.3749F, 23.00003F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "840610016205";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(24.99991F, 240.625F);
            this.xrLabel14.Multiline = true;
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "No. Kad Pengenalan Baru";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel13
            // 
            this.xrLabel13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pGetCVPersonalInfo.Name")});
            this.xrLabel13.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(310.6251F, 203.0833F);
            this.xrLabel13.Multiline = true;
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(329.3749F, 37.54169F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "MURALI A/L PONNUSAMY";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(282.8125F, 240.625F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = ":";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(282.8125F, 203.0833F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = ":";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(21.99994F, 203.0833F);
            this.xrLabel10.Multiline = true;
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(242.2916F, 37.54169F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "Nama Penuh (Seperti Di Dalam Kad\r\nPengenalan)";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine3
            // 
            this.xrLine3.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrLine3.LineWidth = 2;
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(5.000028F, 179.625F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(640.6249F, 2F);
            this.xrLine3.StylePriority.UseBorderDashStyle = false;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(5.000028F, 155.625F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(273.5416F, 24.00002F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "1. MAKLUMAT PERIBADI PEMOHON";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // XRLjawatandata
            // 
            this.XRLjawatandata.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.XRLjawatandata.LocationFloat = new DevExpress.Utils.PointFloat(10.00006F, 73.62504F);
            this.XRLjawatandata.Name = "XRLjawatandata";
            this.XRLjawatandata.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XRLjawatandata.SizeF = new System.Drawing.SizeF(421.875F, 81.99998F);
            this.XRLjawatandata.StylePriority.UseFont = false;
            this.XRLjawatandata.StylePriority.UseTextAlignment = false;
            this.XRLjawatandata.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.XRLjawatandata.Visible = false;
            // 
            // XRLtarikhiklandata
            // 
            this.XRLtarikhiklandata.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.XRLtarikhiklandata.LocationFloat = new DevExpress.Utils.PointFloat(210F, 27.62502F);
            this.XRLtarikhiklandata.Name = "XRLtarikhiklandata";
            this.XRLtarikhiklandata.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XRLtarikhiklandata.SizeF = new System.Drawing.SizeF(88.54166F, 23F);
            this.XRLtarikhiklandata.StylePriority.UseFont = false;
            this.XRLtarikhiklandata.StylePriority.UseTextAlignment = false;
            this.XRLtarikhiklandata.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.XRLtarikhiklandata.Visible = false;
            // 
            // XRLjawatan2
            // 
            this.XRLjawatan2.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.XRLjawatan2.LocationFloat = new DevExpress.Utils.PointFloat(183.9583F, 48.62501F);
            this.XRLjawatan2.Name = "XRLjawatan2";
            this.XRLjawatan2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XRLjawatan2.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.XRLjawatan2.StylePriority.UseFont = false;
            this.XRLjawatan2.StylePriority.UseTextAlignment = false;
            this.XRLjawatan2.Text = ":";
            this.XRLjawatan2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.XRLjawatan2.Visible = false;
            // 
            // XRLtarikhiklan2
            // 
            this.XRLtarikhiklan2.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.XRLtarikhiklan2.LocationFloat = new DevExpress.Utils.PointFloat(183.9583F, 25.62501F);
            this.XRLtarikhiklan2.Name = "XRLtarikhiklan2";
            this.XRLtarikhiklan2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XRLtarikhiklan2.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.XRLtarikhiklan2.StylePriority.UseFont = false;
            this.XRLtarikhiklan2.StylePriority.UseTextAlignment = false;
            this.XRLtarikhiklan2.Text = ":";
            this.XRLtarikhiklan2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.XRLtarikhiklan2.Visible = false;
            // 
            // XRLjawatan
            // 
            this.XRLjawatan.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.XRLjawatan.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 50.62502F);
            this.XRLjawatan.Name = "XRLjawatan";
            this.XRLjawatan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XRLjawatan.SizeF = new System.Drawing.SizeF(157.2917F, 23F);
            this.XRLjawatan.StylePriority.UseFont = false;
            this.XRLjawatan.StylePriority.UseTextAlignment = false;
            this.XRLjawatan.Text = "Jawatan yang Dipohon";
            this.XRLjawatan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.XRLjawatan.Visible = false;
            // 
            // XRLtarikhiklan
            // 
            this.XRLtarikhiklan.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.XRLtarikhiklan.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 27.62502F);
            this.XRLtarikhiklan.Name = "XRLtarikhiklan";
            this.XRLtarikhiklan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XRLtarikhiklan.SizeF = new System.Drawing.SizeF(88.54166F, 23F);
            this.XRLtarikhiklan.StylePriority.UseFont = false;
            this.XRLtarikhiklan.StylePriority.UseTextAlignment = false;
            this.XRLtarikhiklan.Text = "Tarikh Iklan";
            this.XRLtarikhiklan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.XRLtarikhiklan.Visible = false;
            // 
            // xrPictureBox2
            // 
            this.xrPictureBox2.BorderColor = System.Drawing.Color.LightGray;
            this.xrPictureBox2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPictureBox2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("ImageUrl", null, "pGetCVPersonalInfo.imgurl")});
            this.xrPictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox2.Image")));
            this.xrPictureBox2.LocationFloat = new DevExpress.Utils.PointFloat(523.083F, 27.62502F);
            this.xrPictureBox2.Name = "xrPictureBox2";
            this.xrPictureBox2.SizeF = new System.Drawing.SizeF(99F, 128F);
            this.xrPictureBox2.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            this.xrPictureBox2.StylePriority.UseBorderColor = false;
            this.xrPictureBox2.StylePriority.UseBorders = false;
            // 
            // xrLine4
            // 
            this.xrLine4.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrLine4.LineWidth = 2;
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(2.166669F, 52.95842F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(640.6249F, 2F);
            this.xrLine4.StylePriority.UseBorderDashStyle = false;
            // 
            // xrLabel94
            // 
            this.xrLabel94.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel94.LocationFloat = new DevExpress.Utils.PointFloat(2.166669F, 28.95838F);
            this.xrLabel94.Name = "xrLabel94";
            this.xrLabel94.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel94.SizeF = new System.Drawing.SizeF(273.5416F, 24.00002F);
            this.xrLabel94.StylePriority.UseFont = false;
            this.xrLabel94.StylePriority.UseTextAlignment = false;
            this.xrLabel94.Text = "2. MAKLUMAT PASANGAN";
            this.xrLabel94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel96
            // 
            this.xrLabel96.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel96.LocationFloat = new DevExpress.Utils.PointFloat(21.99987F, 9.999974F);
            this.xrLabel96.Multiline = true;
            this.xrLabel96.Name = "xrLabel96";
            this.xrLabel96.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel96.SizeF = new System.Drawing.SizeF(242.2917F, 23F);
            this.xrLabel96.StylePriority.UseFont = false;
            this.xrLabel96.StylePriority.UseTextAlignment = false;
            this.xrLabel96.Text = "Nama Pasangan";
            this.xrLabel96.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel100
            // 
            this.xrLabel100.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel100.LocationFloat = new DevExpress.Utils.PointFloat(279.8122F, 32.99997F);
            this.xrLabel100.Name = "xrLabel100";
            this.xrLabel100.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel100.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel100.StylePriority.UseFont = false;
            this.xrLabel100.StylePriority.UseTextAlignment = false;
            this.xrLabel100.Text = ":";
            this.xrLabel100.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel103
            // 
            this.xrLabel103.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MaklumatPasangan.Hphone")});
            this.xrLabel103.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel103.LocationFloat = new DevExpress.Utils.PointFloat(307.6248F, 56.00006F);
            this.xrLabel103.Multiline = true;
            this.xrLabel103.Name = "xrLabel103";
            this.xrLabel103.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel103.SizeF = new System.Drawing.SizeF(329.3749F, 23F);
            this.xrLabel103.StylePriority.UseFont = false;
            this.xrLabel103.StylePriority.UseTextAlignment = false;
            this.xrLabel103.Text = "xrLabel103";
            this.xrLabel103.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel102
            // 
            this.xrLabel102.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel102.LocationFloat = new DevExpress.Utils.PointFloat(21.99974F, 55.99997F);
            this.xrLabel102.Multiline = true;
            this.xrLabel102.Name = "xrLabel102";
            this.xrLabel102.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel102.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel102.StylePriority.UseFont = false;
            this.xrLabel102.StylePriority.UseTextAlignment = false;
            this.xrLabel102.Text = "No. Telefon Bimbit";
            this.xrLabel102.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel99
            // 
            this.xrLabel99.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel99.LocationFloat = new DevExpress.Utils.PointFloat(279.8122F, 55.99997F);
            this.xrLabel99.Name = "xrLabel99";
            this.xrLabel99.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel99.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel99.StylePriority.UseFont = false;
            this.xrLabel99.StylePriority.UseTextAlignment = false;
            this.xrLabel99.Text = ":";
            this.xrLabel99.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel101
            // 
            this.xrLabel101.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel101.LocationFloat = new DevExpress.Utils.PointFloat(21.99974F, 32.99997F);
            this.xrLabel101.Multiline = true;
            this.xrLabel101.Name = "xrLabel101";
            this.xrLabel101.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel101.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel101.StylePriority.UseFont = false;
            this.xrLabel101.StylePriority.UseTextAlignment = false;
            this.xrLabel101.Text = "No. Telefon Rumah";
            this.xrLabel101.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel95
            // 
            this.xrLabel95.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MaklumatPasangan.Name")});
            this.xrLabel95.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel95.LocationFloat = new DevExpress.Utils.PointFloat(307.6247F, 9.999974F);
            this.xrLabel95.Multiline = true;
            this.xrLabel95.Name = "xrLabel95";
            this.xrLabel95.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel95.SizeF = new System.Drawing.SizeF(329.3749F, 23.00003F);
            this.xrLabel95.StylePriority.UseFont = false;
            this.xrLabel95.StylePriority.UseTextAlignment = false;
            this.xrLabel95.Text = "xrLabel95";
            this.xrLabel95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel98
            // 
            this.xrLabel98.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MaklumatPasangan.phone")});
            this.xrLabel98.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel98.LocationFloat = new DevExpress.Utils.PointFloat(307.6248F, 33F);
            this.xrLabel98.Multiline = true;
            this.xrLabel98.Name = "xrLabel98";
            this.xrLabel98.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel98.SizeF = new System.Drawing.SizeF(329.3749F, 23.00006F);
            this.xrLabel98.StylePriority.UseFont = false;
            this.xrLabel98.StylePriority.UseTextAlignment = false;
            this.xrLabel98.Text = "0333452167";
            this.xrLabel98.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel97
            // 
            this.xrLabel97.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel97.LocationFloat = new DevExpress.Utils.PointFloat(279.8123F, 9.999974F);
            this.xrLabel97.Name = "xrLabel97";
            this.xrLabel97.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel97.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel97.StylePriority.UseFont = false;
            this.xrLabel97.StylePriority.UseTextAlignment = false;
            this.xrLabel97.Text = ":";
            this.xrLabel97.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel109
            // 
            this.xrLabel109.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel109.LocationFloat = new DevExpress.Utils.PointFloat(22.9997F, 102F);
            this.xrLabel109.Multiline = true;
            this.xrLabel109.Name = "xrLabel109";
            this.xrLabel109.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel109.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel109.StylePriority.UseFont = false;
            this.xrLabel109.StylePriority.UseTextAlignment = false;
            this.xrLabel109.Text = "Pekerjaan";
            this.xrLabel109.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel110
            // 
            this.xrLabel110.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MaklumatWaris.Occupation")});
            this.xrLabel110.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel110.LocationFloat = new DevExpress.Utils.PointFloat(308.6246F, 102.0001F);
            this.xrLabel110.Multiline = true;
            this.xrLabel110.Name = "xrLabel110";
            this.xrLabel110.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel110.SizeF = new System.Drawing.SizeF(329.3749F, 23F);
            this.xrLabel110.StylePriority.UseFont = false;
            this.xrLabel110.StylePriority.UseTextAlignment = false;
            this.xrLabel110.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel111
            // 
            this.xrLabel111.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel111.LocationFloat = new DevExpress.Utils.PointFloat(280.8121F, 78.99997F);
            this.xrLabel111.Name = "xrLabel111";
            this.xrLabel111.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel111.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel111.StylePriority.UseFont = false;
            this.xrLabel111.StylePriority.UseTextAlignment = false;
            this.xrLabel111.Text = ":";
            this.xrLabel111.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel108
            // 
            this.xrLabel108.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel108.LocationFloat = new DevExpress.Utils.PointFloat(280.8121F, 102F);
            this.xrLabel108.Name = "xrLabel108";
            this.xrLabel108.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel108.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel108.StylePriority.UseFont = false;
            this.xrLabel108.StylePriority.UseTextAlignment = false;
            this.xrLabel108.Text = ":";
            this.xrLabel108.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel105
            // 
            this.xrLabel105.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MaklumatWaris.Employer")});
            this.xrLabel105.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel105.LocationFloat = new DevExpress.Utils.PointFloat(308.6248F, 79.00006F);
            this.xrLabel105.Multiline = true;
            this.xrLabel105.Name = "xrLabel105";
            this.xrLabel105.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel105.SizeF = new System.Drawing.SizeF(329.3749F, 23.00006F);
            this.xrLabel105.StylePriority.UseFont = false;
            this.xrLabel105.StylePriority.UseTextAlignment = false;
            this.xrLabel105.Text = "-";
            this.xrLabel105.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel107
            // 
            this.xrLabel107.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel107.LocationFloat = new DevExpress.Utils.PointFloat(22.9997F, 78.99997F);
            this.xrLabel107.Multiline = true;
            this.xrLabel107.Name = "xrLabel107";
            this.xrLabel107.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel107.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel107.StylePriority.UseFont = false;
            this.xrLabel107.StylePriority.UseTextAlignment = false;
            this.xrLabel107.Text = "Majikan Pasangan";
            this.xrLabel107.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel114
            // 
            this.xrLabel114.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel114.LocationFloat = new DevExpress.Utils.PointFloat(23.99991F, 125F);
            this.xrLabel114.Multiline = true;
            this.xrLabel114.Name = "xrLabel114";
            this.xrLabel114.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel114.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel114.StylePriority.UseFont = false;
            this.xrLabel114.StylePriority.UseTextAlignment = false;
            this.xrLabel114.Text = "Nama Syarikat / Majikan / Jabatan";
            this.xrLabel114.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel115
            // 
            this.xrLabel115.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MaklumatWaris.CompanyName")});
            this.xrLabel115.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel115.LocationFloat = new DevExpress.Utils.PointFloat(309.6247F, 125.0001F);
            this.xrLabel115.Multiline = true;
            this.xrLabel115.Name = "xrLabel115";
            this.xrLabel115.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel115.SizeF = new System.Drawing.SizeF(329.3749F, 23.00003F);
            this.xrLabel115.StylePriority.UseFont = false;
            this.xrLabel115.StylePriority.UseTextAlignment = false;
            this.xrLabel115.Text = "LEMBAGA HASIL DALAM NEGERI MALAYSIA";
            this.xrLabel115.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel113
            // 
            this.xrLabel113.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel113.LocationFloat = new DevExpress.Utils.PointFloat(281.8123F, 125F);
            this.xrLabel113.Name = "xrLabel113";
            this.xrLabel113.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel113.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel113.StylePriority.UseFont = false;
            this.xrLabel113.StylePriority.UseTextAlignment = false;
            this.xrLabel113.Text = ":";
            this.xrLabel113.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel116
            // 
            this.xrLabel116.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MaklumatWaris.CompanyAddress")});
            this.xrLabel116.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel116.LocationFloat = new DevExpress.Utils.PointFloat(309.6247F, 148.0002F);
            this.xrLabel116.Multiline = true;
            this.xrLabel116.Name = "xrLabel116";
            this.xrLabel116.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel116.SizeF = new System.Drawing.SizeF(329.3749F, 42.79169F);
            this.xrLabel116.StylePriority.UseFont = false;
            this.xrLabel116.StylePriority.UseTextAlignment = false;
            this.xrLabel116.Text = "LOT 21, BANGUNAN HASIL\r\nPERSIARAN RIMBA";
            this.xrLabel116.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel118
            // 
            this.xrLabel118.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel118.LocationFloat = new DevExpress.Utils.PointFloat(281.8123F, 148F);
            this.xrLabel118.Name = "xrLabel118";
            this.xrLabel118.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel118.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel118.StylePriority.UseFont = false;
            this.xrLabel118.StylePriority.UseTextAlignment = false;
            this.xrLabel118.Text = ":";
            this.xrLabel118.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel117
            // 
            this.xrLabel117.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel117.LocationFloat = new DevExpress.Utils.PointFloat(23.99991F, 148F);
            this.xrLabel117.Multiline = true;
            this.xrLabel117.Name = "xrLabel117";
            this.xrLabel117.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel117.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel117.StylePriority.UseFont = false;
            this.xrLabel117.StylePriority.UseTextAlignment = false;
            this.xrLabel117.Text = "Alamat Syarikat / Majikan / Jabatan";
            this.xrLabel117.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 50F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(267.7083F, 1F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(107.2917F, 71.54167F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine2,
            this.xrLine1,
            this.xrLabel2,
            this.xrLabel1,
            this.xrPictureBox1});
            this.ReportHeader.HeightF = 139.5833F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrLine2
            // 
            this.xrLine2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(3.999996F, 126.5417F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(640.6249F, 2F);
            this.xrLine2.StylePriority.UseBorderDashStyle = false;
            // 
            // xrLine1
            // 
            this.xrLine1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(4.166667F, 123.5417F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(640.625F, 2F);
            this.xrLine1.StylePriority.UseBorderDashStyle = false;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 97.54168F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(614.5834F, 23F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "BORANG PERMOHONAN PEKERJAAN";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 74.54167F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(614.5833F, 23F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "LEMBAGA HASIL DALAM NEGERI MALAYSIA";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Expanded = false;
            this.ReportFooter.HeightF = 62.48436F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLabel128
            // 
            this.xrLabel128.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel128.LocationFloat = new DevExpress.Utils.PointFloat(5.000003F, 33.33333F);
            this.xrLabel128.Name = "xrLabel128";
            this.xrLabel128.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel128.SizeF = new System.Drawing.SizeF(273.5416F, 24.00002F);
            this.xrLabel128.StylePriority.UseFont = false;
            this.xrLabel128.StylePriority.UseTextAlignment = false;
            this.xrLabel128.Text = "3. MAKLUMAT WARIS";
            this.xrLabel128.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine5
            // 
            this.xrLine5.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrLine5.LineWidth = 2;
            this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(5.000003F, 57.33337F);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.SizeF = new System.Drawing.SizeF(640.6249F, 2F);
            this.xrLine5.StylePriority.UseBorderDashStyle = false;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(5.000003F, 72.38287F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(639.6249F, 25F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell6,
            this.xrTableCell2,
            this.xrTableCell5,
            this.xrTableCell3});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "Hubungan";
            this.xrTableCell1.Weight = 0.43873366292559413D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "Nama";
            this.xrTableCell6.Weight = 1.0969311491882099D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "Warganegara";
            this.xrTableCell2.Weight = 0.46267367646616786D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "Pekerjaan";
            this.xrTableCell5.Weight = 0.49511408577509769D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "No.Telefon";
            this.xrTableCell3.Weight = 0.50654742564493072D;
            // 
            // Maklumat_Waris
            // 
            this.Maklumat_Waris.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.GroupHeader1,
            this.GroupFooter1});
            this.Maklumat_Waris.DataAdapter = this.maklumatWarisTableAdapter;
            this.Maklumat_Waris.DataMember = "MaklumatWaris";
            this.Maklumat_Waris.DataSource = this.repDataset1;
            this.Maklumat_Waris.Expanded = false;
            this.Maklumat_Waris.Level = 1;
            this.Maklumat_Waris.Name = "Maklumat_Waris";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail1.HeightF = 25F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(5F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable2.SizeF = new System.Drawing.SizeF(639.6249F, 25F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21,
            this.xrTableCell22,
            this.xrTableCell23,
            this.xrTableCell24,
            this.xrTableCell25});
            this.xrTableRow5.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.StylePriority.UseFont = false;
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MaklumatWaris.Relation")});
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Text = "BAPA";
            this.xrTableCell21.Weight = 0.43873362714183678D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MaklumatWaris.Name")});
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Text = "PONNUSAMY A/L KUPPA VASU DEWAN";
            this.xrTableCell22.Weight = 1.0969311849719672D;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MaklumatWaris.Nationality")});
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Text = "WARGANEGARA";
            this.xrTableCell23.Weight = 0.46267367646616786D;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MaklumatWaris.Occupation")});
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Text = "BERSARA";
            this.xrTableCell24.Weight = 0.49511408577509769D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MaklumatWaris.Hphone")});
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Text = "0127789012";
            this.xrTableCell25.Weight = 0.50654742564493072D;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel128,
            this.xrLine5,
            this.xrTable1});
            this.GroupHeader1.HeightF = 97.38287F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.HeightF = 26.04167F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // maklumatWarisTableAdapter
            // 
            this.maklumatWarisTableAdapter.ClearBeforeFill = true;
            // 
            // repDataset1
            // 
            this.repDataset1.DataSetName = "RepDataset";
            this.repDataset1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // Maklumat_KesihatanFizikal
            // 
            this.Maklumat_KesihatanFizikal.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2,
            this.GroupHeader2,
            this.GroupFooter2});
            this.Maklumat_KesihatanFizikal.DataAdapter = this.pGetCVPhysicalHealthinfoTableAdapter;
            this.Maklumat_KesihatanFizikal.DataMember = "pGetCVPhysicalHealthinfo";
            this.Maklumat_KesihatanFizikal.DataSource = this.repDataset1;
            this.Maklumat_KesihatanFizikal.Expanded = false;
            this.Maklumat_KesihatanFizikal.Level = 2;
            this.Maklumat_KesihatanFizikal.Name = "Maklumat_KesihatanFizikal";
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel104,
            this.xrLabel305,
            this.xrLabel136,
            this.xrCheckBox2});
            this.Detail2.HeightF = 23.00003F;
            this.Detail2.Name = "Detail2";
            // 
            // xrLabel104
            // 
            this.xrLabel104.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pGetCVPhysicalHealthinfo.cRemarks")});
            this.xrLabel104.LocationFloat = new DevExpress.Utils.PointFloat(353.0062F, 0F);
            this.xrLabel104.Name = "xrLabel104";
            this.xrLabel104.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel104.SizeF = new System.Drawing.SizeF(277.3265F, 23F);
            this.xrLabel104.Text = "xrLabel104";
            // 
            // xrLabel305
            // 
            this.xrLabel305.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel305.LocationFloat = new DevExpress.Utils.PointFloat(332.4165F, 3.178914E-05F);
            this.xrLabel305.Name = "xrLabel305";
            this.xrLabel305.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel305.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel305.StylePriority.UseFont = false;
            this.xrLabel305.StylePriority.UseTextAlignment = false;
            this.xrLabel305.Text = ":";
            this.xrLabel305.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel136
            // 
            this.xrLabel136.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pGetCVPhysicalHealthinfo.cQuestion")});
            this.xrLabel136.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel136.LocationFloat = new DevExpress.Utils.PointFloat(68.54169F, 0F);
            this.xrLabel136.Multiline = true;
            this.xrLabel136.Name = "xrLabel136";
            this.xrLabel136.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel136.SizeF = new System.Drawing.SizeF(263.8748F, 23.00003F);
            this.xrLabel136.StylePriority.UseFont = false;
            this.xrLabel136.StylePriority.UseTextAlignment = false;
            this.xrLabel136.Text = "Batuk Kering / Tibi";
            this.xrLabel136.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrCheckBox2
            // 
            this.xrCheckBox2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("CheckState", null, "pGetCVPhysicalHealthinfo.Tanda")});
            this.xrCheckBox2.LocationFloat = new DevExpress.Utils.PointFloat(18.00001F, 0F);
            this.xrCheckBox2.Name = "xrCheckBox2";
            this.xrCheckBox2.SizeF = new System.Drawing.SizeF(18.75F, 23.00001F);
            this.xrCheckBox2.StylePriority.UseTextAlignment = false;
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel135,
            this.xrLabel134,
            this.xrLabel133,
            this.xrLabel132,
            this.xrCheckBox1,
            this.xrLabel131,
            this.xrLabel130,
            this.xrLabel129,
            this.xrLine6});
            this.GroupHeader2.HeightF = 184.9167F;
            this.GroupHeader2.Name = "GroupHeader2";
            // 
            // xrLabel135
            // 
            this.xrLabel135.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Underline);
            this.xrLabel135.LocationFloat = new DevExpress.Utils.PointFloat(349.4372F, 161.9167F);
            this.xrLabel135.Multiline = true;
            this.xrLabel135.Name = "xrLabel135";
            this.xrLabel135.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel135.SizeF = new System.Drawing.SizeF(280.8955F, 23.00003F);
            this.xrLabel135.StylePriority.UseFont = false;
            this.xrLabel135.StylePriority.UseTextAlignment = false;
            this.xrLabel135.Text = "Butir-butir Lanjut";
            this.xrLabel135.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel134
            // 
            this.xrLabel134.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Underline);
            this.xrLabel134.LocationFloat = new DevExpress.Utils.PointFloat(68.54169F, 161.9167F);
            this.xrLabel134.Multiline = true;
            this.xrLabel134.Name = "xrLabel134";
            this.xrLabel134.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel134.SizeF = new System.Drawing.SizeF(280.8955F, 23.00003F);
            this.xrLabel134.StylePriority.UseFont = false;
            this.xrLabel134.StylePriority.UseTextAlignment = false;
            this.xrLabel134.Text = "Penyakit";
            this.xrLabel134.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel133
            // 
            this.xrLabel133.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Underline);
            this.xrLabel133.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 161.9167F);
            this.xrLabel133.Multiline = true;
            this.xrLabel133.Name = "xrLabel133";
            this.xrLabel133.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel133.SizeF = new System.Drawing.SizeF(42.49999F, 23.00003F);
            this.xrLabel133.StylePriority.UseFont = false;
            this.xrLabel133.StylePriority.UseTextAlignment = false;
            this.xrLabel133.Text = "Pilih";
            this.xrLabel133.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel132
            // 
            this.xrLabel132.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel132.LocationFloat = new DevExpress.Utils.PointFloat(100.8333F, 118.1667F);
            this.xrLabel132.Multiline = true;
            this.xrLabel132.Name = "xrLabel132";
            this.xrLabel132.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel132.SizeF = new System.Drawing.SizeF(539.1663F, 23.00003F);
            this.xrLabel132.StylePriority.UseFont = false;
            this.xrLabel132.StylePriority.UseTextAlignment = false;
            this.xrLabel132.Text = "pada kotak yang berkenaan dan sila nyatakan butir-butir lanjut.";
            this.xrLabel132.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrCheckBox1
            // 
            this.xrCheckBox1.Checked = true;
            this.xrCheckBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.xrCheckBox1.LocationFloat = new DevExpress.Utils.PointFloat(82.08332F, 118.1667F);
            this.xrCheckBox1.Name = "xrCheckBox1";
            this.xrCheckBox1.SizeF = new System.Drawing.SizeF(18.75F, 23.00001F);
            this.xrCheckBox1.StylePriority.UseTextAlignment = false;
            // 
            // xrLabel131
            // 
            this.xrLabel131.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel131.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 118.1667F);
            this.xrLabel131.Multiline = true;
            this.xrLabel131.Name = "xrLabel131";
            this.xrLabel131.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel131.SizeF = new System.Drawing.SizeF(69.58332F, 23.00003F);
            this.xrLabel131.StylePriority.UseFont = false;
            this.xrLabel131.StylePriority.UseTextAlignment = false;
            this.xrLabel131.Text = "Tandakan";
            this.xrLabel131.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel130
            // 
            this.xrLabel130.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel130.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 87.95833F);
            this.xrLabel130.Multiline = true;
            this.xrLabel130.Name = "xrLabel130";
            this.xrLabel130.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel130.SizeF = new System.Drawing.SizeF(630F, 23.00003F);
            this.xrLabel130.StylePriority.UseFont = false;
            this.xrLabel130.StylePriority.UseTextAlignment = false;
            this.xrLabel130.Text = "1. Adakah anda pernah / sedang menghidap penyakit-penyakit berikut :-";
            this.xrLabel130.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel129
            // 
            this.xrLabel129.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel129.LocationFloat = new DevExpress.Utils.PointFloat(4.687531F, 43.16666F);
            this.xrLabel129.Name = "xrLabel129";
            this.xrLabel129.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel129.SizeF = new System.Drawing.SizeF(273.5416F, 24.00002F);
            this.xrLabel129.StylePriority.UseFont = false;
            this.xrLabel129.StylePriority.UseTextAlignment = false;
            this.xrLabel129.Text = "4. MAKLUMAT KESIHATAN / FIZIKAL";
            this.xrLabel129.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine6
            // 
            this.xrLine6.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrLine6.LineWidth = 2;
            this.xrLine6.LocationFloat = new DevExpress.Utils.PointFloat(4.687531F, 67.16669F);
            this.xrLine6.Name = "xrLine6";
            this.xrLine6.SizeF = new System.Drawing.SizeF(640.6249F, 2F);
            this.xrLine6.StylePriority.UseBorderDashStyle = false;
            // 
            // GroupFooter2
            // 
            this.GroupFooter2.HeightF = 13.54167F;
            this.GroupFooter2.Name = "GroupFooter2";
            // 
            // pGetCVPhysicalHealthinfoTableAdapter
            // 
            this.pGetCVPhysicalHealthinfoTableAdapter.ClearBeforeFill = true;
            // 
            // Kemahiran_Bahasa
            // 
            this.Kemahiran_Bahasa.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail3,
            this.GroupHeader3,
            this.GroupFooter3});
            this.Kemahiran_Bahasa.DataAdapter = this.maklumatKemahiranBahasaTableAdapter;
            this.Kemahiran_Bahasa.DataMember = "maklumatKemahiranBahasa";
            this.Kemahiran_Bahasa.DataSource = this.repDataset1;
            this.Kemahiran_Bahasa.Expanded = false;
            this.Kemahiran_Bahasa.Level = 4;
            this.Kemahiran_Bahasa.Name = "Kemahiran_Bahasa";
            // 
            // Detail3
            // 
            this.Detail3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrCheckBox5,
            this.xrCheckBox3,
            this.xrLabel142,
            this.xrLabel141,
            this.xrCheckBox7});
            this.Detail3.HeightF = 30.91679F;
            this.Detail3.Name = "Detail3";
            // 
            // xrCheckBox5
            // 
            this.xrCheckBox5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrCheckBox5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("CheckState", null, "maklumatKemahiranBahasa.BahasaSederhana")});
            this.xrCheckBox5.LocationFloat = new DevExpress.Utils.PointFloat(485.4167F, 4.00001F);
            this.xrCheckBox5.Name = "xrCheckBox5";
            this.xrCheckBox5.SizeF = new System.Drawing.SizeF(18.75F, 23.00001F);
            this.xrCheckBox5.StylePriority.UseBorders = false;
            this.xrCheckBox5.StylePriority.UseTextAlignment = false;
            // 
            // xrCheckBox3
            // 
            this.xrCheckBox3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrCheckBox3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("CheckState", null, "maklumatKemahiranBahasa.BahasaLemah")});
            this.xrCheckBox3.LocationFloat = new DevExpress.Utils.PointFloat(391.6667F, 4.0001F);
            this.xrCheckBox3.Name = "xrCheckBox3";
            this.xrCheckBox3.SizeF = new System.Drawing.SizeF(18.75F, 23.00001F);
            this.xrCheckBox3.StylePriority.UseBorders = false;
            this.xrCheckBox3.StylePriority.UseTextAlignment = false;
            // 
            // xrLabel142
            // 
            this.xrLabel142.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatKemahiranBahasa.cOtherCompetency")});
            this.xrLabel142.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel142.LocationFloat = new DevExpress.Utils.PointFloat(238.0628F, 4.0001F);
            this.xrLabel142.Multiline = true;
            this.xrLabel142.Name = "xrLabel142";
            this.xrLabel142.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel142.SizeF = new System.Drawing.SizeF(111.3744F, 23F);
            this.xrLabel142.StylePriority.UseFont = false;
            this.xrLabel142.StylePriority.UseTextAlignment = false;
            this.xrLabel142.Text = "Pertuturan";
            this.xrLabel142.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel141
            // 
            this.xrLabel141.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatKemahiranBahasa.cDescription")});
            this.xrLabel141.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel141.LocationFloat = new DevExpress.Utils.PointFloat(3.041585F, 4.000102F);
            this.xrLabel141.Multiline = true;
            this.xrLabel141.Name = "xrLabel141";
            this.xrLabel141.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel141.SizeF = new System.Drawing.SizeF(215.8333F, 23.00001F);
            this.xrLabel141.StylePriority.UseFont = false;
            this.xrLabel141.StylePriority.UseTextAlignment = false;
            this.xrLabel141.Text = "Bahasa Melayu";
            this.xrLabel141.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrCheckBox7
            // 
            this.xrCheckBox7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrCheckBox7.Checked = true;
            this.xrCheckBox7.CheckState = System.Windows.Forms.CheckState.Checked;
            this.xrCheckBox7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("CheckState", null, "maklumatKemahiranBahasa.BahasaBaik")});
            this.xrCheckBox7.ForeColor = System.Drawing.Color.Black;
            this.xrCheckBox7.LocationFloat = new DevExpress.Utils.PointFloat(567.7083F, 4.0001F);
            this.xrCheckBox7.Name = "xrCheckBox7";
            this.xrCheckBox7.SizeF = new System.Drawing.SizeF(18.75F, 23.00001F);
            this.xrCheckBox7.StylePriority.UseBorders = false;
            this.xrCheckBox7.StylePriority.UseForeColor = false;
            this.xrCheckBox7.StylePriority.UseTextAlignment = false;
            // 
            // GroupHeader3
            // 
            this.GroupHeader3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel140,
            this.xrLabel139,
            this.xrLabel138,
            this.xrLabel137,
            this.xrLine7});
            this.GroupHeader3.HeightF = 122.625F;
            this.GroupHeader3.Name = "GroupHeader3";
            // 
            // xrLabel140
            // 
            this.xrLabel140.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel140.LocationFloat = new DevExpress.Utils.PointFloat(560.4165F, 99.62495F);
            this.xrLabel140.Multiline = true;
            this.xrLabel140.Name = "xrLabel140";
            this.xrLabel140.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel140.SizeF = new System.Drawing.SizeF(78.54156F, 23F);
            this.xrLabel140.StylePriority.UseFont = false;
            this.xrLabel140.StylePriority.UseTextAlignment = false;
            this.xrLabel140.Text = "Baik";
            this.xrLabel140.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel139
            // 
            this.xrLabel139.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel139.LocationFloat = new DevExpress.Utils.PointFloat(460.354F, 99.62495F);
            this.xrLabel139.Multiline = true;
            this.xrLabel139.Name = "xrLabel139";
            this.xrLabel139.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel139.SizeF = new System.Drawing.SizeF(78.54156F, 23F);
            this.xrLabel139.StylePriority.UseFont = false;
            this.xrLabel139.StylePriority.UseTextAlignment = false;
            this.xrLabel139.Text = "Sederhana";
            this.xrLabel139.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel138
            // 
            this.xrLabel138.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel138.LocationFloat = new DevExpress.Utils.PointFloat(369.9792F, 99.62495F);
            this.xrLabel138.Multiline = true;
            this.xrLabel138.Name = "xrLabel138";
            this.xrLabel138.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel138.SizeF = new System.Drawing.SizeF(78.54156F, 23F);
            this.xrLabel138.StylePriority.UseFont = false;
            this.xrLabel138.StylePriority.UseTextAlignment = false;
            this.xrLabel138.Text = "Lemah";
            this.xrLabel138.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel137
            // 
            this.xrLabel137.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel137.LocationFloat = new DevExpress.Utils.PointFloat(6.958326F, 61.08341F);
            this.xrLabel137.Name = "xrLabel137";
            this.xrLabel137.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel137.SizeF = new System.Drawing.SizeF(273.5416F, 24.00002F);
            this.xrLabel137.StylePriority.UseFont = false;
            this.xrLabel137.StylePriority.UseTextAlignment = false;
            this.xrLabel137.Text = "5. KEMAHIRAN BAHASA";
            this.xrLabel137.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine7
            // 
            this.xrLine7.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrLine7.LineWidth = 2;
            this.xrLine7.LocationFloat = new DevExpress.Utils.PointFloat(6.958326F, 85.08333F);
            this.xrLine7.Name = "xrLine7";
            this.xrLine7.SizeF = new System.Drawing.SizeF(640.6249F, 2F);
            this.xrLine7.StylePriority.UseBorderDashStyle = false;
            // 
            // GroupFooter3
            // 
            this.GroupFooter3.HeightF = 26.04167F;
            this.GroupFooter3.Name = "GroupFooter3";
            // 
            // maklumatKemahiranBahasaTableAdapter
            // 
            this.maklumatKemahiranBahasaTableAdapter.ClearBeforeFill = true;
            // 
            // noStaffbasedongrpTableAdapter
            // 
            this.noStaffbasedongrpTableAdapter.ClearBeforeFill = true;
            // 
            // Maklumat_Persekolahan
            // 
            this.Maklumat_Persekolahan.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail4,
            this.GroupHeader4,
            this.GroupFooter4});
            this.Maklumat_Persekolahan.DataAdapter = this.maklumatSekolahTableAdapter;
            this.Maklumat_Persekolahan.DataMember = "maklumatSekolah";
            this.Maklumat_Persekolahan.DataSource = this.repDataset1;
            this.Maklumat_Persekolahan.Expanded = false;
            this.Maklumat_Persekolahan.Level = 5;
            this.Maklumat_Persekolahan.Name = "Maklumat_Persekolahan";
            // 
            // Detail4
            // 
            this.Detail4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.Detail4.HeightF = 43.75F;
            this.Detail4.Name = "Detail4";
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(4F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable4.SizeF = new System.Drawing.SizeF(639.6249F, 43.75F);
            this.xrTable4.StylePriority.UseBorders = false;
            this.xrTable4.StylePriority.UseFont = false;
            this.xrTable4.StylePriority.UseTextAlignment = false;
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell11,
            this.xrTableCell12,
            this.xrTableCell13});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatSekolah.cOtherInstitute")});
            this.xrTableCell9.Multiline = true;
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Text = "SEKOLAH MENENGAH KEBANGSAAN\r\nLINGGU";
            this.xrTableCell9.Weight = 1.114519717640968D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatSekolah.TahunSekolah")});
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Text = "1997 - 2001";
            this.xrTableCell11.Weight = 0.42114509447283577D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatSekolah.cOtherQualification")});
            this.xrTableCell12.Multiline = true;
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Text = "SIJIL TINGGI PELAJARAN MALAYSIA\r\n(STPM)";
            this.xrTableCell12.Weight = 0.99677539767460166D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatSekolah.cCodeGrade")});
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Text = "1A,3B";
            this.xrTableCell13.Weight = 0.46755979021159466D;
            // 
            // GroupHeader4
            // 
            this.GroupHeader4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3,
            this.xrLine8,
            this.xrLabel144});
            this.GroupHeader4.HeightF = 129.6042F;
            this.GroupHeader4.Name = "GroupHeader4";
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(4F, 104.6042F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable3.SizeF = new System.Drawing.SizeF(639.6249F, 25F);
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseFont = false;
            this.xrTable3.StylePriority.UseTextAlignment = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell10});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "Nama Sekolah";
            this.xrTableCell4.Weight = 1.114519717640968D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Text = "Tahun";
            this.xrTableCell7.Weight = 0.42114509447283577D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Text = "Peperiksaan";
            this.xrTableCell8.Weight = 0.99677539767460166D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Text = "Pangkat / Gred";
            this.xrTableCell10.Weight = 0.46755979021159466D;
            // 
            // xrLine8
            // 
            this.xrLine8.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrLine8.LineWidth = 2;
            this.xrLine8.LocationFloat = new DevExpress.Utils.PointFloat(4.687561F, 89.60421F);
            this.xrLine8.Name = "xrLine8";
            this.xrLine8.SizeF = new System.Drawing.SizeF(640.6249F, 2F);
            this.xrLine8.StylePriority.UseBorderDashStyle = false;
            // 
            // xrLabel144
            // 
            this.xrLabel144.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel144.LocationFloat = new DevExpress.Utils.PointFloat(4.687561F, 65.60417F);
            this.xrLabel144.Name = "xrLabel144";
            this.xrLabel144.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel144.SizeF = new System.Drawing.SizeF(273.5416F, 24.00002F);
            this.xrLabel144.StylePriority.UseFont = false;
            this.xrLabel144.StylePriority.UseTextAlignment = false;
            this.xrLabel144.Text = "6. MAKLUMAT PERSEKOLAHAN";
            this.xrLabel144.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // GroupFooter4
            // 
            this.GroupFooter4.HeightF = 46.875F;
            this.GroupFooter4.Name = "GroupFooter4";
            // 
            // maklumatSekolahTableAdapter
            // 
            this.maklumatSekolahTableAdapter.ClearBeforeFill = true;
            // 
            // xrLabel145
            // 
            this.xrLabel145.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel145.LocationFloat = new DevExpress.Utils.PointFloat(6.95831F, 14.45833F);
            this.xrLabel145.Name = "xrLabel145";
            this.xrLabel145.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel145.SizeF = new System.Drawing.SizeF(273.5416F, 24.00002F);
            this.xrLabel145.StylePriority.UseFont = false;
            this.xrLabel145.StylePriority.UseTextAlignment = false;
            this.xrLabel145.Text = "KEPUTUSAN PEPERIKSAAN";
            this.xrLabel145.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel146
            // 
            this.xrLabel146.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel146.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 10.00001F);
            this.xrLabel146.Name = "xrLabel146";
            this.xrLabel146.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel146.SizeF = new System.Drawing.SizeF(149.2918F, 24.00002F);
            this.xrLabel146.StylePriority.UseFont = false;
            this.xrLabel146.StylePriority.UseTextAlignment = false;
            this.xrLabel146.Text = "SRP / PMR";
            this.xrLabel146.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel147
            // 
            this.xrLabel147.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel147.LocationFloat = new DevExpress.Utils.PointFloat(169.4791F, 10.00001F);
            this.xrLabel147.Name = "xrLabel147";
            this.xrLabel147.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel147.SizeF = new System.Drawing.SizeF(273.5416F, 24.00002F);
            this.xrLabel147.StylePriority.UseFont = false;
            this.xrLabel147.StylePriority.UseTextAlignment = false;
            this.xrLabel147.Text = "Bahasa Melayu";
            this.xrLabel147.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel148
            // 
            this.xrLabel148.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel148.LocationFloat = new DevExpress.Utils.PointFloat(454.854F, 10.99997F);
            this.xrLabel148.Name = "xrLabel148";
            this.xrLabel148.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel148.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel148.StylePriority.UseFont = false;
            this.xrLabel148.StylePriority.UseTextAlignment = false;
            this.xrLabel148.Text = ":";
            this.xrLabel148.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel149
            // 
            this.xrLabel149.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatPeperiksaan.cPMR_BM")});
            this.xrLabel149.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel149.LocationFloat = new DevExpress.Utils.PointFloat(485.125F, 10.99993F);
            this.xrLabel149.Name = "xrLabel149";
            this.xrLabel149.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel149.SizeF = new System.Drawing.SizeF(37.95804F, 23F);
            this.xrLabel149.StylePriority.UseFont = false;
            this.xrLabel149.StylePriority.UseTextAlignment = false;
            this.xrLabel149.Text = "A";
            this.xrLabel149.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel150
            // 
            this.xrLabel150.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel150.LocationFloat = new DevExpress.Utils.PointFloat(169.4791F, 34.00005F);
            this.xrLabel150.Name = "xrLabel150";
            this.xrLabel150.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel150.SizeF = new System.Drawing.SizeF(273.5416F, 24.00002F);
            this.xrLabel150.StylePriority.UseFont = false;
            this.xrLabel150.StylePriority.UseTextAlignment = false;
            this.xrLabel150.Text = "Bahasa Inggeris";
            this.xrLabel150.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel151
            // 
            this.xrLabel151.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel151.LocationFloat = new DevExpress.Utils.PointFloat(454.854F, 35.00026F);
            this.xrLabel151.Name = "xrLabel151";
            this.xrLabel151.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel151.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel151.StylePriority.UseFont = false;
            this.xrLabel151.StylePriority.UseTextAlignment = false;
            this.xrLabel151.Text = ":";
            this.xrLabel151.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel152
            // 
            this.xrLabel152.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatPeperiksaan.cPMR_BI")});
            this.xrLabel152.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel152.LocationFloat = new DevExpress.Utils.PointFloat(485.125F, 35.00023F);
            this.xrLabel152.Name = "xrLabel152";
            this.xrLabel152.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel152.SizeF = new System.Drawing.SizeF(37.95804F, 23F);
            this.xrLabel152.StylePriority.UseFont = false;
            this.xrLabel152.StylePriority.UseTextAlignment = false;
            this.xrLabel152.Text = "A";
            this.xrLabel152.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel153
            // 
            this.xrLabel153.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel153.LocationFloat = new DevExpress.Utils.PointFloat(169.4791F, 58.00009F);
            this.xrLabel153.Name = "xrLabel153";
            this.xrLabel153.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel153.SizeF = new System.Drawing.SizeF(273.5416F, 24.00002F);
            this.xrLabel153.StylePriority.UseFont = false;
            this.xrLabel153.StylePriority.UseTextAlignment = false;
            this.xrLabel153.Text = "Matematik";
            this.xrLabel153.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel154
            // 
            this.xrLabel154.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel154.LocationFloat = new DevExpress.Utils.PointFloat(454.854F, 59.0003F);
            this.xrLabel154.Name = "xrLabel154";
            this.xrLabel154.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel154.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel154.StylePriority.UseFont = false;
            this.xrLabel154.StylePriority.UseTextAlignment = false;
            this.xrLabel154.Text = ":";
            this.xrLabel154.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel155
            // 
            this.xrLabel155.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatPeperiksaan.cPMR_M3")});
            this.xrLabel155.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel155.LocationFloat = new DevExpress.Utils.PointFloat(485.125F, 59.00027F);
            this.xrLabel155.Name = "xrLabel155";
            this.xrLabel155.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel155.SizeF = new System.Drawing.SizeF(37.95804F, 23F);
            this.xrLabel155.StylePriority.UseFont = false;
            this.xrLabel155.StylePriority.UseTextAlignment = false;
            this.xrLabel155.Text = "A";
            this.xrLabel155.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // Keputusan_Peperiksaan
            // 
            this.Keputusan_Peperiksaan.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail5,
            this.GroupHeader5,
            this.GroupFooter5});
            this.Keputusan_Peperiksaan.DataAdapter = this.maklumatPeperiksaanTableAdapter;
            this.Keputusan_Peperiksaan.DataMember = "maklumatPeperiksaan";
            this.Keputusan_Peperiksaan.DataSource = this.repDataset1;
            this.Keputusan_Peperiksaan.Expanded = false;
            this.Keputusan_Peperiksaan.Level = 6;
            this.Keputusan_Peperiksaan.Name = "Keputusan_Peperiksaan";
            // 
            // Detail5
            // 
            this.Detail5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel124,
            this.xrLabel123,
            this.xrLabel125,
            this.xrLabel127,
            this.xrLabel126,
            this.xrLabel119,
            this.xrLabel112,
            this.xrLabel120,
            this.xrLabel122,
            this.xrLabel121,
            this.xrLabel146,
            this.xrLabel147,
            this.xrLabel148,
            this.xrLabel149,
            this.xrLabel150,
            this.xrLabel151,
            this.xrLabel152,
            this.xrLabel153,
            this.xrLabel154,
            this.xrLabel155});
            this.Detail5.HeightF = 196.5718F;
            this.Detail5.Name = "Detail5";
            // 
            // xrLabel124
            // 
            this.xrLabel124.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatPeperiksaan.cSPM_BM")});
            this.xrLabel124.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel124.LocationFloat = new DevExpress.Utils.PointFloat(482.9377F, 120.2082F);
            this.xrLabel124.Name = "xrLabel124";
            this.xrLabel124.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel124.SizeF = new System.Drawing.SizeF(37.95801F, 22.99999F);
            this.xrLabel124.StylePriority.UseFont = false;
            this.xrLabel124.StylePriority.UseTextAlignment = false;
            this.xrLabel124.Text = "A";
            this.xrLabel124.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel123
            // 
            this.xrLabel123.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel123.LocationFloat = new DevExpress.Utils.PointFloat(167.2918F, 143.2083F);
            this.xrLabel123.Name = "xrLabel123";
            this.xrLabel123.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel123.SizeF = new System.Drawing.SizeF(273.5416F, 24.00002F);
            this.xrLabel123.StylePriority.UseFont = false;
            this.xrLabel123.StylePriority.UseTextAlignment = false;
            this.xrLabel123.Text = "Bahasa Inggeris";
            this.xrLabel123.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel125
            // 
            this.xrLabel125.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel125.LocationFloat = new DevExpress.Utils.PointFloat(452.6667F, 120.2082F);
            this.xrLabel125.Name = "xrLabel125";
            this.xrLabel125.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel125.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel125.StylePriority.UseFont = false;
            this.xrLabel125.StylePriority.UseTextAlignment = false;
            this.xrLabel125.Text = ":";
            this.xrLabel125.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel127
            // 
            this.xrLabel127.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel127.LocationFloat = new DevExpress.Utils.PointFloat(10.31265F, 119.2082F);
            this.xrLabel127.Name = "xrLabel127";
            this.xrLabel127.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel127.SizeF = new System.Drawing.SizeF(149.2918F, 24.00002F);
            this.xrLabel127.StylePriority.UseFont = false;
            this.xrLabel127.StylePriority.UseTextAlignment = false;
            this.xrLabel127.Text = "SPM";
            this.xrLabel127.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel126
            // 
            this.xrLabel126.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel126.LocationFloat = new DevExpress.Utils.PointFloat(167.2918F, 119.2082F);
            this.xrLabel126.Name = "xrLabel126";
            this.xrLabel126.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel126.SizeF = new System.Drawing.SizeF(273.5416F, 24.00002F);
            this.xrLabel126.StylePriority.UseFont = false;
            this.xrLabel126.StylePriority.UseTextAlignment = false;
            this.xrLabel126.Text = "Bahasa Melayu";
            this.xrLabel126.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel119
            // 
            this.xrLabel119.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel119.LocationFloat = new DevExpress.Utils.PointFloat(452.6667F, 168.2085F);
            this.xrLabel119.Name = "xrLabel119";
            this.xrLabel119.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel119.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel119.StylePriority.UseFont = false;
            this.xrLabel119.StylePriority.UseTextAlignment = false;
            this.xrLabel119.Text = ":";
            this.xrLabel119.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel112
            // 
            this.xrLabel112.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatPeperiksaan.cSPM_M3")});
            this.xrLabel112.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel112.LocationFloat = new DevExpress.Utils.PointFloat(482.9377F, 168.2086F);
            this.xrLabel112.Name = "xrLabel112";
            this.xrLabel112.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel112.SizeF = new System.Drawing.SizeF(37.95801F, 22.99998F);
            this.xrLabel112.StylePriority.UseFont = false;
            this.xrLabel112.StylePriority.UseTextAlignment = false;
            this.xrLabel112.Text = "A";
            this.xrLabel112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel120
            // 
            this.xrLabel120.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel120.LocationFloat = new DevExpress.Utils.PointFloat(167.2918F, 167.2083F);
            this.xrLabel120.Name = "xrLabel120";
            this.xrLabel120.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel120.SizeF = new System.Drawing.SizeF(273.5416F, 24.00002F);
            this.xrLabel120.StylePriority.UseFont = false;
            this.xrLabel120.StylePriority.UseTextAlignment = false;
            this.xrLabel120.Text = "Matematik";
            this.xrLabel120.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel122
            // 
            this.xrLabel122.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel122.LocationFloat = new DevExpress.Utils.PointFloat(452.6667F, 144.2085F);
            this.xrLabel122.Name = "xrLabel122";
            this.xrLabel122.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel122.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel122.StylePriority.UseFont = false;
            this.xrLabel122.StylePriority.UseTextAlignment = false;
            this.xrLabel122.Text = ":";
            this.xrLabel122.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel121
            // 
            this.xrLabel121.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatPeperiksaan.cSPM_BI")});
            this.xrLabel121.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel121.LocationFloat = new DevExpress.Utils.PointFloat(482.9377F, 144.2085F);
            this.xrLabel121.Name = "xrLabel121";
            this.xrLabel121.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel121.SizeF = new System.Drawing.SizeF(37.95801F, 23F);
            this.xrLabel121.StylePriority.UseFont = false;
            this.xrLabel121.StylePriority.UseTextAlignment = false;
            this.xrLabel121.Text = "A";
            this.xrLabel121.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // GroupHeader5
            // 
            this.GroupHeader5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel145});
            this.GroupHeader5.HeightF = 43.66668F;
            this.GroupHeader5.Name = "GroupHeader5";
            // 
            // GroupFooter5
            // 
            this.GroupFooter5.HeightF = 63.54167F;
            this.GroupFooter5.Name = "GroupFooter5";
            // 
            // maklumatPeperiksaanTableAdapter
            // 
            this.maklumatPeperiksaanTableAdapter.ClearBeforeFill = true;
            // 
            // Maklumat_Institusi_Pengajian
            // 
            this.Maklumat_Institusi_Pengajian.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail6,
            this.GroupHeader6,
            this.GroupFooter6});
            this.Maklumat_Institusi_Pengajian.DataAdapter = this.maklumatIPTTableAdapter;
            this.Maklumat_Institusi_Pengajian.DataMember = "maklumatIPT";
            this.Maklumat_Institusi_Pengajian.DataSource = this.repDataset1;
            this.Maklumat_Institusi_Pengajian.Expanded = false;
            this.Maklumat_Institusi_Pengajian.Level = 7;
            this.Maklumat_Institusi_Pengajian.Name = "Maklumat_Institusi_Pengajian";
            // 
            // Detail6
            // 
            this.Detail6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.Detail6.HeightF = 71.875F;
            this.Detail6.Name = "Detail6";
            // 
            // xrTable6
            // 
            this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable6.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(2.37484F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable6.SizeF = new System.Drawing.SizeF(639.6249F, 71.875F);
            this.xrTable6.StylePriority.UseBorders = false;
            this.xrTable6.StylePriority.UseFont = false;
            this.xrTable6.StylePriority.UseTextAlignment = false;
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell27,
            this.xrTableCell29,
            this.xrTableCell31,
            this.xrTableCell32,
            this.xrTableCell33});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatIPT.NamaIPT")});
            this.xrTableCell27.Multiline = true;
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Text = "UNIVERSITI\r\nMALAYSIA SABAH\r\n(UMS)";
            this.xrTableCell27.Weight = 0.5707447870948622D;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatIPT.Kelulusan")});
            this.xrTableCell29.Multiline = true;
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Text = "SARJANA\r\nMUDA\r\n(KEPUJIAN)";
            this.xrTableCell29.Weight = 0.73373137268177768D;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatIPT.Pengkhususan")});
            this.xrTableCell31.Multiline = true;
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Text = "KEJURUTERAAN\r\nKOMPUTER /\r\nCOMPUTER\r\nENGINEERING";
            this.xrTableCell31.Weight = 0.87043025319309619D;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatIPT.Convodate", "{0:dd/MM/yyyy}")});
            this.xrTableCell32.Multiline = true;
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Text = "10/10/2008";
            this.xrTableCell32.Weight = 0.36515608690131984D;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatIPT.CGPA")});
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Text = "2.98";
            this.xrTableCell33.Weight = 0.45993750012894441D;
            // 
            // GroupHeader6
            // 
            this.GroupHeader6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel156,
            this.xrLine9,
            this.xrTable5});
            this.GroupHeader6.HeightF = 99.73322F;
            this.GroupHeader6.Name = "GroupHeader6";
            // 
            // xrLabel156
            // 
            this.xrLabel156.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel156.LocationFloat = new DevExpress.Utils.PointFloat(3.041553F, 24.22511F);
            this.xrLabel156.Name = "xrLabel156";
            this.xrLabel156.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel156.SizeF = new System.Drawing.SizeF(273.5416F, 24.00002F);
            this.xrLabel156.StylePriority.UseFont = false;
            this.xrLabel156.StylePriority.UseTextAlignment = false;
            this.xrLabel156.Text = "7. MAKLUMAT INSTITUSI PENGAJIAN";
            this.xrLabel156.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine9
            // 
            this.xrLine9.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrLine9.LineWidth = 2;
            this.xrLine9.LocationFloat = new DevExpress.Utils.PointFloat(3.041553F, 48.22515F);
            this.xrLine9.Name = "xrLine9";
            this.xrLine9.SizeF = new System.Drawing.SizeF(640.6249F, 2F);
            this.xrLine9.StylePriority.UseBorderDashStyle = false;
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(3.041553F, 63.27489F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable5.SizeF = new System.Drawing.SizeF(639.6249F, 36.45833F);
            this.xrTable5.StylePriority.UseBorders = false;
            this.xrTable5.StylePriority.UseFont = false;
            this.xrTable5.StylePriority.UseTextAlignment = false;
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell14,
            this.xrTableCell16,
            this.xrTableCell19,
            this.xrTableCell18,
            this.xrTableCell20});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Text = "Nama IPTA / IPTS";
            this.xrTableCell14.Weight = 0.5707447870948622D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Text = "Kelulusan";
            this.xrTableCell16.Weight = 0.730604158263384D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Text = "Pengkhususan";
            this.xrTableCell19.Weight = 0.87355746761148989D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Multiline = true;
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Text = "Tarikh\r\nKonvo";
            this.xrTableCell18.Weight = 0.36202915875301206D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Text = "CGPA /PNGK";
            this.xrTableCell20.Weight = 0.46306442827725219D;
            // 
            // GroupFooter6
            // 
            this.GroupFooter6.HeightF = 57.29167F;
            this.GroupFooter6.Name = "GroupFooter6";
            // 
            // maklumatIPTTableAdapter
            // 
            this.maklumatIPTTableAdapter.ClearBeforeFill = true;
            // 
            // Maklumat_Badan_Profesional
            // 
            this.Maklumat_Badan_Profesional.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail7,
            this.GroupHeader7,
            this.GroupFooter7});
            this.Maklumat_Badan_Profesional.DataAdapter = this.maklumatBadanProTableAdapter;
            this.Maklumat_Badan_Profesional.DataMember = "maklumatBadanPro";
            this.Maklumat_Badan_Profesional.DataSource = this.repDataset1;
            this.Maklumat_Badan_Profesional.Expanded = false;
            this.Maklumat_Badan_Profesional.Level = 8;
            this.Maklumat_Badan_Profesional.Name = "Maklumat_Badan_Profesional";
            // 
            // Detail7
            // 
            this.Detail7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable8});
            this.Detail7.HeightF = 36.45833F;
            this.Detail7.Name = "Detail7";
            // 
            // xrTable8
            // 
            this.xrTable8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable8.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(4.166667F, 0F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable8.SizeF = new System.Drawing.SizeF(639.6249F, 36.45833F);
            this.xrTable8.StylePriority.UseBorders = false;
            this.xrTable8.StylePriority.UseFont = false;
            this.xrTable8.StylePriority.UseTextAlignment = false;
            this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell36,
            this.xrTableCell37,
            this.xrTableCell40,
            this.xrTableCell41});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1D;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatBadanPro.Institusi")});
            this.xrTableCell36.Multiline = true;
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Text = "MALAYSIAN ASSOCIATION OF\r\nCERTIFIED PUBLIC ACCOUNTANTS";
            this.xrTableCell36.Weight = 1.111294885123282D;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatBadanPro.NoSijil")});
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Text = "A90876";
            this.xrTableCell37.Weight = 0.625562765750545D;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatBadanPro.Kursus")});
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Text = "CCNA";
            this.xrTableCell40.Weight = 0.69698950170711993D;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatBadanPro.TarikhAhli", "{0:dd/MM/yyyy}")});
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Text = "10/9/2012";
            this.xrTableCell41.Weight = 0.56615284741905336D;
            // 
            // GroupHeader7
            // 
            this.GroupHeader7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel157,
            this.xrLine10,
            this.xrTable7});
            this.GroupHeader7.HeightF = 116.9207F;
            this.GroupHeader7.Name = "GroupHeader7";
            // 
            // xrLabel157
            // 
            this.xrLabel157.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel157.LocationFloat = new DevExpress.Utils.PointFloat(4.687563F, 41.41261F);
            this.xrLabel157.Name = "xrLabel157";
            this.xrLabel157.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel157.SizeF = new System.Drawing.SizeF(642.8956F, 24.00002F);
            this.xrLabel157.StylePriority.UseFont = false;
            this.xrLabel157.StylePriority.UseTextAlignment = false;
            this.xrLabel157.Text = "8. MAKLUMAT BADAN PROFESIONAL/ IKHTISAS (AKAUNTAN BERTAULIAH / PEGUAM SAHAJA)";
            this.xrLabel157.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine10
            // 
            this.xrLine10.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrLine10.LineWidth = 2;
            this.xrLine10.LocationFloat = new DevExpress.Utils.PointFloat(4.687563F, 65.41265F);
            this.xrLine10.Name = "xrLine10";
            this.xrLine10.SizeF = new System.Drawing.SizeF(640.6249F, 2F);
            this.xrLine10.StylePriority.UseBorderDashStyle = false;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable7.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(4.687563F, 80.46239F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(639.6249F, 36.45833F);
            this.xrTable7.StylePriority.UseBorders = false;
            this.xrTable7.StylePriority.UseFont = false;
            this.xrTable7.StylePriority.UseTextAlignment = false;
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell35,
            this.xrTableCell38,
            this.xrTableCell39,
            this.xrTableCell42});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Text = "Nama Lembaga / Badan Profesional / Ikhtisas / Institusi";
            this.xrTableCell35.Weight = 1.111294885123282D;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Text = "No. Ahli / Sijil";
            this.xrTableCell38.Weight = 0.625562765750545D;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Text = "Kursus / Peperiksaan / Sijil Yang Diperolehi";
            this.xrTableCell39.Weight = 0.69698950170711993D;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Text = "Tarikh Ahli / Sijil";
            this.xrTableCell42.Weight = 0.56615284741905336D;
            // 
            // GroupFooter7
            // 
            this.GroupFooter7.HeightF = 66.66666F;
            this.GroupFooter7.Name = "GroupFooter7";
            // 
            // maklumatBadanProTableAdapter
            // 
            this.maklumatBadanProTableAdapter.ClearBeforeFill = true;
            // 
            // Maklumat_KPSL
            // 
            this.Maklumat_KPSL.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail8,
            this.GroupHeader8,
            this.GroupFooter8});
            this.Maklumat_KPSL.DataSource = this.repDataset1;
            this.Maklumat_KPSL.Expanded = false;
            this.Maklumat_KPSL.Level = 9;
            this.Maklumat_KPSL.Name = "Maklumat_KPSL";
            // 
            // Detail8
            // 
            this.Detail8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable10});
            this.Detail8.HeightF = 36.45833F;
            this.Detail8.Name = "Detail8";
            // 
            // xrTable10
            // 
            this.xrTable10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable10.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(639.6249F, 36.45833F);
            this.xrTable10.StylePriority.UseBorders = false;
            this.xrTable10.StylePriority.UseFont = false;
            this.xrTable10.StylePriority.UseTextAlignment = false;
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell48,
            this.xrTableCell49,
            this.xrTableCell50,
            this.xrTableCell51,
            this.xrTableCell52});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1D;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Weight = 0.47293327657548617D;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Weight = 1.263924374298341D;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Weight = 0.34849475085355996D;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Weight = 0.42070380359157111D;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Weight = 0.49394379468104221D;
            // 
            // GroupHeader8
            // 
            this.GroupHeader8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable9,
            this.xrLabel158,
            this.xrLine11});
            this.GroupHeader8.HeightF = 111.4583F;
            this.GroupHeader8.Name = "GroupHeader8";
            // 
            // xrTable9
            // 
            this.xrTable9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable9.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(0F, 75F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.xrTable9.SizeF = new System.Drawing.SizeF(639.6249F, 36.45833F);
            this.xrTable9.StylePriority.UseBorders = false;
            this.xrTable9.StylePriority.UseFont = false;
            this.xrTable9.StylePriority.UseTextAlignment = false;
            this.xrTable9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell43,
            this.xrTableCell44,
            this.xrTableCell45,
            this.xrTableCell47,
            this.xrTableCell46});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1D;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Text = "KPSL";
            this.xrTableCell43.Weight = 0.47293327657548617D;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Text = "Jenis Peperiksaan";
            this.xrTableCell44.Weight = 1.263924374298341D;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Text = "Tarikh";
            this.xrTableCell45.Weight = 0.34849475085355996D;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Text = "Markah";
            this.xrTableCell47.Weight = 0.42070380359157111D;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Text = "Keputusan";
            this.xrTableCell46.Weight = 0.49394379468104221D;
            // 
            // xrLabel158
            // 
            this.xrLabel158.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel158.LocationFloat = new DevExpress.Utils.PointFloat(3.552216F, 36.99998F);
            this.xrLabel158.Name = "xrLabel158";
            this.xrLabel158.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel158.SizeF = new System.Drawing.SizeF(642.8956F, 24.00002F);
            this.xrLabel158.StylePriority.UseFont = false;
            this.xrLabel158.StylePriority.UseTextAlignment = false;
            this.xrLabel158.Text = "9. MAKLUMAT KPSL (UNTUK KAKITANGAN LHDNM SAHAJA)";
            this.xrLabel158.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine11
            // 
            this.xrLine11.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrLine11.LineWidth = 2;
            this.xrLine11.LocationFloat = new DevExpress.Utils.PointFloat(3.552216F, 61.00002F);
            this.xrLine11.Name = "xrLine11";
            this.xrLine11.SizeF = new System.Drawing.SizeF(640.6249F, 2F);
            this.xrLine11.StylePriority.UseBorderDashStyle = false;
            // 
            // GroupFooter8
            // 
            this.GroupFooter8.Name = "GroupFooter8";
            // 
            // Maklumat_Sijil_Kemahiran
            // 
            this.Maklumat_Sijil_Kemahiran.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail9,
            this.GroupHeader9,
            this.GroupFooter9});
            this.Maklumat_Sijil_Kemahiran.DataAdapter = this.maklumatsijilkemahiranTableAdapter;
            this.Maklumat_Sijil_Kemahiran.DataMember = "maklumatsijilkemahiran";
            this.Maklumat_Sijil_Kemahiran.DataSource = this.repDataset1;
            this.Maklumat_Sijil_Kemahiran.Expanded = false;
            this.Maklumat_Sijil_Kemahiran.Level = 10;
            this.Maklumat_Sijil_Kemahiran.Name = "Maklumat_Sijil_Kemahiran";
            // 
            // Detail9
            // 
            this.Detail9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable12});
            this.Detail9.HeightF = 36.45833F;
            this.Detail9.Name = "Detail9";
            // 
            // xrTable12
            // 
            this.xrTable12.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable12.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.xrTable12.LocationFloat = new DevExpress.Utils.PointFloat(2.166667F, 0F);
            this.xrTable12.Name = "xrTable12";
            this.xrTable12.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12});
            this.xrTable12.SizeF = new System.Drawing.SizeF(639.6249F, 36.45833F);
            this.xrTable12.StylePriority.UseBorders = false;
            this.xrTable12.StylePriority.UseFont = false;
            this.xrTable12.StylePriority.UseTextAlignment = false;
            this.xrTable12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell53,
            this.xrTableCell55});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1D;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatsijilkemahiran.cCertDesc")});
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Text = "SIJIL JURUTEKNIK KOMPUTER";
            this.xrTableCell53.Weight = 2.1885617880681933D;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatsijilkemahiran.dCertDate", "{0:dd/MM/yyyy}")});
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Text = "23/1/2012";
            this.xrTableCell55.Weight = 0.8114382119318071D;
            // 
            // GroupHeader9
            // 
            this.GroupHeader9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable11,
            this.xrLabel159,
            this.xrLine12});
            this.GroupHeader9.HeightF = 102.2292F;
            this.GroupHeader9.Name = "GroupHeader9";
            // 
            // xrTable11
            // 
            this.xrTable11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable11.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.xrTable11.LocationFloat = new DevExpress.Utils.PointFloat(1.776123F, 65.77084F);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow11});
            this.xrTable11.SizeF = new System.Drawing.SizeF(639.6249F, 36.45833F);
            this.xrTable11.StylePriority.UseBorders = false;
            this.xrTable11.StylePriority.UseFont = false;
            this.xrTable11.StylePriority.UseTextAlignment = false;
            this.xrTable11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell54,
            this.xrTableCell57});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1D;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Text = "Kemahiran / Sijil Diperoleh";
            this.xrTableCell54.Weight = 2.1885617880681933D;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Text = "Tarikh Ahli / Sijil";
            this.xrTableCell57.Weight = 0.8114382119318071D;
            // 
            // xrLabel159
            // 
            this.xrLabel159.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel159.LocationFloat = new DevExpress.Utils.PointFloat(5.328339F, 27.77082F);
            this.xrLabel159.Name = "xrLabel159";
            this.xrLabel159.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel159.SizeF = new System.Drawing.SizeF(642.8956F, 24.00002F);
            this.xrLabel159.StylePriority.UseFont = false;
            this.xrLabel159.StylePriority.UseTextAlignment = false;
            this.xrLabel159.Text = "10. MAKLUMAT SIJIL KEMAHIRAN";
            this.xrLabel159.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine12
            // 
            this.xrLine12.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrLine12.LineWidth = 2;
            this.xrLine12.LocationFloat = new DevExpress.Utils.PointFloat(5.328339F, 51.77086F);
            this.xrLine12.Name = "xrLine12";
            this.xrLine12.SizeF = new System.Drawing.SizeF(640.6249F, 2F);
            this.xrLine12.StylePriority.UseBorderDashStyle = false;
            // 
            // GroupFooter9
            // 
            this.GroupFooter9.Name = "GroupFooter9";
            // 
            // maklumatsijilkemahiranTableAdapter
            // 
            this.maklumatsijilkemahiranTableAdapter.ClearBeforeFill = true;
            // 
            // Maklumat_Kemahiran_Komputer
            // 
            this.Maklumat_Kemahiran_Komputer.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail10,
            this.GroupHeader10,
            this.GroupFooter10});
            this.Maklumat_Kemahiran_Komputer.DataAdapter = this.maklumatKemahiranKomputerTableAdapter;
            this.Maklumat_Kemahiran_Komputer.DataMember = "maklumatKemahiranKomputer";
            this.Maklumat_Kemahiran_Komputer.DataSource = this.repDataset1;
            this.Maklumat_Kemahiran_Komputer.Expanded = false;
            this.Maklumat_Kemahiran_Komputer.Level = 11;
            this.Maklumat_Kemahiran_Komputer.Name = "Maklumat_Kemahiran_Komputer";
            // 
            // Detail10
            // 
            this.Detail10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable14});
            this.Detail10.HeightF = 36.45833F;
            this.Detail10.Name = "Detail10";
            // 
            // xrTable14
            // 
            this.xrTable14.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable14.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.xrTable14.LocationFloat = new DevExpress.Utils.PointFloat(3.041569F, 0F);
            this.xrTable14.Name = "xrTable14";
            this.xrTable14.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow14});
            this.xrTable14.SizeF = new System.Drawing.SizeF(639.6249F, 36.45833F);
            this.xrTable14.StylePriority.UseBorders = false;
            this.xrTable14.StylePriority.UseFont = false;
            this.xrTable14.StylePriority.UseTextAlignment = false;
            this.xrTable14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell60,
            this.xrTableCell62});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1D;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatKemahiranKomputer.Nama Perisian")});
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Text = "MSSQL CERTIFICATE 2013";
            this.xrTableCell60.Weight = 2.0894081716227562D;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatKemahiranKomputer.TahapKemahiran")});
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Text = "SANGAT MAHIR";
            this.xrTableCell62.Weight = 0.91059182837724428D;
            // 
            // GroupHeader10
            // 
            this.GroupHeader10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable13,
            this.xrLabel160,
            this.xrLine13});
            this.GroupHeader10.HeightF = 99.22917F;
            this.GroupHeader10.Name = "GroupHeader10";
            // 
            // xrTable13
            // 
            this.xrTable13.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable13.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.xrTable13.LocationFloat = new DevExpress.Utils.PointFloat(1.776123F, 62.77084F);
            this.xrTable13.Name = "xrTable13";
            this.xrTable13.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13});
            this.xrTable13.SizeF = new System.Drawing.SizeF(639.6249F, 36.45833F);
            this.xrTable13.StylePriority.UseBorders = false;
            this.xrTable13.StylePriority.UseFont = false;
            this.xrTable13.StylePriority.UseTextAlignment = false;
            this.xrTable13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell56,
            this.xrTableCell58});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1D;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Text = "Nama Perisian";
            this.xrTableCell56.Weight = 2.0953434093137626D;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Text = "Tahap Kemahiran";
            this.xrTableCell58.Weight = 0.90465659068623783D;
            // 
            // xrLabel160
            // 
            this.xrLabel160.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel160.LocationFloat = new DevExpress.Utils.PointFloat(5.328339F, 24.77082F);
            this.xrLabel160.Name = "xrLabel160";
            this.xrLabel160.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel160.SizeF = new System.Drawing.SizeF(642.8956F, 24.00002F);
            this.xrLabel160.StylePriority.UseFont = false;
            this.xrLabel160.StylePriority.UseTextAlignment = false;
            this.xrLabel160.Text = "11. MAKLUMAT KEMAHIRAN KOMPUTER";
            this.xrLabel160.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine13
            // 
            this.xrLine13.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrLine13.LineWidth = 2;
            this.xrLine13.LocationFloat = new DevExpress.Utils.PointFloat(5.328339F, 48.77086F);
            this.xrLine13.Name = "xrLine13";
            this.xrLine13.SizeF = new System.Drawing.SizeF(640.6249F, 2F);
            this.xrLine13.StylePriority.UseBorderDashStyle = false;
            // 
            // GroupFooter10
            // 
            this.GroupFooter10.Name = "GroupFooter10";
            // 
            // maklumatKemahiranKomputerTableAdapter
            // 
            this.maklumatKemahiranKomputerTableAdapter.ClearBeforeFill = true;
            // 
            // Maklumat_Kegiatan_Luar
            // 
            this.Maklumat_Kegiatan_Luar.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail11,
            this.GroupHeader11,
            this.GroupFooter11});
            this.Maklumat_Kegiatan_Luar.DataAdapter = this.maklumatKegiatanLuarTableAdapter;
            this.Maklumat_Kegiatan_Luar.DataMember = "maklumatKegiatanLuar";
            this.Maklumat_Kegiatan_Luar.DataSource = this.repDataset1;
            this.Maklumat_Kegiatan_Luar.Expanded = false;
            this.Maklumat_Kegiatan_Luar.Level = 12;
            this.Maklumat_Kegiatan_Luar.Name = "Maklumat_Kegiatan_Luar";
            // 
            // Detail11
            // 
            this.Detail11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable16});
            this.Detail11.HeightF = 36.45833F;
            this.Detail11.Name = "Detail11";
            // 
            // xrTable16
            // 
            this.xrTable16.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable16.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.xrTable16.LocationFloat = new DevExpress.Utils.PointFloat(3.041569F, 0F);
            this.xrTable16.Name = "xrTable16";
            this.xrTable16.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow16});
            this.xrTable16.SizeF = new System.Drawing.SizeF(639.6249F, 36.45833F);
            this.xrTable16.StylePriority.UseBorders = false;
            this.xrTable16.StylePriority.UseFont = false;
            this.xrTable16.StylePriority.UseTextAlignment = false;
            this.xrTable16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell67,
            this.xrTableCell68,
            this.xrTableCell69,
            this.xrTableCell70});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1D;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatKegiatanLuar.Persatuan")});
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Text = "OLAHRAGA";
            this.xrTableCell67.Weight = 1.0942808940340967D;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatKegiatanLuar.Jawatan")});
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Text = "PESERTA";
            this.xrTableCell68.Weight = 0.54714044701704834D;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatKegiatanLuar.Tahun")});
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Text = "2009";
            this.xrTableCell69.Weight = 0.54714044701704834D;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatKegiatanLuar.Peringkat")});
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Text = "DAERAH";
            this.xrTableCell70.Weight = 0.8114382119318071D;
            // 
            // GroupHeader11
            // 
            this.GroupHeader11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine14,
            this.xrLabel161,
            this.xrTable15});
            this.GroupHeader11.HeightF = 98.1041F;
            this.GroupHeader11.Name = "GroupHeader11";
            // 
            // xrLine14
            // 
            this.xrLine14.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrLine14.LineWidth = 2;
            this.xrLine14.LocationFloat = new DevExpress.Utils.PointFloat(6.5F, 47.64582F);
            this.xrLine14.Name = "xrLine14";
            this.xrLine14.SizeF = new System.Drawing.SizeF(640.6249F, 2F);
            this.xrLine14.StylePriority.UseBorderDashStyle = false;
            // 
            // xrLabel161
            // 
            this.xrLabel161.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel161.LocationFloat = new DevExpress.Utils.PointFloat(6.5F, 23.64578F);
            this.xrLabel161.Name = "xrLabel161";
            this.xrLabel161.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel161.SizeF = new System.Drawing.SizeF(642.8956F, 24.00002F);
            this.xrLabel161.StylePriority.UseFont = false;
            this.xrLabel161.StylePriority.UseTextAlignment = false;
            this.xrLabel161.Text = "12. MAKLUMAT KEGIATAN LUAR";
            this.xrLabel161.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable15
            // 
            this.xrTable15.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable15.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.xrTable15.LocationFloat = new DevExpress.Utils.PointFloat(2.947785F, 61.64576F);
            this.xrTable15.Name = "xrTable15";
            this.xrTable15.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow15});
            this.xrTable15.SizeF = new System.Drawing.SizeF(639.6249F, 36.45833F);
            this.xrTable15.StylePriority.UseBorders = false;
            this.xrTable15.StylePriority.UseFont = false;
            this.xrTable15.StylePriority.UseTextAlignment = false;
            this.xrTable15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell63,
            this.xrTableCell64,
            this.xrTableCell66,
            this.xrTableCell65});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 1D;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Text = "Sukan / Persatuan / Kelab";
            this.xrTableCell63.Weight = 1.0942808940340967D;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Text = "Jawatan";
            this.xrTableCell64.Weight = 0.54714044701704834D;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Text = "Tahun";
            this.xrTableCell66.Weight = 0.54714044701704834D;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Text = "Peringkat";
            this.xrTableCell65.Weight = 0.8114382119318071D;
            // 
            // GroupFooter11
            // 
            this.GroupFooter11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable17});
            this.GroupFooter11.Name = "GroupFooter11";
            // 
            // xrTable17
            // 
            this.xrTable17.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable17.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.xrTable17.LocationFloat = new DevExpress.Utils.PointFloat(0F, 37.5F);
            this.xrTable17.Name = "xrTable17";
            this.xrTable17.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow17});
            this.xrTable17.SizeF = new System.Drawing.SizeF(639.6249F, 36.45833F);
            this.xrTable17.StylePriority.UseBorders = false;
            this.xrTable17.StylePriority.UseFont = false;
            this.xrTable17.StylePriority.UseTextAlignment = false;
            this.xrTable17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell71,
            this.xrTableCell74});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 1D;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.StylePriority.UseFont = false;
            this.xrTableCell71.Text = "Hobi :";
            this.xrTableCell71.Weight = 0.32147754936892087D;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatHobi.cHobby")});
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Text = "MENONTON TV, BOLASEPAK";
            this.xrTableCell74.Weight = 2.6785224506310796D;
            // 
            // maklumatKegiatanLuarTableAdapter
            // 
            this.maklumatKegiatanLuarTableAdapter.ClearBeforeFill = true;
            // 
            // Maklumat_Pekerjaan
            // 
            this.Maklumat_Pekerjaan.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail12,
            this.GroupHeader12,
            this.GroupFooter12});
            this.Maklumat_Pekerjaan.DataAdapter = this.maklumatPekerjaanTableAdapter;
            this.Maklumat_Pekerjaan.DataMember = "maklumatPekerjaan";
            this.Maklumat_Pekerjaan.DataSource = this.repDataset1;
            this.Maklumat_Pekerjaan.Expanded = false;
            this.Maklumat_Pekerjaan.Level = 13;
            this.Maklumat_Pekerjaan.Name = "Maklumat_Pekerjaan";
            // 
            // Detail12
            // 
            this.Detail12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel176,
            this.xrLabel143,
            this.xrLabel208,
            this.xrLabel209,
            this.xrLabel210,
            this.xrLabel205,
            this.xrLabel206,
            this.xrLabel207,
            this.xrLabel204,
            this.xrLabel203,
            this.xrLabel202,
            this.xrLabel199,
            this.xrLabel198,
            this.xrLabel201,
            this.xrLabel200,
            this.xrLabel197,
            this.xrLabel194,
            this.xrLabel193,
            this.xrLabel196,
            this.xrLabel195,
            this.xrLabel190,
            this.xrLabel189,
            this.xrLabel192,
            this.xrLabel191,
            this.xrLabel188,
            this.xrLabel185,
            this.xrLabel184,
            this.xrLabel187,
            this.xrLabel186,
            this.xrLabel182,
            this.xrLabel175,
            this.xrLabel177,
            this.xrLabel174,
            this.xrLabel173,
            this.xrLabel172,
            this.xrLabel168,
            this.xrLabel167,
            this.xrLabel166,
            this.xrLabel165,
            this.xrLabel164,
            this.xrLabel163});
            this.Detail12.HeightF = 420.886F;
            this.Detail12.Name = "Detail12";
            // 
            // xrLabel176
            // 
            this.xrLabel176.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatPekerjaan.Alamat3")});
            this.xrLabel176.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel176.LocationFloat = new DevExpress.Utils.PointFloat(299.1252F, 137.4168F);
            this.xrLabel176.Multiline = true;
            this.xrLabel176.Name = "xrLabel176";
            this.xrLabel176.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel176.SizeF = new System.Drawing.SizeF(329.3749F, 23.00003F);
            this.xrLabel176.StylePriority.UseFont = false;
            this.xrLabel176.StylePriority.UseTextAlignment = false;
            this.xrLabel176.Text = "51000";
            this.xrLabel176.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel143
            // 
            this.xrLabel143.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatPekerjaan.Alamat2")});
            this.xrLabel143.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel143.LocationFloat = new DevExpress.Utils.PointFloat(299.1252F, 114.4168F);
            this.xrLabel143.Multiline = true;
            this.xrLabel143.Name = "xrLabel143";
            this.xrLabel143.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel143.SizeF = new System.Drawing.SizeF(329.3749F, 23.00003F);
            this.xrLabel143.StylePriority.UseFont = false;
            this.xrLabel143.StylePriority.UseTextAlignment = false;
            this.xrLabel143.Text = "51000";
            this.xrLabel143.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel208
            // 
            this.xrLabel208.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatPekerjaan.Gaji")});
            this.xrLabel208.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel208.LocationFloat = new DevExpress.Utils.PointFloat(300.9579F, 364.1671F);
            this.xrLabel208.Multiline = true;
            this.xrLabel208.Name = "xrLabel208";
            this.xrLabel208.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel208.SizeF = new System.Drawing.SizeF(329.3749F, 23.00006F);
            this.xrLabel208.StylePriority.UseFont = false;
            this.xrLabel208.StylePriority.UseTextAlignment = false;
            this.xrLabel208.Text = "5000";
            this.xrLabel208.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel209
            // 
            this.xrLabel209.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatPekerjaan.SebabBerhenti")});
            this.xrLabel209.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel209.LocationFloat = new DevExpress.Utils.PointFloat(300.958F, 387.1672F);
            this.xrLabel209.Multiline = true;
            this.xrLabel209.Name = "xrLabel209";
            this.xrLabel209.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel209.SizeF = new System.Drawing.SizeF(329.3749F, 23F);
            this.xrLabel209.StylePriority.UseFont = false;
            this.xrLabel209.StylePriority.UseTextAlignment = false;
            this.xrLabel209.Text = "GAJI KURANG";
            this.xrLabel209.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel210
            // 
            this.xrLabel210.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel210.LocationFloat = new DevExpress.Utils.PointFloat(273.1453F, 365.1053F);
            this.xrLabel210.Name = "xrLabel210";
            this.xrLabel210.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel210.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel210.StylePriority.UseFont = false;
            this.xrLabel210.StylePriority.UseTextAlignment = false;
            this.xrLabel210.Text = ":";
            this.xrLabel210.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel205
            // 
            this.xrLabel205.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel205.LocationFloat = new DevExpress.Utils.PointFloat(15.33283F, 364.1672F);
            this.xrLabel205.Multiline = true;
            this.xrLabel205.Name = "xrLabel205";
            this.xrLabel205.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel205.SizeF = new System.Drawing.SizeF(250.7915F, 23F);
            this.xrLabel205.StylePriority.UseFont = false;
            this.xrLabel205.StylePriority.UseTextAlignment = false;
            this.xrLabel205.Text = "Gaji";
            this.xrLabel205.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel206
            // 
            this.xrLabel206.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel206.LocationFloat = new DevExpress.Utils.PointFloat(273.1453F, 388.1053F);
            this.xrLabel206.Name = "xrLabel206";
            this.xrLabel206.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel206.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel206.StylePriority.UseFont = false;
            this.xrLabel206.StylePriority.UseTextAlignment = false;
            this.xrLabel206.Text = ":";
            this.xrLabel206.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel207
            // 
            this.xrLabel207.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel207.LocationFloat = new DevExpress.Utils.PointFloat(15.33283F, 387.1672F);
            this.xrLabel207.Multiline = true;
            this.xrLabel207.Name = "xrLabel207";
            this.xrLabel207.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel207.SizeF = new System.Drawing.SizeF(250.7915F, 23F);
            this.xrLabel207.StylePriority.UseFont = false;
            this.xrLabel207.StylePriority.UseTextAlignment = false;
            this.xrLabel207.Text = "Sebab Berhenti";
            this.xrLabel207.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel204
            // 
            this.xrLabel204.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatPekerjaan.BidangTugas")});
            this.xrLabel204.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel204.LocationFloat = new DevExpress.Utils.PointFloat(299.1252F, 322.4167F);
            this.xrLabel204.Multiline = true;
            this.xrLabel204.Name = "xrLabel204";
            this.xrLabel204.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel204.SizeF = new System.Drawing.SizeF(329.3749F, 41.7504F);
            this.xrLabel204.StylePriority.UseFont = false;
            this.xrLabel204.StylePriority.UseTextAlignment = false;
            this.xrLabel204.Text = "MENYELENGGARA KOMPUTER";
            this.xrLabel204.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel203
            // 
            this.xrLabel203.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel203.LocationFloat = new DevExpress.Utils.PointFloat(13.50031F, 322.4172F);
            this.xrLabel203.Multiline = true;
            this.xrLabel203.Name = "xrLabel203";
            this.xrLabel203.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel203.SizeF = new System.Drawing.SizeF(250.7915F, 41.75F);
            this.xrLabel203.StylePriority.UseFont = false;
            this.xrLabel203.StylePriority.UseTextAlignment = false;
            this.xrLabel203.Text = "Bidang Tugass / Kerja Yang \r\nDilakukan";
            this.xrLabel203.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel202
            // 
            this.xrLabel202.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel202.LocationFloat = new DevExpress.Utils.PointFloat(271.3127F, 322.4168F);
            this.xrLabel202.Name = "xrLabel202";
            this.xrLabel202.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel202.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel202.StylePriority.UseFont = false;
            this.xrLabel202.StylePriority.UseTextAlignment = false;
            this.xrLabel202.Text = ":";
            this.xrLabel202.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel199
            // 
            this.xrLabel199.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel199.LocationFloat = new DevExpress.Utils.PointFloat(271.3126F, 276.4168F);
            this.xrLabel199.Name = "xrLabel199";
            this.xrLabel199.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel199.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel199.StylePriority.UseFont = false;
            this.xrLabel199.StylePriority.UseTextAlignment = false;
            this.xrLabel199.Text = ":";
            this.xrLabel199.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel198
            // 
            this.xrLabel198.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatPekerjaan.TarafJawatan")});
            this.xrLabel198.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel198.LocationFloat = new DevExpress.Utils.PointFloat(299.1251F, 299.417F);
            this.xrLabel198.Multiline = true;
            this.xrLabel198.Name = "xrLabel198";
            this.xrLabel198.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel198.SizeF = new System.Drawing.SizeF(329.3749F, 23F);
            this.xrLabel198.StylePriority.UseFont = false;
            this.xrLabel198.StylePriority.UseTextAlignment = false;
            this.xrLabel198.Text = "TETAP";
            this.xrLabel198.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel201
            // 
            this.xrLabel201.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatPekerjaan.Grade")});
            this.xrLabel201.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel201.LocationFloat = new DevExpress.Utils.PointFloat(299.1252F, 276.4169F);
            this.xrLabel201.Multiline = true;
            this.xrLabel201.Name = "xrLabel201";
            this.xrLabel201.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel201.SizeF = new System.Drawing.SizeF(329.3749F, 23.00006F);
            this.xrLabel201.StylePriority.UseFont = false;
            this.xrLabel201.StylePriority.UseTextAlignment = false;
            this.xrLabel201.Text = "N42";
            this.xrLabel201.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel200
            // 
            this.xrLabel200.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel200.LocationFloat = new DevExpress.Utils.PointFloat(271.3126F, 253.4168F);
            this.xrLabel200.Name = "xrLabel200";
            this.xrLabel200.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel200.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel200.StylePriority.UseFont = false;
            this.xrLabel200.StylePriority.UseTextAlignment = false;
            this.xrLabel200.Text = ":";
            this.xrLabel200.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel197
            // 
            this.xrLabel197.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel197.LocationFloat = new DevExpress.Utils.PointFloat(13.50015F, 299.4168F);
            this.xrLabel197.Multiline = true;
            this.xrLabel197.Name = "xrLabel197";
            this.xrLabel197.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel197.SizeF = new System.Drawing.SizeF(250.7915F, 23F);
            this.xrLabel197.StylePriority.UseFont = false;
            this.xrLabel197.StylePriority.UseTextAlignment = false;
            this.xrLabel197.Text = "Taraf Jawatan";
            this.xrLabel197.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel194
            // 
            this.xrLabel194.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel194.LocationFloat = new DevExpress.Utils.PointFloat(271.3126F, 299.4168F);
            this.xrLabel194.Name = "xrLabel194";
            this.xrLabel194.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel194.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel194.StylePriority.UseFont = false;
            this.xrLabel194.StylePriority.UseTextAlignment = false;
            this.xrLabel194.Text = ":";
            this.xrLabel194.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel193
            // 
            this.xrLabel193.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatPekerjaan.Jawatan")});
            this.xrLabel193.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel193.LocationFloat = new DevExpress.Utils.PointFloat(299.1251F, 253.4169F);
            this.xrLabel193.Multiline = true;
            this.xrLabel193.Name = "xrLabel193";
            this.xrLabel193.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel193.SizeF = new System.Drawing.SizeF(329.3749F, 23.00003F);
            this.xrLabel193.StylePriority.UseFont = false;
            this.xrLabel193.StylePriority.UseTextAlignment = false;
            this.xrLabel193.Text = "PEMBANTU TADBIR (KEJURUTERAAN) GRED 21";
            this.xrLabel193.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel196
            // 
            this.xrLabel196.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel196.LocationFloat = new DevExpress.Utils.PointFloat(13.50015F, 276.4168F);
            this.xrLabel196.Multiline = true;
            this.xrLabel196.Name = "xrLabel196";
            this.xrLabel196.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel196.SizeF = new System.Drawing.SizeF(250.7915F, 23F);
            this.xrLabel196.StylePriority.UseFont = false;
            this.xrLabel196.StylePriority.UseTextAlignment = false;
            this.xrLabel196.Text = "Gred";
            this.xrLabel196.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel195
            // 
            this.xrLabel195.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel195.LocationFloat = new DevExpress.Utils.PointFloat(13.50021F, 253.4168F);
            this.xrLabel195.Multiline = true;
            this.xrLabel195.Name = "xrLabel195";
            this.xrLabel195.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel195.SizeF = new System.Drawing.SizeF(250.7915F, 23F);
            this.xrLabel195.StylePriority.UseFont = false;
            this.xrLabel195.StylePriority.UseTextAlignment = false;
            this.xrLabel195.Text = "Jawatan";
            this.xrLabel195.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel190
            // 
            this.xrLabel190.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatPekerjaan.TempohTamat", "{0:dd/MM/yyyy}")});
            this.xrLabel190.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel190.LocationFloat = new DevExpress.Utils.PointFloat(299.1251F, 207.4169F);
            this.xrLabel190.Multiline = true;
            this.xrLabel190.Name = "xrLabel190";
            this.xrLabel190.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel190.SizeF = new System.Drawing.SizeF(329.3749F, 23.00006F);
            this.xrLabel190.StylePriority.UseFont = false;
            this.xrLabel190.StylePriority.UseTextAlignment = false;
            this.xrLabel190.Text = "1/10/2012";
            this.xrLabel190.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel189
            // 
            this.xrLabel189.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel189.LocationFloat = new DevExpress.Utils.PointFloat(271.3124F, 184.4168F);
            this.xrLabel189.Name = "xrLabel189";
            this.xrLabel189.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel189.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel189.StylePriority.UseFont = false;
            this.xrLabel189.StylePriority.UseTextAlignment = false;
            this.xrLabel189.Text = ":";
            this.xrLabel189.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel192
            // 
            this.xrLabel192.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel192.LocationFloat = new DevExpress.Utils.PointFloat(271.3124F, 207.4168F);
            this.xrLabel192.Name = "xrLabel192";
            this.xrLabel192.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel192.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel192.StylePriority.UseFont = false;
            this.xrLabel192.StylePriority.UseTextAlignment = false;
            this.xrLabel192.Text = ":";
            this.xrLabel192.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel191
            // 
            this.xrLabel191.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatPekerjaan.UnitBahagian")});
            this.xrLabel191.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel191.LocationFloat = new DevExpress.Utils.PointFloat(299.1249F, 230.4169F);
            this.xrLabel191.Multiline = true;
            this.xrLabel191.Name = "xrLabel191";
            this.xrLabel191.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel191.SizeF = new System.Drawing.SizeF(329.3749F, 23F);
            this.xrLabel191.StylePriority.UseFont = false;
            this.xrLabel191.StylePriority.UseTextAlignment = false;
            this.xrLabel191.Text = "AKADEMI PERCUKAIAN MALAYSIA";
            this.xrLabel191.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel188
            // 
            this.xrLabel188.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel188.LocationFloat = new DevExpress.Utils.PointFloat(13.49999F, 230.4168F);
            this.xrLabel188.Multiline = true;
            this.xrLabel188.Name = "xrLabel188";
            this.xrLabel188.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel188.SizeF = new System.Drawing.SizeF(250.7915F, 23F);
            this.xrLabel188.StylePriority.UseFont = false;
            this.xrLabel188.StylePriority.UseTextAlignment = false;
            this.xrLabel188.Text = "Unit / Bahagian";
            this.xrLabel188.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel185
            // 
            this.xrLabel185.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel185.LocationFloat = new DevExpress.Utils.PointFloat(13.49999F, 207.4168F);
            this.xrLabel185.Multiline = true;
            this.xrLabel185.Name = "xrLabel185";
            this.xrLabel185.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel185.SizeF = new System.Drawing.SizeF(250.7915F, 23F);
            this.xrLabel185.StylePriority.UseFont = false;
            this.xrLabel185.StylePriority.UseTextAlignment = false;
            this.xrLabel185.Text = "Tempoh Tamat";
            this.xrLabel185.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel184
            // 
            this.xrLabel184.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel184.LocationFloat = new DevExpress.Utils.PointFloat(13.50005F, 184.4168F);
            this.xrLabel184.Multiline = true;
            this.xrLabel184.Name = "xrLabel184";
            this.xrLabel184.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel184.SizeF = new System.Drawing.SizeF(250.7915F, 23F);
            this.xrLabel184.StylePriority.UseFont = false;
            this.xrLabel184.StylePriority.UseTextAlignment = false;
            this.xrLabel184.Text = "Tempoh Mula Berkhidmat";
            this.xrLabel184.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel187
            // 
            this.xrLabel187.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel187.LocationFloat = new DevExpress.Utils.PointFloat(271.3124F, 230.4168F);
            this.xrLabel187.Name = "xrLabel187";
            this.xrLabel187.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel187.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel187.StylePriority.UseFont = false;
            this.xrLabel187.StylePriority.UseTextAlignment = false;
            this.xrLabel187.Text = ":";
            this.xrLabel187.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel186
            // 
            this.xrLabel186.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatPekerjaan.MulaBerkihdmat", "{0:dd/MM/yyyy}")});
            this.xrLabel186.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel186.LocationFloat = new DevExpress.Utils.PointFloat(299.1249F, 184.4168F);
            this.xrLabel186.Multiline = true;
            this.xrLabel186.Name = "xrLabel186";
            this.xrLabel186.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel186.SizeF = new System.Drawing.SizeF(329.3749F, 23.00003F);
            this.xrLabel186.StylePriority.UseFont = false;
            this.xrLabel186.StylePriority.UseTextAlignment = false;
            this.xrLabel186.Text = "1/1/2008";
            this.xrLabel186.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel182
            // 
            this.xrLabel182.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel182.LocationFloat = new DevExpress.Utils.PointFloat(271.3125F, 161.4168F);
            this.xrLabel182.Name = "xrLabel182";
            this.xrLabel182.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel182.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel182.StylePriority.UseFont = false;
            this.xrLabel182.StylePriority.UseTextAlignment = false;
            this.xrLabel182.Text = ":";
            this.xrLabel182.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel175
            // 
            this.xrLabel175.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatPekerjaan.Poskod")});
            this.xrLabel175.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel175.LocationFloat = new DevExpress.Utils.PointFloat(299.125F, 161.4168F);
            this.xrLabel175.Multiline = true;
            this.xrLabel175.Name = "xrLabel175";
            this.xrLabel175.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel175.SizeF = new System.Drawing.SizeF(329.3749F, 23.00003F);
            this.xrLabel175.StylePriority.UseFont = false;
            this.xrLabel175.StylePriority.UseTextAlignment = false;
            this.xrLabel175.Text = "51000";
            this.xrLabel175.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel177
            // 
            this.xrLabel177.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel177.LocationFloat = new DevExpress.Utils.PointFloat(13.50002F, 161.4168F);
            this.xrLabel177.Multiline = true;
            this.xrLabel177.Name = "xrLabel177";
            this.xrLabel177.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel177.SizeF = new System.Drawing.SizeF(250.7915F, 23F);
            this.xrLabel177.StylePriority.UseFont = false;
            this.xrLabel177.StylePriority.UseTextAlignment = false;
            this.xrLabel177.Text = "Poskod";
            this.xrLabel177.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel174
            // 
            this.xrLabel174.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatPekerjaan.Alamat1")});
            this.xrLabel174.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel174.LocationFloat = new DevExpress.Utils.PointFloat(298.1249F, 74.75011F);
            this.xrLabel174.Multiline = true;
            this.xrLabel174.Name = "xrLabel174";
            this.xrLabel174.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel174.SizeF = new System.Drawing.SizeF(326.4584F, 39.66665F);
            this.xrLabel174.StylePriority.UseFont = false;
            this.xrLabel174.StylePriority.UseTextAlignment = false;
            this.xrLabel174.Text = "NO 10, JALAN SAGA\r\nBANGI";
            this.xrLabel174.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel173
            // 
            this.xrLabel173.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel173.LocationFloat = new DevExpress.Utils.PointFloat(12.49997F, 74.75011F);
            this.xrLabel173.Multiline = true;
            this.xrLabel173.Name = "xrLabel173";
            this.xrLabel173.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel173.SizeF = new System.Drawing.SizeF(251.7916F, 39.66665F);
            this.xrLabel173.StylePriority.UseFont = false;
            this.xrLabel173.StylePriority.UseTextAlignment = false;
            this.xrLabel173.Text = "Alamat Syarikat / Majikan / Jabatan /\r\nCawangan";
            this.xrLabel173.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel172
            // 
            this.xrLabel172.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel172.LocationFloat = new DevExpress.Utils.PointFloat(270.3124F, 74.75011F);
            this.xrLabel172.Name = "xrLabel172";
            this.xrLabel172.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel172.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel172.StylePriority.UseFont = false;
            this.xrLabel172.StylePriority.UseTextAlignment = false;
            this.xrLabel172.Text = ":";
            this.xrLabel172.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel168
            // 
            this.xrLabel168.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatPekerjaan.TelPejabat")});
            this.xrLabel168.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel168.LocationFloat = new DevExpress.Utils.PointFloat(298.1249F, 51.7501F);
            this.xrLabel168.Multiline = true;
            this.xrLabel168.Name = "xrLabel168";
            this.xrLabel168.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel168.SizeF = new System.Drawing.SizeF(326.4585F, 23.00001F);
            this.xrLabel168.StylePriority.UseFont = false;
            this.xrLabel168.StylePriority.UseTextAlignment = false;
            this.xrLabel168.Text = "0398722322";
            this.xrLabel168.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel167
            // 
            this.xrLabel167.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel167.LocationFloat = new DevExpress.Utils.PointFloat(12.49997F, 51.7501F);
            this.xrLabel167.Multiline = true;
            this.xrLabel167.Name = "xrLabel167";
            this.xrLabel167.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel167.SizeF = new System.Drawing.SizeF(251.7916F, 23.00001F);
            this.xrLabel167.StylePriority.UseFont = false;
            this.xrLabel167.StylePriority.UseTextAlignment = false;
            this.xrLabel167.Text = "No. Telefon Pejabat";
            this.xrLabel167.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel166
            // 
            this.xrLabel166.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel166.LocationFloat = new DevExpress.Utils.PointFloat(270.3124F, 51.75011F);
            this.xrLabel166.Name = "xrLabel166";
            this.xrLabel166.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel166.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel166.StylePriority.UseFont = false;
            this.xrLabel166.StylePriority.UseTextAlignment = false;
            this.xrLabel166.Text = ":";
            this.xrLabel166.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel165
            // 
            this.xrLabel165.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatPekerjaan.NamaSyarikat")});
            this.xrLabel165.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel165.LocationFloat = new DevExpress.Utils.PointFloat(298.125F, 9.999974F);
            this.xrLabel165.Multiline = true;
            this.xrLabel165.Name = "xrLabel165";
            this.xrLabel165.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel165.SizeF = new System.Drawing.SizeF(329.3749F, 23F);
            this.xrLabel165.StylePriority.UseFont = false;
            this.xrLabel165.StylePriority.UseTextAlignment = false;
            this.xrLabel165.Text = "LEMBAGA HASIL DALAM NEGERI MALAYSIA";
            this.xrLabel165.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel164
            // 
            this.xrLabel164.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel164.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 10.0001F);
            this.xrLabel164.Multiline = true;
            this.xrLabel164.Name = "xrLabel164";
            this.xrLabel164.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel164.SizeF = new System.Drawing.SizeF(251.7916F, 41.75F);
            this.xrLabel164.StylePriority.UseFont = false;
            this.xrLabel164.StylePriority.UseTextAlignment = false;
            this.xrLabel164.Text = "Nama Syarikat / Majikan / Jabatan /\r\nCawangan";
            this.xrLabel164.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel163
            // 
            this.xrLabel163.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel163.LocationFloat = new DevExpress.Utils.PointFloat(270.3125F, 9.999974F);
            this.xrLabel163.Name = "xrLabel163";
            this.xrLabel163.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel163.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel163.StylePriority.UseFont = false;
            this.xrLabel163.StylePriority.UseTextAlignment = false;
            this.xrLabel163.Text = ":";
            this.xrLabel163.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // GroupHeader12
            // 
            this.GroupHeader12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine15,
            this.xrLabel162});
            this.GroupHeader12.HeightF = 61.77082F;
            this.GroupHeader12.Name = "GroupHeader12";
            // 
            // xrLine15
            // 
            this.xrLine15.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrLine15.LineWidth = 2;
            this.xrLine15.LocationFloat = new DevExpress.Utils.PointFloat(5.328338F, 47.77089F);
            this.xrLine15.Name = "xrLine15";
            this.xrLine15.SizeF = new System.Drawing.SizeF(640.6249F, 2F);
            this.xrLine15.StylePriority.UseBorderDashStyle = false;
            // 
            // xrLabel162
            // 
            this.xrLabel162.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel162.LocationFloat = new DevExpress.Utils.PointFloat(5.328338F, 23.77084F);
            this.xrLabel162.Name = "xrLabel162";
            this.xrLabel162.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel162.SizeF = new System.Drawing.SizeF(642.8956F, 24.00002F);
            this.xrLabel162.StylePriority.UseFont = false;
            this.xrLabel162.StylePriority.UseTextAlignment = false;
            this.xrLabel162.Text = "13. MAKLUMAT PEKERJAAN";
            this.xrLabel162.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // GroupFooter12
            // 
            this.GroupFooter12.Name = "GroupFooter12";
            // 
            // maklumatPekerjaanTableAdapter
            // 
            this.maklumatPekerjaanTableAdapter.ClearBeforeFill = true;
            // 
            // LainLain_Maklumat
            // 
            this.LainLain_Maklumat.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail13,
            this.GroupHeader13,
            this.GroupFooter13});
            this.LainLain_Maklumat.DataAdapter = this.lainlainMaklumatTableAdapter;
            this.LainLain_Maklumat.DataMember = "LainlainMaklumat";
            this.LainLain_Maklumat.DataSource = this.repDataset1;
            this.LainLain_Maklumat.Expanded = false;
            this.LainLain_Maklumat.Level = 14;
            this.LainLain_Maklumat.Name = "LainLain_Maklumat";
            // 
            // Detail13
            // 
            this.Detail13.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrCheckBox11,
            this.xrCheckBox10,
            this.xrLabel214,
            this.xrLabel213});
            this.Detail13.HeightF = 83.00002F;
            this.Detail13.Name = "Detail13";
            // 
            // xrCheckBox11
            // 
            this.xrCheckBox11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("CheckState", null, "LainlainMaklumat.LainYa")});
            this.xrCheckBox11.LocationFloat = new DevExpress.Utils.PointFloat(466.0829F, 32.83326F);
            this.xrCheckBox11.Name = "xrCheckBox11";
            this.xrCheckBox11.SizeF = new System.Drawing.SizeF(18.75F, 23.00001F);
            this.xrCheckBox11.StylePriority.UseTextAlignment = false;
            // 
            // xrCheckBox10
            // 
            this.xrCheckBox10.Checked = true;
            this.xrCheckBox10.CheckState = System.Windows.Forms.CheckState.Checked;
            this.xrCheckBox10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("CheckState", null, "LainlainMaklumat.LainTidak")});
            this.xrCheckBox10.ForeColor = System.Drawing.Color.Black;
            this.xrCheckBox10.LocationFloat = new DevExpress.Utils.PointFloat(391.6667F, 32.83326F);
            this.xrCheckBox10.Name = "xrCheckBox10";
            this.xrCheckBox10.SizeF = new System.Drawing.SizeF(18.75F, 23.00001F);
            this.xrCheckBox10.StylePriority.UseForeColor = false;
            this.xrCheckBox10.StylePriority.UseTextAlignment = false;
            // 
            // xrLabel214
            // 
            this.xrLabel214.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "LainlainMaklumat.iSerialNo")});
            this.xrLabel214.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel214.LocationFloat = new DevExpress.Utils.PointFloat(8.041636F, 10F);
            this.xrLabel214.Multiline = true;
            this.xrLabel214.Name = "xrLabel214";
            this.xrLabel214.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel214.SizeF = new System.Drawing.SizeF(18.95825F, 25.08337F);
            this.xrLabel214.StylePriority.UseFont = false;
            this.xrLabel214.StylePriority.UseTextAlignment = false;
            this.xrLabel214.Text = "1.";
            this.xrLabel214.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel213
            // 
            this.xrLabel213.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "LainlainMaklumat.cQuestion")});
            this.xrLabel213.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel213.LocationFloat = new DevExpress.Utils.PointFloat(26.99989F, 10F);
            this.xrLabel213.Multiline = true;
            this.xrLabel213.Name = "xrLabel213";
            this.xrLabel213.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel213.SizeF = new System.Drawing.SizeF(329.3749F, 66.75003F);
            this.xrLabel213.StylePriority.UseFont = false;
            this.xrLabel213.StylePriority.UseTextAlignment = false;
            this.xrLabel213.Text = "Adakah anda sekarang seorang pekerja di\r\ndalam mana-mana Perkhidmatan Awam\r\natau " +
    "Badan Berkanun?";
            this.xrLabel213.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // GroupHeader13
            // 
            this.GroupHeader13.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine16,
            this.xrLabel211,
            this.xrLabel212,
            this.xrLabel169,
            this.xrLabel170});
            this.GroupHeader13.HeightF = 69.79166F;
            this.GroupHeader13.Name = "GroupHeader13";
            // 
            // xrLine16
            // 
            this.xrLine16.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrLine16.LineWidth = 2;
            this.xrLine16.LocationFloat = new DevExpress.Utils.PointFloat(3.552216F, 34.00004F);
            this.xrLine16.Name = "xrLine16";
            this.xrLine16.SizeF = new System.Drawing.SizeF(640.6249F, 2F);
            this.xrLine16.StylePriority.UseBorderDashStyle = false;
            // 
            // xrLabel211
            // 
            this.xrLabel211.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel211.LocationFloat = new DevExpress.Utils.PointFloat(3.552216F, 10F);
            this.xrLabel211.Name = "xrLabel211";
            this.xrLabel211.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel211.SizeF = new System.Drawing.SizeF(642.8956F, 24.00002F);
            this.xrLabel211.StylePriority.UseFont = false;
            this.xrLabel211.StylePriority.UseTextAlignment = false;
            this.xrLabel211.Text = "14. LAIN-LAIN MAKLUMAT";
            this.xrLabel211.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel212
            // 
            this.xrLabel212.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel212.LocationFloat = new DevExpress.Utils.PointFloat(8.041636F, 46.79162F);
            this.xrLabel212.Multiline = true;
            this.xrLabel212.Name = "xrLabel212";
            this.xrLabel212.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel212.SizeF = new System.Drawing.SizeF(329.3749F, 23.00003F);
            this.xrLabel212.StylePriority.UseFont = false;
            this.xrLabel212.StylePriority.UseTextAlignment = false;
            this.xrLabel212.Text = "* Jika Ya , sila berikan butir-butir lanjut.";
            this.xrLabel212.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel169
            // 
            this.xrLabel169.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel169.LocationFloat = new DevExpress.Utils.PointFloat(385.5413F, 46.79155F);
            this.xrLabel169.Multiline = true;
            this.xrLabel169.Name = "xrLabel169";
            this.xrLabel169.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel169.SizeF = new System.Drawing.SizeF(38.9371F, 23.00003F);
            this.xrLabel169.StylePriority.UseFont = false;
            this.xrLabel169.StylePriority.UseTextAlignment = false;
            this.xrLabel169.Text = "Ya";
            this.xrLabel169.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel170
            // 
            this.xrLabel170.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel170.LocationFloat = new DevExpress.Utils.PointFloat(455.2117F, 46.79155F);
            this.xrLabel170.Multiline = true;
            this.xrLabel170.Name = "xrLabel170";
            this.xrLabel170.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel170.SizeF = new System.Drawing.SizeF(38.9371F, 23.00003F);
            this.xrLabel170.StylePriority.UseFont = false;
            this.xrLabel170.StylePriority.UseTextAlignment = false;
            this.xrLabel170.Text = "Tidak";
            this.xrLabel170.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // GroupFooter13
            // 
            this.GroupFooter13.Name = "GroupFooter13";
            // 
            // lainlainMaklumatTableAdapter
            // 
            this.lainlainMaklumatTableAdapter.ClearBeforeFill = true;
            // 
            // Rujukan
            // 
            this.Rujukan.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail14,
            this.GroupHeader14,
            this.GroupFooter14});
            this.Rujukan.DataAdapter = this.maklumatrujukanTableAdapter;
            this.Rujukan.DataMember = "maklumatrujukan";
            this.Rujukan.DataSource = this.repDataset1;
            this.Rujukan.Expanded = false;
            this.Rujukan.Level = 15;
            this.Rujukan.Name = "Rujukan";
            // 
            // Detail14
            // 
            this.Detail14.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel261,
            this.xrLabel260,
            this.xrLabel259,
            this.xrLabel256,
            this.xrLabel257,
            this.xrLabel258,
            this.xrLabel253,
            this.xrLabel254,
            this.xrLabel255,
            this.xrLabel252,
            this.xrLabel251,
            this.xrLabel250,
            this.xrLabel234,
            this.xrLabel233,
            this.xrLabel232,
            this.xrLabel249,
            this.xrLabel248,
            this.xrLabel247,
            this.xrLabel231,
            this.xrLabel230,
            this.xrLabel228,
            this.xrLabel237,
            this.xrLabel236,
            this.xrLabel229,
            this.xrLabel226,
            this.xrLabel227,
            this.xrLabel235});
            this.Detail14.HeightF = 275.0489F;
            this.Detail14.Name = "Detail14";
            // 
            // xrLabel261
            // 
            this.xrLabel261.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel261.LocationFloat = new DevExpress.Utils.PointFloat(15.33292F, 237.7295F);
            this.xrLabel261.Multiline = true;
            this.xrLabel261.Name = "xrLabel261";
            this.xrLabel261.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel261.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel261.StylePriority.UseFont = false;
            this.xrLabel261.StylePriority.UseTextAlignment = false;
            this.xrLabel261.Text = "No. Telefon Pejabat";
            this.xrLabel261.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel260
            // 
            this.xrLabel260.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatrujukan.CompanyTelNo")});
            this.xrLabel260.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel260.LocationFloat = new DevExpress.Utils.PointFloat(298.1249F, 237.7296F);
            this.xrLabel260.Multiline = true;
            this.xrLabel260.Name = "xrLabel260";
            this.xrLabel260.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel260.SizeF = new System.Drawing.SizeF(332.3751F, 23F);
            this.xrLabel260.StylePriority.UseFont = false;
            this.xrLabel260.StylePriority.UseTextAlignment = false;
            this.xrLabel260.Text = "0165856321";
            this.xrLabel260.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel259
            // 
            this.xrLabel259.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel259.LocationFloat = new DevExpress.Utils.PointFloat(273.3125F, 237.7295F);
            this.xrLabel259.Name = "xrLabel259";
            this.xrLabel259.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel259.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel259.StylePriority.UseFont = false;
            this.xrLabel259.StylePriority.UseTextAlignment = false;
            this.xrLabel259.Text = ":";
            this.xrLabel259.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel256
            // 
            this.xrLabel256.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel256.LocationFloat = new DevExpress.Utils.PointFloat(15.33292F, 214.7295F);
            this.xrLabel256.Multiline = true;
            this.xrLabel256.Name = "xrLabel256";
            this.xrLabel256.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel256.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel256.StylePriority.UseFont = false;
            this.xrLabel256.StylePriority.UseTextAlignment = false;
            this.xrLabel256.Text = "No. Telefon Bimbit";
            this.xrLabel256.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel257
            // 
            this.xrLabel257.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatrujukan.HpNo")});
            this.xrLabel257.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel257.LocationFloat = new DevExpress.Utils.PointFloat(299.1252F, 214.7296F);
            this.xrLabel257.Multiline = true;
            this.xrLabel257.Name = "xrLabel257";
            this.xrLabel257.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel257.SizeF = new System.Drawing.SizeF(329.3749F, 23F);
            this.xrLabel257.StylePriority.UseFont = false;
            this.xrLabel257.StylePriority.UseTextAlignment = false;
            this.xrLabel257.Text = "0165856321";
            this.xrLabel257.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel258
            // 
            this.xrLabel258.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel258.LocationFloat = new DevExpress.Utils.PointFloat(273.3125F, 191.7295F);
            this.xrLabel258.Name = "xrLabel258";
            this.xrLabel258.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel258.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel258.StylePriority.UseFont = false;
            this.xrLabel258.StylePriority.UseTextAlignment = false;
            this.xrLabel258.Text = ":";
            this.xrLabel258.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel253
            // 
            this.xrLabel253.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatrujukan.TelNo")});
            this.xrLabel253.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel253.LocationFloat = new DevExpress.Utils.PointFloat(299.1252F, 191.7296F);
            this.xrLabel253.Multiline = true;
            this.xrLabel253.Name = "xrLabel253";
            this.xrLabel253.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel253.SizeF = new System.Drawing.SizeF(329.3749F, 23.00006F);
            this.xrLabel253.StylePriority.UseFont = false;
            this.xrLabel253.StylePriority.UseTextAlignment = false;
            this.xrLabel253.Text = "0333452167";
            this.xrLabel253.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel254
            // 
            this.xrLabel254.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel254.LocationFloat = new DevExpress.Utils.PointFloat(15.33292F, 191.7295F);
            this.xrLabel254.Multiline = true;
            this.xrLabel254.Name = "xrLabel254";
            this.xrLabel254.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel254.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel254.StylePriority.UseFont = false;
            this.xrLabel254.StylePriority.UseTextAlignment = false;
            this.xrLabel254.Text = "No. Telefon Rumah";
            this.xrLabel254.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel255
            // 
            this.xrLabel255.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel255.LocationFloat = new DevExpress.Utils.PointFloat(273.3125F, 214.7295F);
            this.xrLabel255.Name = "xrLabel255";
            this.xrLabel255.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel255.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel255.StylePriority.UseFont = false;
            this.xrLabel255.StylePriority.UseTextAlignment = false;
            this.xrLabel255.Text = ":";
            this.xrLabel255.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel252
            // 
            this.xrLabel252.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel252.LocationFloat = new DevExpress.Utils.PointFloat(273.3125F, 98.79189F);
            this.xrLabel252.Name = "xrLabel252";
            this.xrLabel252.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel252.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel252.StylePriority.UseFont = false;
            this.xrLabel252.StylePriority.UseTextAlignment = false;
            this.xrLabel252.Text = ":";
            this.xrLabel252.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel251
            // 
            this.xrLabel251.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatrujukan.Jawatan")});
            this.xrLabel251.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel251.LocationFloat = new DevExpress.Utils.PointFloat(300.9581F, 145.7295F);
            this.xrLabel251.Multiline = true;
            this.xrLabel251.Name = "xrLabel251";
            this.xrLabel251.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel251.SizeF = new System.Drawing.SizeF(329.3749F, 23.00006F);
            this.xrLabel251.StylePriority.UseFont = false;
            this.xrLabel251.StylePriority.UseTextAlignment = false;
            this.xrLabel251.Text = "CEO";
            this.xrLabel251.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel250
            // 
            this.xrLabel250.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel250.LocationFloat = new DevExpress.Utils.PointFloat(15.33292F, 98.79188F);
            this.xrLabel250.Multiline = true;
            this.xrLabel250.Name = "xrLabel250";
            this.xrLabel250.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel250.SizeF = new System.Drawing.SizeF(242.2917F, 45.47392F);
            this.xrLabel250.StylePriority.UseFont = false;
            this.xrLabel250.StylePriority.UseTextAlignment = false;
            this.xrLabel250.Text = "Tempoh Mengenali Diri Anda\r\n(Tahun)";
            this.xrLabel250.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel234
            // 
            this.xrLabel234.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel234.LocationFloat = new DevExpress.Utils.PointFloat(15.33292F, 145.7295F);
            this.xrLabel234.Multiline = true;
            this.xrLabel234.Name = "xrLabel234";
            this.xrLabel234.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel234.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel234.StylePriority.UseFont = false;
            this.xrLabel234.StylePriority.UseTextAlignment = false;
            this.xrLabel234.Text = "Jawatan";
            this.xrLabel234.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel233
            // 
            this.xrLabel233.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel233.LocationFloat = new DevExpress.Utils.PointFloat(273.3125F, 168.7295F);
            this.xrLabel233.Name = "xrLabel233";
            this.xrLabel233.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel233.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel233.StylePriority.UseFont = false;
            this.xrLabel233.StylePriority.UseTextAlignment = false;
            this.xrLabel233.Text = ":";
            this.xrLabel233.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel232
            // 
            this.xrLabel232.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel232.LocationFloat = new DevExpress.Utils.PointFloat(273.3125F, 145.7295F);
            this.xrLabel232.Name = "xrLabel232";
            this.xrLabel232.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel232.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel232.StylePriority.UseFont = false;
            this.xrLabel232.StylePriority.UseTextAlignment = false;
            this.xrLabel232.Text = ":";
            this.xrLabel232.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel249
            // 
            this.xrLabel249.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatrujukan.TahunMengenali")});
            this.xrLabel249.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel249.LocationFloat = new DevExpress.Utils.PointFloat(300.9579F, 98.79188F);
            this.xrLabel249.Multiline = true;
            this.xrLabel249.Name = "xrLabel249";
            this.xrLabel249.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel249.SizeF = new System.Drawing.SizeF(329.3749F, 23.00003F);
            this.xrLabel249.StylePriority.UseFont = false;
            this.xrLabel249.StylePriority.UseTextAlignment = false;
            this.xrLabel249.Text = "5 TAHUN";
            this.xrLabel249.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel248
            // 
            this.xrLabel248.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel248.LocationFloat = new DevExpress.Utils.PointFloat(15.33292F, 168.7295F);
            this.xrLabel248.Multiline = true;
            this.xrLabel248.Name = "xrLabel248";
            this.xrLabel248.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel248.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel248.StylePriority.UseFont = false;
            this.xrLabel248.StylePriority.UseTextAlignment = false;
            this.xrLabel248.Text = "Nama Majikan";
            this.xrLabel248.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel247
            // 
            this.xrLabel247.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatrujukan.Majikan")});
            this.xrLabel247.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel247.LocationFloat = new DevExpress.Utils.PointFloat(300.9581F, 168.7296F);
            this.xrLabel247.Multiline = true;
            this.xrLabel247.Name = "xrLabel247";
            this.xrLabel247.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel247.SizeF = new System.Drawing.SizeF(329.3749F, 23F);
            this.xrLabel247.StylePriority.UseFont = false;
            this.xrLabel247.StylePriority.UseTextAlignment = false;
            this.xrLabel247.Text = "MYSYSNET SDN BHD";
            this.xrLabel247.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel231
            // 
            this.xrLabel231.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel231.LocationFloat = new DevExpress.Utils.PointFloat(15.33292F, 75.79189F);
            this.xrLabel231.Multiline = true;
            this.xrLabel231.Name = "xrLabel231";
            this.xrLabel231.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel231.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel231.StylePriority.UseFont = false;
            this.xrLabel231.StylePriority.UseTextAlignment = false;
            this.xrLabel231.Text = "Hubungan";
            this.xrLabel231.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel230
            // 
            this.xrLabel230.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatrujukan.Hubungan")});
            this.xrLabel230.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel230.LocationFloat = new DevExpress.Utils.PointFloat(300.9581F, 75.79189F);
            this.xrLabel230.Multiline = true;
            this.xrLabel230.Name = "xrLabel230";
            this.xrLabel230.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel230.SizeF = new System.Drawing.SizeF(329.3749F, 23F);
            this.xrLabel230.StylePriority.UseFont = false;
            this.xrLabel230.StylePriority.UseTextAlignment = false;
            this.xrLabel230.Text = "CEO";
            this.xrLabel230.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel228
            // 
            this.xrLabel228.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel228.LocationFloat = new DevExpress.Utils.PointFloat(273.3125F, 75.79189F);
            this.xrLabel228.Name = "xrLabel228";
            this.xrLabel228.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel228.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel228.StylePriority.UseFont = false;
            this.xrLabel228.StylePriority.UseTextAlignment = false;
            this.xrLabel228.Text = ":";
            this.xrLabel228.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel237
            // 
            this.xrLabel237.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel237.LocationFloat = new DevExpress.Utils.PointFloat(273.3125F, 33.00001F);
            this.xrLabel237.Name = "xrLabel237";
            this.xrLabel237.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel237.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel237.StylePriority.UseFont = false;
            this.xrLabel237.StylePriority.UseTextAlignment = false;
            this.xrLabel237.Text = ":";
            this.xrLabel237.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel236
            // 
            this.xrLabel236.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel236.LocationFloat = new DevExpress.Utils.PointFloat(15.33292F, 33.0001F);
            this.xrLabel236.Multiline = true;
            this.xrLabel236.Name = "xrLabel236";
            this.xrLabel236.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel236.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel236.StylePriority.UseFont = false;
            this.xrLabel236.StylePriority.UseTextAlignment = false;
            this.xrLabel236.Text = "Alamat Rumah / Pejabat";
            this.xrLabel236.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel229
            // 
            this.xrLabel229.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatrujukan.Nama")});
            this.xrLabel229.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel229.LocationFloat = new DevExpress.Utils.PointFloat(301.1251F, 10.0001F);
            this.xrLabel229.Multiline = true;
            this.xrLabel229.Name = "xrLabel229";
            this.xrLabel229.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel229.SizeF = new System.Drawing.SizeF(329.3749F, 23.0001F);
            this.xrLabel229.StylePriority.UseFont = false;
            this.xrLabel229.StylePriority.UseTextAlignment = false;
            this.xrLabel229.Text = "FAROUK YEW ABDULLAH";
            this.xrLabel229.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel226
            // 
            this.xrLabel226.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel226.LocationFloat = new DevExpress.Utils.PointFloat(15.33292F, 10.0001F);
            this.xrLabel226.Multiline = true;
            this.xrLabel226.Name = "xrLabel226";
            this.xrLabel226.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel226.SizeF = new System.Drawing.SizeF(242.2916F, 23F);
            this.xrLabel226.StylePriority.UseFont = false;
            this.xrLabel226.StylePriority.UseTextAlignment = false;
            this.xrLabel226.Text = "Nama";
            this.xrLabel226.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel227
            // 
            this.xrLabel227.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel227.LocationFloat = new DevExpress.Utils.PointFloat(273.3125F, 10.00001F);
            this.xrLabel227.Name = "xrLabel227";
            this.xrLabel227.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel227.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel227.StylePriority.UseFont = false;
            this.xrLabel227.StylePriority.UseTextAlignment = false;
            this.xrLabel227.Text = ":";
            this.xrLabel227.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel235
            // 
            this.xrLabel235.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatrujukan.Alamat")});
            this.xrLabel235.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel235.LocationFloat = new DevExpress.Utils.PointFloat(300.9578F, 33.0002F);
            this.xrLabel235.Multiline = true;
            this.xrLabel235.Name = "xrLabel235";
            this.xrLabel235.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel235.SizeF = new System.Drawing.SizeF(329.3749F, 42.79169F);
            this.xrLabel235.StylePriority.UseFont = false;
            this.xrLabel235.StylePriority.UseTextAlignment = false;
            this.xrLabel235.Text = "NO 12, JALAN RUMIA\r\nSETIA TROPIKA";
            this.xrLabel235.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // GroupHeader14
            // 
            this.GroupHeader14.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine17,
            this.xrLabel225});
            this.GroupHeader14.HeightF = 70.83334F;
            this.GroupHeader14.Name = "GroupHeader14";
            // 
            // xrLine17
            // 
            this.xrLine17.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrLine17.LineWidth = 2;
            this.xrLine17.LocationFloat = new DevExpress.Utils.PointFloat(3.552246F, 61.00002F);
            this.xrLine17.Name = "xrLine17";
            this.xrLine17.SizeF = new System.Drawing.SizeF(640.6249F, 2F);
            this.xrLine17.StylePriority.UseBorderDashStyle = false;
            // 
            // xrLabel225
            // 
            this.xrLabel225.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel225.LocationFloat = new DevExpress.Utils.PointFloat(3.552246F, 36.99998F);
            this.xrLabel225.Name = "xrLabel225";
            this.xrLabel225.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel225.SizeF = new System.Drawing.SizeF(642.8956F, 24.00002F);
            this.xrLabel225.StylePriority.UseFont = false;
            this.xrLabel225.StylePriority.UseTextAlignment = false;
            this.xrLabel225.Text = "15. RUJUKAN";
            this.xrLabel225.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // GroupFooter14
            // 
            this.GroupFooter14.Name = "GroupFooter14";
            // 
            // maklumatrujukanTableAdapter
            // 
            this.maklumatrujukanTableAdapter.ClearBeforeFill = true;
            // 
            // Pengakuan_Pemohon
            // 
            this.Pengakuan_Pemohon.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail15,
            this.GroupHeader15,
            this.GroupFooter15});
            this.Pengakuan_Pemohon.DataSource = this.repDataset1;
            this.Pengakuan_Pemohon.Expanded = false;
            this.Pengakuan_Pemohon.Level = 16;
            this.Pengakuan_Pemohon.Name = "Pengakuan_Pemohon";
            this.Pengakuan_Pemohon.Visible = false;
            // 
            // Detail15
            // 
            this.Detail15.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel300,
            this.xrLabel299,
            this.xrLabel298,
            this.xrLabel297,
            this.xrLabel296,
            this.xrLabel295,
            this.xrLabel294,
            this.xrLabel293,
            this.xrLabel291,
            this.xrLabel290,
            this.xrLabel289,
            this.xrLabel288,
            this.xrLabel287,
            this.xrLabel286,
            this.xrLabel285,
            this.xrLabel284,
            this.xrLabel283,
            this.xrLabel282,
            this.xrLabel281,
            this.xrLabel280,
            this.xrLabel279,
            this.xrLabel278,
            this.xrLabel277,
            this.xrLabel276,
            this.xrLabel275,
            this.xrLabel274,
            this.xrLabel273,
            this.xrLabel272,
            this.xrLabel271,
            this.xrLabel270,
            this.xrLabel269,
            this.xrLabel268,
            this.xrLabel267,
            this.xrLabel266,
            this.xrLabel292});
            this.Detail15.HeightF = 479.8752F;
            this.Detail15.Name = "Detail15";
            // 
            // xrLabel300
            // 
            this.xrLabel300.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel300.LocationFloat = new DevExpress.Utils.PointFloat(270.1873F, 410.6669F);
            this.xrLabel300.Multiline = true;
            this.xrLabel300.Name = "xrLabel300";
            this.xrLabel300.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel300.SizeF = new System.Drawing.SizeF(373.4791F, 25.0834F);
            this.xrLabel300.StylePriority.UseFont = false;
            this.xrLabel300.StylePriority.UseTextAlignment = false;
            this.xrLabel300.Text = "atau kedua-duanya sekali\".";
            this.xrLabel300.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel299
            // 
            this.xrLabel299.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel299.LocationFloat = new DevExpress.Utils.PointFloat(151.0416F, 410.6669F);
            this.xrLabel299.Multiline = true;
            this.xrLabel299.Name = "xrLabel299";
            this.xrLabel299.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel299.SizeF = new System.Drawing.SizeF(118.25F, 25.0834F);
            this.xrLabel299.StylePriority.UseFont = false;
            this.xrLabel299.StylePriority.UseTextAlignment = false;
            this.xrLabel299.Text = "didenda RM2000";
            this.xrLabel299.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel298
            // 
            this.xrLabel298.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel298.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 410.6669F);
            this.xrLabel298.Multiline = true;
            this.xrLabel298.Name = "xrLabel298";
            this.xrLabel298.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel298.SizeF = new System.Drawing.SizeF(109.2914F, 25.0834F);
            this.xrLabel298.StylePriority.UseFont = false;
            this.xrLabel298.StylePriority.UseTextAlignment = false;
            this.xrLabel298.Text = "penjara 2 tahun";
            this.xrLabel298.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel297
            // 
            this.xrLabel297.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel297.LocationFloat = new DevExpress.Utils.PointFloat(119.2914F, 410.6669F);
            this.xrLabel297.Multiline = true;
            this.xrLabel297.Name = "xrLabel297";
            this.xrLabel297.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel297.SizeF = new System.Drawing.SizeF(31.75023F, 25.0834F);
            this.xrLabel297.StylePriority.UseFont = false;
            this.xrLabel297.StylePriority.UseTextAlignment = false;
            this.xrLabel297.Text = "atau";
            this.xrLabel297.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel296
            // 
            this.xrLabel296.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel296.LocationFloat = new DevExpress.Utils.PointFloat(276.5831F, 385.5835F);
            this.xrLabel296.Multiline = true;
            this.xrLabel296.Name = "xrLabel296";
            this.xrLabel296.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel296.SizeF = new System.Drawing.SizeF(368.2086F, 25.0834F);
            this.xrLabel296.StylePriority.UseFont = false;
            this.xrLabel296.StylePriority.UseTextAlignment = false;
            this.xrLabel296.Text = "dalam borang permohonan jika disabitkan boleh dihukum";
            this.xrLabel296.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel295
            // 
            this.xrLabel295.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel295.LocationFloat = new DevExpress.Utils.PointFloat(167.2918F, 385.5835F);
            this.xrLabel295.Multiline = true;
            this.xrLabel295.Name = "xrLabel295";
            this.xrLabel295.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel295.SizeF = new System.Drawing.SizeF(109.2914F, 25.0834F);
            this.xrLabel295.StylePriority.UseFont = false;
            this.xrLabel295.StylePriority.UseTextAlignment = false;
            this.xrLabel295.Text = "maklumat palsu";
            this.xrLabel295.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel294
            // 
            this.xrLabel294.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel294.LocationFloat = new DevExpress.Utils.PointFloat(7.406107F, 385.5835F);
            this.xrLabel294.Multiline = true;
            this.xrLabel294.Name = "xrLabel294";
            this.xrLabel294.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel294.SizeF = new System.Drawing.SizeF(159.8856F, 25.0834F);
            this.xrLabel294.StylePriority.UseFont = false;
            this.xrLabel294.StylePriority.UseTextAlignment = false;
            this.xrLabel294.Text = "pemohon yang memberi";
            this.xrLabel294.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel293
            // 
            this.xrLabel293.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel293.LocationFloat = new DevExpress.Utils.PointFloat(6.958326F, 360.5001F);
            this.xrLabel293.Multiline = true;
            this.xrLabel293.Name = "xrLabel293";
            this.xrLabel293.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel293.SizeF = new System.Drawing.SizeF(637.2188F, 25.0834F);
            this.xrLabel293.StylePriority.UseFont = false;
            this.xrLabel293.StylePriority.UseTextAlignment = false;
            this.xrLabel293.Text = "undang di bawah Seksyen 5 Akta Suruhanjaya Perkhidmatan 1957 ( Semakan 1989 ), \"s" +
    "eseorang";
            this.xrLabel293.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel291
            // 
            this.xrLabel291.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel291.LocationFloat = new DevExpress.Utils.PointFloat(207.5412F, 335.4167F);
            this.xrLabel291.Multiline = true;
            this.xrLabel291.Name = "xrLabel291";
            this.xrLabel291.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel291.SizeF = new System.Drawing.SizeF(116.6252F, 25.0834F);
            this.xrLabel291.StylePriority.UseFont = false;
            this.xrLabel291.StylePriority.UseTextAlignment = false;
            this.xrLabel291.Text = "maklumat palsu";
            this.xrLabel291.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel290
            // 
            this.xrLabel290.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel290.LocationFloat = new DevExpress.Utils.PointFloat(310.6247F, 286.9583F);
            this.xrLabel290.Name = "xrLabel290";
            this.xrLabel290.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel290.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel290.StylePriority.UseFont = false;
            this.xrLabel290.StylePriority.UseTextAlignment = false;
            this.xrLabel290.Text = ":";
            this.xrLabel290.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel289
            // 
            this.xrLabel289.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel289.LocationFloat = new DevExpress.Utils.PointFloat(310.6247F, 251.5416F);
            this.xrLabel289.Name = "xrLabel289";
            this.xrLabel289.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel289.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel289.StylePriority.UseFont = false;
            this.xrLabel289.StylePriority.UseTextAlignment = false;
            this.xrLabel289.Text = ":";
            this.xrLabel289.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel288
            // 
            this.xrLabel288.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel288.LocationFloat = new DevExpress.Utils.PointFloat(310.6247F, 216.125F);
            this.xrLabel288.Name = "xrLabel288";
            this.xrLabel288.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel288.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel288.StylePriority.UseFont = false;
            this.xrLabel288.StylePriority.UseTextAlignment = false;
            this.xrLabel288.Text = ":";
            this.xrLabel288.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel287
            // 
            this.xrLabel287.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel287.LocationFloat = new DevExpress.Utils.PointFloat(334.0623F, 286.9583F);
            this.xrLabel287.Multiline = true;
            this.xrLabel287.Name = "xrLabel287";
            this.xrLabel287.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel287.SizeF = new System.Drawing.SizeF(159.0032F, 25.0834F);
            this.xrLabel287.StylePriority.UseFont = false;
            this.xrLabel287.StylePriority.UseTextAlignment = false;
            this.xrLabel287.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel286
            // 
            this.xrLabel286.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel286.LocationFloat = new DevExpress.Utils.PointFloat(334.0623F, 251.5416F);
            this.xrLabel286.Multiline = true;
            this.xrLabel286.Name = "xrLabel286";
            this.xrLabel286.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel286.SizeF = new System.Drawing.SizeF(159.0032F, 25.08342F);
            this.xrLabel286.StylePriority.UseFont = false;
            this.xrLabel286.StylePriority.UseTextAlignment = false;
            this.xrLabel286.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel285
            // 
            this.xrLabel285.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pGetCVPersonalInfo.NRIC")});
            this.xrLabel285.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel285.LocationFloat = new DevExpress.Utils.PointFloat(334.0623F, 216.125F);
            this.xrLabel285.Multiline = true;
            this.xrLabel285.Name = "xrLabel285";
            this.xrLabel285.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel285.SizeF = new System.Drawing.SizeF(159.0032F, 25.0834F);
            this.xrLabel285.StylePriority.UseFont = false;
            this.xrLabel285.StylePriority.UseTextAlignment = false;
            this.xrLabel285.Text = "840610016205";
            this.xrLabel285.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel284
            // 
            this.xrLabel284.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel284.LocationFloat = new DevExpress.Utils.PointFloat(142.7499F, 286.9583F);
            this.xrLabel284.Multiline = true;
            this.xrLabel284.Name = "xrLabel284";
            this.xrLabel284.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel284.SizeF = new System.Drawing.SizeF(155.7917F, 25.0834F);
            this.xrLabel284.StylePriority.UseFont = false;
            this.xrLabel284.StylePriority.UseTextAlignment = false;
            this.xrLabel284.Text = "No. Rujukan";
            this.xrLabel284.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel283
            // 
            this.xrLabel283.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel283.LocationFloat = new DevExpress.Utils.PointFloat(142.7499F, 251.5417F);
            this.xrLabel283.Multiline = true;
            this.xrLabel283.Name = "xrLabel283";
            this.xrLabel283.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel283.SizeF = new System.Drawing.SizeF(155.7917F, 25.0834F);
            this.xrLabel283.StylePriority.UseFont = false;
            this.xrLabel283.StylePriority.UseTextAlignment = false;
            this.xrLabel283.Text = "Tarikh";
            this.xrLabel283.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel282
            // 
            this.xrLabel282.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel282.LocationFloat = new DevExpress.Utils.PointFloat(142.7499F, 216.125F);
            this.xrLabel282.Multiline = true;
            this.xrLabel282.Name = "xrLabel282";
            this.xrLabel282.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel282.SizeF = new System.Drawing.SizeF(155.7917F, 25.0834F);
            this.xrLabel282.StylePriority.UseFont = false;
            this.xrLabel282.StylePriority.UseTextAlignment = false;
            this.xrLabel282.Text = "No. Kad Pengenalan";
            this.xrLabel282.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel281
            // 
            this.xrLabel281.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel281.LocationFloat = new DevExpress.Utils.PointFloat(6.958326F, 335.4167F);
            this.xrLabel281.Multiline = true;
            this.xrLabel281.Name = "xrLabel281";
            this.xrLabel281.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel281.SizeF = new System.Drawing.SizeF(200.5828F, 25.0834F);
            this.xrLabel281.StylePriority.UseFont = false;
            this.xrLabel281.StylePriority.UseTextAlignment = false;
            this.xrLabel281.Text = "Pemohon yang mengemukakan";
            this.xrLabel281.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel280
            // 
            this.xrLabel280.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel280.LocationFloat = new DevExpress.Utils.PointFloat(257.6245F, 160.5006F);
            this.xrLabel280.Multiline = true;
            this.xrLabel280.Name = "xrLabel280";
            this.xrLabel280.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel280.SizeF = new System.Drawing.SizeF(155.7917F, 25.0834F);
            this.xrLabel280.StylePriority.UseFont = false;
            this.xrLabel280.StylePriority.UseTextAlignment = false;
            this.xrLabel280.Text = "tanpa apa-apa syarat.";
            this.xrLabel280.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel279
            // 
            this.xrLabel279.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel279.LocationFloat = new DevExpress.Utils.PointFloat(6.000026F, 160.5006F);
            this.xrLabel279.Multiline = true;
            this.xrLabel279.Name = "xrLabel279";
            this.xrLabel279.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel279.SizeF = new System.Drawing.SizeF(251.6245F, 25.0834F);
            this.xrLabel279.StylePriority.UseFont = false;
            this.xrLabel279.StylePriority.UseTextAlignment = false;
            this.xrLabel279.Text = "perkhidmatan saya dengan serta-merta";
            this.xrLabel279.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel278
            // 
            this.xrLabel278.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel278.LocationFloat = new DevExpress.Utils.PointFloat(161.7918F, 135.4172F);
            this.xrLabel278.Multiline = true;
            this.xrLabel278.Name = "xrLabel278";
            this.xrLabel278.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel278.SizeF = new System.Drawing.SizeF(482.3854F, 25.0834F);
            this.xrLabel278.StylePriority.UseFont = false;
            this.xrLabel278.StylePriority.UseTextAlignment = false;
            this.xrLabel278.Text = ", pihak Lembaga Hasil Dalam Negeri Malaysia berhak menamatkan ";
            this.xrLabel278.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel277
            // 
            this.xrLabel277.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel277.LocationFloat = new DevExpress.Utils.PointFloat(6.000026F, 135.4172F);
            this.xrLabel277.Multiline = true;
            this.xrLabel277.Name = "xrLabel277";
            this.xrLabel277.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel277.SizeF = new System.Drawing.SizeF(155.7917F, 25.0834F);
            this.xrLabel277.StylePriority.UseFont = false;
            this.xrLabel277.StylePriority.UseTextAlignment = false;
            this.xrLabel277.Text = "palsu atau tidak benar";
            this.xrLabel277.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel276
            // 
            this.xrLabel276.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel276.LocationFloat = new DevExpress.Utils.PointFloat(6.00001F, 110.3338F);
            this.xrLabel276.Multiline = true;
            this.xrLabel276.Name = "xrLabel276";
            this.xrLabel276.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel276.SizeF = new System.Drawing.SizeF(639.6249F, 25.08341F);
            this.xrLabel276.StylePriority.UseFont = false;
            this.xrLabel276.StylePriority.UseTextAlignment = false;
            this.xrLabel276.Text = "perkhidmatan saya. Saya bersetuju dan menerima bahawa jika mana-mana dari pengaku" +
    "an ini didapati";
            this.xrLabel276.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel275
            // 
            this.xrLabel275.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel275.LocationFloat = new DevExpress.Utils.PointFloat(98.54164F, 85.25034F);
            this.xrLabel275.Multiline = true;
            this.xrLabel275.Name = "xrLabel275";
            this.xrLabel275.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel275.SizeF = new System.Drawing.SizeF(547.9061F, 25.0834F);
            this.xrLabel275.StylePriority.UseFont = false;
            this.xrLabel275.StylePriority.UseTextAlignment = false;
            this.xrLabel275.Text = " segala maklumat mengenai Lembaga Hasil Dalam Negeri Malaysia sepanjang ";
            this.xrLabel275.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel274
            // 
            this.xrLabel274.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel274.LocationFloat = new DevExpress.Utils.PointFloat(6.00001F, 85.25034F);
            this.xrLabel274.Multiline = true;
            this.xrLabel274.Name = "xrLabel274";
            this.xrLabel274.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel274.SizeF = new System.Drawing.SizeF(92.54164F, 25.08341F);
            this.xrLabel274.StylePriority.UseFont = false;
            this.xrLabel274.StylePriority.UseTextAlignment = false;
            this.xrLabel274.Text = "merahsiakan";
            this.xrLabel274.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel273
            // 
            this.xrLabel273.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel273.LocationFloat = new DevExpress.Utils.PointFloat(6.00001F, 60.16693F);
            this.xrLabel273.Multiline = true;
            this.xrLabel273.Name = "xrLabel273";
            this.xrLabel273.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel273.SizeF = new System.Drawing.SizeF(640.4478F, 25.08341F);
            this.xrLabel273.StylePriority.UseFont = false;
            this.xrLabel273.StylePriority.UseTextAlignment = false;
            this.xrLabel273.Text = "di antara saya dengan pihak Lembaga Hasil Dalam Negeri Malaysia. Saya juga berjan" +
    "ji akan ";
            this.xrLabel273.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel272
            // 
            this.xrLabel272.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel272.LocationFloat = new DevExpress.Utils.PointFloat(466.0829F, 35.08352F);
            this.xrLabel272.Multiline = true;
            this.xrLabel272.Name = "xrLabel272";
            this.xrLabel272.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel272.SizeF = new System.Drawing.SizeF(180.3649F, 25.08341F);
            this.xrLabel272.StylePriority.UseFont = false;
            this.xrLabel272.StylePriority.UseTextAlignment = false;
            this.xrLabel272.Text = "perjanjian perkhidmatan";
            this.xrLabel272.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel271
            // 
            this.xrLabel271.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel271.LocationFloat = new DevExpress.Utils.PointFloat(78.08333F, 35.08352F);
            this.xrLabel271.Multiline = true;
            this.xrLabel271.Name = "xrLabel271";
            this.xrLabel271.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel271.SizeF = new System.Drawing.SizeF(387.9996F, 25.08341F);
            this.xrLabel271.StylePriority.UseFont = false;
            this.xrLabel271.StylePriority.UseTextAlignment = false;
            this.xrLabel271.Text = "Jika sekiranya saya diambil bekerja, pengakuan ini merupakan";
            this.xrLabel271.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel270
            // 
            this.xrLabel270.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel270.LocationFloat = new DevExpress.Utils.PointFloat(6.000007F, 35.08352F);
            this.xrLabel270.Multiline = true;
            this.xrLabel270.Name = "xrLabel270";
            this.xrLabel270.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel270.SizeF = new System.Drawing.SizeF(72.08331F, 25.08341F);
            this.xrLabel270.StylePriority.UseFont = false;
            this.xrLabel270.StylePriority.UseTextAlignment = false;
            this.xrLabel270.Text = "dan tepat.";
            this.xrLabel270.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel269
            // 
            this.xrLabel269.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel269.LocationFloat = new DevExpress.Utils.PointFloat(580.1456F, 10.0001F);
            this.xrLabel269.Multiline = true;
            this.xrLabel269.Name = "xrLabel269";
            this.xrLabel269.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel269.SizeF = new System.Drawing.SizeF(65.85443F, 25.08341F);
            this.xrLabel269.StylePriority.UseFont = false;
            this.xrLabel269.StylePriority.UseTextAlignment = false;
            this.xrLabel269.Text = "benar";
            this.xrLabel269.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel268
            // 
            this.xrLabel268.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel268.LocationFloat = new DevExpress.Utils.PointFloat(180.3749F, 10.0001F);
            this.xrLabel268.Multiline = true;
            this.xrLabel268.Name = "xrLabel268";
            this.xrLabel268.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel268.SizeF = new System.Drawing.SizeF(399.7707F, 25.08341F);
            this.xrLabel268.StylePriority.UseFont = false;
            this.xrLabel268.StylePriority.UseTextAlignment = false;
            this.xrLabel268.Text = "segala maklumat yang saya berikan di dalam borang ini adalah";
            this.xrLabel268.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel267
            // 
            this.xrLabel267.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel267.LocationFloat = new DevExpress.Utils.PointFloat(109.3333F, 10.0001F);
            this.xrLabel267.Multiline = true;
            this.xrLabel267.Name = "xrLabel267";
            this.xrLabel267.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel267.SizeF = new System.Drawing.SizeF(71.04162F, 25.08341F);
            this.xrLabel267.StylePriority.UseFont = false;
            this.xrLabel267.StylePriority.UseTextAlignment = false;
            this.xrLabel267.Text = "mengaku";
            this.xrLabel267.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel266
            // 
            this.xrLabel266.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel266.LocationFloat = new DevExpress.Utils.PointFloat(6.000007F, 10.0001F);
            this.xrLabel266.Multiline = true;
            this.xrLabel266.Name = "xrLabel266";
            this.xrLabel266.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel266.SizeF = new System.Drawing.SizeF(103.3333F, 25.08341F);
            this.xrLabel266.StylePriority.UseFont = false;
            this.xrLabel266.StylePriority.UseTextAlignment = false;
            this.xrLabel266.Text = "Saya dengan ini ";
            this.xrLabel266.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel292
            // 
            this.xrLabel292.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel292.LocationFloat = new DevExpress.Utils.PointFloat(324.1664F, 335.4166F);
            this.xrLabel292.Multiline = true;
            this.xrLabel292.Name = "xrLabel292";
            this.xrLabel292.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel292.SizeF = new System.Drawing.SizeF(321.4585F, 25.0834F);
            this.xrLabel292.StylePriority.UseFont = false;
            this.xrLabel292.StylePriority.UseTextAlignment = false;
            this.xrLabel292.Text = "akan disenarai hitam dan boleh diambil tindakan ";
            this.xrLabel292.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // GroupHeader15
            // 
            this.GroupHeader15.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine18,
            this.xrLabel265});
            this.GroupHeader15.HeightF = 71.875F;
            this.GroupHeader15.Name = "GroupHeader15";
            // 
            // xrLine18
            // 
            this.xrLine18.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrLine18.LineWidth = 2;
            this.xrLine18.LocationFloat = new DevExpress.Utils.PointFloat(3.552216F, 61.00002F);
            this.xrLine18.Name = "xrLine18";
            this.xrLine18.SizeF = new System.Drawing.SizeF(640.6249F, 2F);
            this.xrLine18.StylePriority.UseBorderDashStyle = false;
            // 
            // xrLabel265
            // 
            this.xrLabel265.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel265.LocationFloat = new DevExpress.Utils.PointFloat(3.552216F, 36.99998F);
            this.xrLabel265.Name = "xrLabel265";
            this.xrLabel265.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel265.SizeF = new System.Drawing.SizeF(642.8956F, 24.00002F);
            this.xrLabel265.StylePriority.UseFont = false;
            this.xrLabel265.StylePriority.UseTextAlignment = false;
            this.xrLabel265.Text = "16. PENGAKUAN PEMOHON";
            this.xrLabel265.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // GroupFooter15
            // 
            this.GroupFooter15.Name = "GroupFooter15";
            // 
            // Maklumat_Kecacatan
            // 
            this.Maklumat_Kecacatan.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail16,
            this.GroupHeader16,
            this.GroupFooter16});
            this.Maklumat_Kecacatan.DataAdapter = this.maklumatkecacatanTableAdapter;
            this.Maklumat_Kecacatan.DataMember = "maklumatkecacatan";
            this.Maklumat_Kecacatan.DataSource = this.repDataset1;
            this.Maklumat_Kecacatan.Expanded = false;
            this.Maklumat_Kecacatan.Level = 3;
            this.Maklumat_Kecacatan.Name = "Maklumat_Kecacatan";
            // 
            // Detail16
            // 
            this.Detail16.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel106,
            this.xrLabel306,
            this.xrLabel301,
            this.xrCheckBox22});
            this.Detail16.HeightF = 23.00003F;
            this.Detail16.Name = "Detail16";
            // 
            // xrLabel106
            // 
            this.xrLabel106.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatkecacatan.cRemarks")});
            this.xrLabel106.LocationFloat = new DevExpress.Utils.PointFloat(348.5208F, 0F);
            this.xrLabel106.Name = "xrLabel106";
            this.xrLabel106.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel106.SizeF = new System.Drawing.SizeF(286.8076F, 23F);
            this.xrLabel106.Text = "xrLabel106";
            // 
            // xrLabel306
            // 
            this.xrLabel306.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel306.LocationFloat = new DevExpress.Utils.PointFloat(332.4165F, 0F);
            this.xrLabel306.Name = "xrLabel306";
            this.xrLabel306.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel306.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel306.StylePriority.UseFont = false;
            this.xrLabel306.StylePriority.UseTextAlignment = false;
            this.xrLabel306.Text = ":";
            this.xrLabel306.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel301
            // 
            this.xrLabel301.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatkecacatan.cQuestion")});
            this.xrLabel301.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel301.LocationFloat = new DevExpress.Utils.PointFloat(68.54179F, 0F);
            this.xrLabel301.Multiline = true;
            this.xrLabel301.Name = "xrLabel301";
            this.xrLabel301.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel301.SizeF = new System.Drawing.SizeF(263.8747F, 23.00003F);
            this.xrLabel301.StylePriority.UseFont = false;
            this.xrLabel301.StylePriority.UseTextAlignment = false;
            this.xrLabel301.Text = "Anggota Badan";
            this.xrLabel301.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrCheckBox22
            // 
            this.xrCheckBox22.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("CheckState", null, "maklumatkecacatan.TandaKecacatan")});
            this.xrCheckBox22.LocationFloat = new DevExpress.Utils.PointFloat(18.00014F, 0F);
            this.xrCheckBox22.Name = "xrCheckBox22";
            this.xrCheckBox22.SizeF = new System.Drawing.SizeF(18.75F, 23.00001F);
            this.xrCheckBox22.StylePriority.UseTextAlignment = false;
            // 
            // GroupHeader16
            // 
            this.GroupHeader16.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel40,
            this.xrCheckBox21,
            this.xrLabel39,
            this.xrLabel37,
            this.xrLabel27,
            this.xrLabel26,
            this.xrLabel25});
            this.GroupHeader16.Expanded = false;
            this.GroupHeader16.HeightF = 107.7083F;
            this.GroupHeader16.Name = "GroupHeader16";
            // 
            // xrLabel40
            // 
            this.xrLabel40.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(98.54164F, 47.58345F);
            this.xrLabel40.Multiline = true;
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel40.SizeF = new System.Drawing.SizeF(539.1663F, 23.00003F);
            this.xrLabel40.StylePriority.UseFont = false;
            this.xrLabel40.StylePriority.UseTextAlignment = false;
            this.xrLabel40.Text = "pada kotak yang berkenaan dan sila nyatakan butir-butir lanjut.";
            this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrCheckBox21
            // 
            this.xrCheckBox21.Checked = true;
            this.xrCheckBox21.CheckState = System.Windows.Forms.CheckState.Checked;
            this.xrCheckBox21.LocationFloat = new DevExpress.Utils.PointFloat(79.79164F, 47.58345F);
            this.xrCheckBox21.Name = "xrCheckBox21";
            this.xrCheckBox21.SizeF = new System.Drawing.SizeF(18.75F, 23.00001F);
            this.xrCheckBox21.StylePriority.UseTextAlignment = false;
            // 
            // xrLabel39
            // 
            this.xrLabel39.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(10.20832F, 47.58345F);
            this.xrLabel39.Multiline = true;
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel39.SizeF = new System.Drawing.SizeF(69.58332F, 23.00003F);
            this.xrLabel39.StylePriority.UseFont = false;
            this.xrLabel39.StylePriority.UseTextAlignment = false;
            this.xrLabel39.Text = "Tandakan";
            this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel37
            // 
            this.xrLabel37.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Underline);
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(349.4372F, 84.70827F);
            this.xrLabel37.Multiline = true;
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(280.8955F, 23.00003F);
            this.xrLabel37.StylePriority.UseFont = false;
            this.xrLabel37.StylePriority.UseTextAlignment = false;
            this.xrLabel37.Text = "Butir-butir Lanjut";
            this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel27
            // 
            this.xrLabel27.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Underline);
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(68.54169F, 84.70827F);
            this.xrLabel27.Multiline = true;
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(280.8955F, 23.00003F);
            this.xrLabel27.StylePriority.UseFont = false;
            this.xrLabel27.StylePriority.UseTextAlignment = false;
            this.xrLabel27.Text = "Penyakit";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel26
            // 
            this.xrLabel26.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Underline);
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 84.70827F);
            this.xrLabel26.Multiline = true;
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(42.49999F, 23.00003F);
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.StylePriority.UseTextAlignment = false;
            this.xrLabel26.Text = "Pilih";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(8.041636F, 24.58344F);
            this.xrLabel25.Multiline = true;
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(630F, 23.00003F);
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.StylePriority.UseTextAlignment = false;
            this.xrLabel25.Text = "2. Maklumat Kecacatan";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // GroupFooter16
            // 
            this.GroupFooter16.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel319,
            this.xrLabel318,
            this.xrLabel317,
            this.xrLabel316,
            this.xrLabel315,
            this.xrLabel314,
            this.xrLabel313,
            this.xrLabel312,
            this.xrCheckBox27,
            this.xrLabel311,
            this.xrCheckBox26,
            this.xrLabel310});
            this.GroupFooter16.HeightF = 157.9246F;
            this.GroupFooter16.Name = "GroupFooter16";
            // 
            // xrLabel319
            // 
            this.xrLabel319.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatfizikal.cWeight", "{0:#,# kg}")});
            this.xrLabel319.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel319.LocationFloat = new DevExpress.Utils.PointFloat(291.0416F, 122.3541F);
            this.xrLabel319.Multiline = true;
            this.xrLabel319.Name = "xrLabel319";
            this.xrLabel319.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel319.SizeF = new System.Drawing.SizeF(60.74976F, 23F);
            this.xrLabel319.StylePriority.UseFont = false;
            this.xrLabel319.StylePriority.UseTextAlignment = false;
            this.xrLabel319.Text = "80 Kg";
            this.xrLabel319.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel318
            // 
            this.xrLabel318.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel318.LocationFloat = new DevExpress.Utils.PointFloat(271.5441F, 122.3541F);
            this.xrLabel318.Name = "xrLabel318";
            this.xrLabel318.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel318.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel318.StylePriority.UseFont = false;
            this.xrLabel318.StylePriority.UseTextAlignment = false;
            this.xrLabel318.Text = ":";
            this.xrLabel318.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel317
            // 
            this.xrLabel317.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel317.LocationFloat = new DevExpress.Utils.PointFloat(180.3749F, 122.3541F);
            this.xrLabel317.Multiline = true;
            this.xrLabel317.Name = "xrLabel317";
            this.xrLabel317.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel317.SizeF = new System.Drawing.SizeF(88.9165F, 23F);
            this.xrLabel317.StylePriority.UseFont = false;
            this.xrLabel317.StylePriority.UseTextAlignment = false;
            this.xrLabel317.Text = "Berat Badan";
            this.xrLabel317.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel316
            // 
            this.xrLabel316.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "maklumatfizikal.cHeight", "{0:#,# m}")});
            this.xrLabel316.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel316.LocationFloat = new DevExpress.Utils.PointFloat(90.29189F, 122.3541F);
            this.xrLabel316.Multiline = true;
            this.xrLabel316.Name = "xrLabel316";
            this.xrLabel316.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel316.SizeF = new System.Drawing.SizeF(60.74976F, 23F);
            this.xrLabel316.StylePriority.UseFont = false;
            this.xrLabel316.StylePriority.UseTextAlignment = false;
            this.xrLabel316.Text = "1.79m";
            this.xrLabel316.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel315
            // 
            this.xrLabel315.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel315.LocationFloat = new DevExpress.Utils.PointFloat(70.79437F, 122.3541F);
            this.xrLabel315.Name = "xrLabel315";
            this.xrLabel315.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel315.SizeF = new System.Drawing.SizeF(13.54167F, 23F);
            this.xrLabel315.StylePriority.UseFont = false;
            this.xrLabel315.StylePriority.UseTextAlignment = false;
            this.xrLabel315.Text = ":";
            this.xrLabel315.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel314
            // 
            this.xrLabel314.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel314.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 122.3541F);
            this.xrLabel314.Multiline = true;
            this.xrLabel314.Name = "xrLabel314";
            this.xrLabel314.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel314.SizeF = new System.Drawing.SizeF(58.54168F, 23F);
            this.xrLabel314.StylePriority.UseFont = false;
            this.xrLabel314.StylePriority.UseTextAlignment = false;
            this.xrLabel314.Text = "Tinggi";
            this.xrLabel314.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel313
            // 
            this.xrLabel313.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel313.LocationFloat = new DevExpress.Utils.PointFloat(5.328337F, 87.5F);
            this.xrLabel313.Multiline = true;
            this.xrLabel313.Name = "xrLabel313";
            this.xrLabel313.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel313.SizeF = new System.Drawing.SizeF(630F, 23.00003F);
            this.xrLabel313.StylePriority.UseFont = false;
            this.xrLabel313.StylePriority.UseTextAlignment = false;
            this.xrLabel313.Text = "4. Ukuran Fizikal";
            this.xrLabel313.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel312
            // 
            this.xrLabel312.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel312.LocationFloat = new DevExpress.Utils.PointFloat(119.7058F, 44.87502F);
            this.xrLabel312.Multiline = true;
            this.xrLabel312.Name = "xrLabel312";
            this.xrLabel312.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel312.SizeF = new System.Drawing.SizeF(47.58591F, 23.00003F);
            this.xrLabel312.StylePriority.UseFont = false;
            this.xrLabel312.StylePriority.UseTextAlignment = false;
            this.xrLabel312.Text = "TIDAK";
            this.xrLabel312.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrCheckBox27
            // 
            this.xrCheckBox27.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("CheckState", null, "maklumatfizikal.NonSmoker")});
            this.xrCheckBox27.LocationFloat = new DevExpress.Utils.PointFloat(100.9558F, 44.87502F);
            this.xrCheckBox27.Name = "xrCheckBox27";
            this.xrCheckBox27.SizeF = new System.Drawing.SizeF(18.75F, 23.00001F);
            this.xrCheckBox27.StylePriority.UseTextAlignment = false;
            // 
            // xrLabel311
            // 
            this.xrLabel311.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel311.LocationFloat = new DevExpress.Utils.PointFloat(36.75014F, 44.87498F);
            this.xrLabel311.Multiline = true;
            this.xrLabel311.Name = "xrLabel311";
            this.xrLabel311.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel311.SizeF = new System.Drawing.SizeF(47.58591F, 23.00003F);
            this.xrLabel311.StylePriority.UseFont = false;
            this.xrLabel311.StylePriority.UseTextAlignment = false;
            this.xrLabel311.Text = "YA";
            this.xrLabel311.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrCheckBox26
            // 
            this.xrCheckBox26.Checked = true;
            this.xrCheckBox26.CheckState = System.Windows.Forms.CheckState.Checked;
            this.xrCheckBox26.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("CheckState", null, "maklumatfizikal.lSmoker")});
            this.xrCheckBox26.LocationFloat = new DevExpress.Utils.PointFloat(18.00014F, 44.87502F);
            this.xrCheckBox26.Name = "xrCheckBox26";
            this.xrCheckBox26.SizeF = new System.Drawing.SizeF(18.75F, 23.00001F);
            this.xrCheckBox26.StylePriority.UseTextAlignment = false;
            // 
            // xrLabel310
            // 
            this.xrLabel310.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xrLabel310.LocationFloat = new DevExpress.Utils.PointFloat(4.166667F, 21.875F);
            this.xrLabel310.Multiline = true;
            this.xrLabel310.Name = "xrLabel310";
            this.xrLabel310.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel310.SizeF = new System.Drawing.SizeF(630F, 23.00003F);
            this.xrLabel310.StylePriority.UseFont = false;
            this.xrLabel310.StylePriority.UseTextAlignment = false;
            this.xrLabel310.Text = "3. Adakah anda seorang perokok ?";
            this.xrLabel310.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // maklumatkecacatanTableAdapter
            // 
            this.maklumatkecacatanTableAdapter.ClearBeforeFill = true;
            // 
            // Maklumat_Pasangan
            // 
            this.Maklumat_Pasangan.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail17,
            this.GroupHeader17,
            this.GroupFooter17});
            this.Maklumat_Pasangan.DataSource = this.repDataset1;
            this.Maklumat_Pasangan.Expanded = false;
            this.Maklumat_Pasangan.Level = 0;
            this.Maklumat_Pasangan.Name = "Maklumat_Pasangan";
            // 
            // Detail17
            // 
            this.Detail17.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel96,
            this.xrLabel118,
            this.xrLabel116,
            this.xrLabel113,
            this.xrLabel115,
            this.xrLabel114,
            this.xrLabel107,
            this.xrLabel105,
            this.xrLabel108,
            this.xrLabel111,
            this.xrLabel110,
            this.xrLabel109,
            this.xrLabel97,
            this.xrLabel98,
            this.xrLabel95,
            this.xrLabel101,
            this.xrLabel99,
            this.xrLabel102,
            this.xrLabel103,
            this.xrLabel100,
            this.xrLabel117});
            this.Detail17.Expanded = false;
            this.Detail17.HeightF = 195.0762F;
            this.Detail17.Name = "Detail17";
            // 
            // GroupHeader17
            // 
            this.GroupHeader17.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel94,
            this.xrLine4});
            this.GroupHeader17.HeightF = 59.375F;
            this.GroupHeader17.Name = "GroupHeader17";
            // 
            // GroupFooter17
            // 
            this.GroupFooter17.Expanded = false;
            this.GroupFooter17.Name = "GroupFooter17";
            // 
            // FullAddress
            // 
            this.FullAddress.DataMember = "pGetCVPersonalInfo";
            this.FullAddress.Expression = "[Address1]+Environment.NewLine+[Address2]+Environment.NewLine+[Address3]";
            this.FullAddress.Name = "FullAddress";
            // 
            // OtherFullAddress
            // 
            this.OtherFullAddress.DataMember = "pGetCVPersonalInfo";
            this.OtherFullAddress.Expression = "[OtherAddress1]+Environment.NewLine+[OtherAddress2]+Environment.NewLine+[OtherAdd" +
    "ress3]";
            this.OtherFullAddress.Name = "OtherFullAddress";
            // 
            // Tanda
            // 
            this.Tanda.DataMember = "pGetCVPhysicalHealthinfo";
            this.Tanda.Expression = "Iif([cResult] == \'N\',False  ,True )";
            this.Tanda.Name = "Tanda";
            // 
            // NonSmoker
            // 
            this.NonSmoker.DataMember = "maklumatfizikal";
            this.NonSmoker.Expression = "![lSmoker]";
            this.NonSmoker.Name = "NonSmoker";
            // 
            // maklumatPasanganTableAdapter
            // 
            this.maklumatPasanganTableAdapter.ClearBeforeFill = true;
            // 
            // maklumatfizikalTableAdapter
            // 
            this.maklumatfizikalTableAdapter.ClearBeforeFill = true;
            // 
            // TandaKecacatan
            // 
            this.TandaKecacatan.DataMember = "maklumatkecacatan";
            this.TandaKecacatan.Expression = "Iif([cResult] == \'N\',False  ,True )";
            this.TandaKecacatan.Name = "TandaKecacatan";
            // 
            // BahasaLemah
            // 
            this.BahasaLemah.DataMember = "maklumatKemahiranBahasa";
            this.BahasaLemah.Expression = "Iif([cCodeLevel] == \'1\',TRUE  ,FALSE )";
            this.BahasaLemah.Name = "BahasaLemah";
            // 
            // BahasaSederhana
            // 
            this.BahasaSederhana.DataMember = "maklumatKemahiranBahasa";
            this.BahasaSederhana.Expression = "Iif([cCodeLevel] == \'2\',TRUE  ,FALSE )";
            this.BahasaSederhana.Name = "BahasaSederhana";
            // 
            // BahasaBaik
            // 
            this.BahasaBaik.DataMember = "maklumatKemahiranBahasa";
            this.BahasaBaik.Expression = "Iif([cCodeLevel] == \'3\',TRUE  ,FALSE )";
            this.BahasaBaik.Name = "BahasaBaik";
            // 
            // TahunSekolah
            // 
            this.TahunSekolah.DataMember = "maklumatSekolah";
            this.TahunSekolah.Expression = "[cYearFrom]+\'-\'+[cYearTo]";
            this.TahunSekolah.Name = "TahunSekolah";
            // 
            // maklumatHobiTableAdapter
            // 
            this.maklumatHobiTableAdapter.ClearBeforeFill = true;
            // 
            // LainYa
            // 
            this.LainYa.DataMember = "LainlainMaklumat";
            this.LainYa.Expression = "Iif([cResult] == \'Y\',False  ,True )";
            this.LainYa.Name = "LainYa";
            // 
            // LainTidak
            // 
            this.LainTidak.DataMember = "LainlainMaklumat";
            this.LainTidak.Expression = "Iif([cResult] == \'N\',False  ,True )";
            this.LainTidak.Name = "LainTidak";
            // 
            // pGetCVPersonalInfoTableAdapter
            // 
            this.pGetCVPersonalInfoTableAdapter.ClearBeforeFill = true;
            // 
            // Rptcvrep
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.ReportFooter,
            this.Maklumat_Waris,
            this.Maklumat_KesihatanFizikal,
            this.Kemahiran_Bahasa,
            this.Maklumat_Persekolahan,
            this.Keputusan_Peperiksaan,
            this.Maklumat_Institusi_Pengajian,
            this.Maklumat_Badan_Profesional,
            this.Maklumat_KPSL,
            this.Maklumat_Sijil_Kemahiran,
            this.Maklumat_Kemahiran_Komputer,
            this.Maklumat_Kegiatan_Luar,
            this.Maklumat_Pekerjaan,
            this.LainLain_Maklumat,
            this.Rujukan,
            this.Pengakuan_Pemohon,
            this.Maklumat_Kecacatan,
            this.Maklumat_Pasangan});
            this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.FullAddress,
            this.OtherFullAddress,
            this.Tanda,
            this.NonSmoker,
            this.TandaKecacatan,
            this.BahasaLemah,
            this.BahasaSederhana,
            this.BahasaBaik,
            this.TahunSekolah,
            this.LainYa,
            this.LainTidak});
            this.DataAdapter = this.pGetCVPersonalInfoTableAdapter;
            this.DataSource = this.repDataset1;
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 50, 100);
            this.Version = "11.2";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Rptcvrep_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repDataset1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel49;
        private DevExpress.XtraReports.UI.XRLabel xrLabel48;
        private DevExpress.XtraReports.UI.XRLabel xrLabel51;
        private DevExpress.XtraReports.UI.XRLabel xrLabel50;
        private DevExpress.XtraReports.UI.XRLabel xrLabel47;
        private DevExpress.XtraReports.UI.XRLabel xrLabel44;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        private DevExpress.XtraReports.UI.XRLabel xrLabel45;
        private DevExpress.XtraReports.UI.XRLabel xrLabel42;
        private DevExpress.XtraReports.UI.XRLabel xrLabel41;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRLabel xrLabel93;
        private DevExpress.XtraReports.UI.XRLabel xrLabel92;
        private DevExpress.XtraReports.UI.XRLabel xrLabel91;
        private DevExpress.XtraReports.UI.XRLabel xrLabel84;
        private DevExpress.XtraReports.UI.XRLabel xrLabel85;
        private DevExpress.XtraReports.UI.XRLabel xrLabel83;
        private DevExpress.XtraReports.UI.XRLabel xrLabel81;
        private DevExpress.XtraReports.UI.XRLabel xrLabel82;
        private DevExpress.XtraReports.UI.XRLabel xrLabel89;
        private DevExpress.XtraReports.UI.XRLabel xrLabel90;
        private DevExpress.XtraReports.UI.XRLabel xrLabel88;
        private DevExpress.XtraReports.UI.XRLabel xrLabel86;
        private DevExpress.XtraReports.UI.XRLabel xrLabel87;
        private DevExpress.XtraReports.UI.XRLabel xrLabel80;
        private DevExpress.XtraReports.UI.XRLabel xrLabel73;
        private DevExpress.XtraReports.UI.XRLabel xrLabel74;
        private DevExpress.XtraReports.UI.XRLabel xrLabel72;
        private DevExpress.XtraReports.UI.XRLabel xrLabel70;
        private DevExpress.XtraReports.UI.XRLabel xrLabel71;
        private DevExpress.XtraReports.UI.XRLabel xrLabel78;
        private DevExpress.XtraReports.UI.XRLabel xrLabel79;
        private DevExpress.XtraReports.UI.XRLabel xrLabel77;
        private DevExpress.XtraReports.UI.XRLabel xrLabel75;
        private DevExpress.XtraReports.UI.XRLabel xrLabel76;
        private DevExpress.XtraReports.UI.XRLabel xrLabel63;
        private DevExpress.XtraReports.UI.XRLabel xrLabel64;
        private DevExpress.XtraReports.UI.XRLabel xrLabel61;
        private DevExpress.XtraReports.UI.XRLabel xrLabel62;
        private DevExpress.XtraReports.UI.XRLabel xrLabel65;
        private DevExpress.XtraReports.UI.XRLabel xrLabel68;
        private DevExpress.XtraReports.UI.XRLabel xrLabel69;
        private DevExpress.XtraReports.UI.XRLabel xrLabel66;
        private DevExpress.XtraReports.UI.XRLabel xrLabel67;
        private DevExpress.XtraReports.UI.XRLabel xrLabel54;
        private DevExpress.XtraReports.UI.XRLabel xrLabel55;
        private DevExpress.XtraReports.UI.XRLabel xrLabel52;
        private DevExpress.XtraReports.UI.XRLabel xrLabel53;
        private DevExpress.XtraReports.UI.XRLabel xrLabel56;
        private DevExpress.XtraReports.UI.XRLabel xrLabel59;
        private DevExpress.XtraReports.UI.XRLabel xrLabel60;
        private DevExpress.XtraReports.UI.XRLabel xrLabel57;
        private DevExpress.XtraReports.UI.XRLabel xrLabel58;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel94;
        private DevExpress.XtraReports.UI.XRLabel xrLabel96;
        private DevExpress.XtraReports.UI.XRLabel xrLabel100;
        private DevExpress.XtraReports.UI.XRLabel xrLabel103;
        private DevExpress.XtraReports.UI.XRLabel xrLabel102;
        private DevExpress.XtraReports.UI.XRLabel xrLabel99;
        private DevExpress.XtraReports.UI.XRLabel xrLabel101;
        private DevExpress.XtraReports.UI.XRLabel xrLabel95;
        private DevExpress.XtraReports.UI.XRLabel xrLabel98;
        private DevExpress.XtraReports.UI.XRLabel xrLabel97;
        private DevExpress.XtraReports.UI.XRLabel xrLabel109;
        private DevExpress.XtraReports.UI.XRLabel xrLabel110;
        private DevExpress.XtraReports.UI.XRLabel xrLabel111;
        private DevExpress.XtraReports.UI.XRLabel xrLabel108;
        private DevExpress.XtraReports.UI.XRLabel xrLabel105;
        private DevExpress.XtraReports.UI.XRLabel xrLabel107;
        private DevExpress.XtraReports.UI.XRLabel xrLabel114;
        private DevExpress.XtraReports.UI.XRLabel xrLabel115;
        private DevExpress.XtraReports.UI.XRLabel xrLabel113;
        private DevExpress.XtraReports.UI.XRLabel xrLabel116;
        private DevExpress.XtraReports.UI.XRLabel xrLabel118;
        private DevExpress.XtraReports.UI.XRLabel xrLabel117;
        private DevExpress.XtraReports.UI.XRLabel xrLabel128;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.DetailReportBand Maklumat_Waris;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.DetailReportBand Maklumat_KesihatanFizikal;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel135;
        private DevExpress.XtraReports.UI.XRLabel xrLabel134;
        private DevExpress.XtraReports.UI.XRLabel xrLabel133;
        private DevExpress.XtraReports.UI.XRLabel xrLabel132;
        private DevExpress.XtraReports.UI.XRCheckBox xrCheckBox1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel131;
        private DevExpress.XtraReports.UI.XRLabel xrLabel130;
        private DevExpress.XtraReports.UI.XRLabel xrLabel129;
        private DevExpress.XtraReports.UI.XRLine xrLine6;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel136;
        private DevExpress.XtraReports.UI.XRCheckBox xrCheckBox2;
        private DevExpress.XtraReports.UI.DetailReportBand Kemahiran_Bahasa;
        private DevExpress.XtraReports.UI.DetailBand Detail3;
        private DevExpress.XtraReports.UI.XRCheckBox xrCheckBox5;
        private DevExpress.XtraReports.UI.XRCheckBox xrCheckBox3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel142;
        private DevExpress.XtraReports.UI.XRLabel xrLabel141;
        private DevExpress.XtraReports.UI.XRCheckBox xrCheckBox7;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel140;
        private DevExpress.XtraReports.UI.XRLabel xrLabel139;
        private DevExpress.XtraReports.UI.XRLabel xrLabel138;
        private DevExpress.XtraReports.UI.XRLabel xrLabel137;
        private DevExpress.XtraReports.UI.XRLine xrLine7;
        private DevExpress.XtraReports.UI.DetailReportBand Maklumat_Persekolahan;
        private DevExpress.XtraReports.UI.DetailBand Detail4;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader4;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRLine xrLine8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel144;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter2;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel145;
        private DevExpress.XtraReports.UI.XRLabel xrLabel146;
        private DevExpress.XtraReports.UI.XRLabel xrLabel147;
        private DevExpress.XtraReports.UI.XRLabel xrLabel148;
        private DevExpress.XtraReports.UI.XRLabel xrLabel149;
        private DevExpress.XtraReports.UI.XRLabel xrLabel150;
        private DevExpress.XtraReports.UI.XRLabel xrLabel151;
        private DevExpress.XtraReports.UI.XRLabel xrLabel152;
        private DevExpress.XtraReports.UI.XRLabel xrLabel153;
        private DevExpress.XtraReports.UI.XRLabel xrLabel154;
        private DevExpress.XtraReports.UI.XRLabel xrLabel155;
        private DevExpress.XtraReports.UI.DetailReportBand Keputusan_Peperiksaan;
        private DevExpress.XtraReports.UI.DetailBand Detail5;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader5;
        private DevExpress.XtraReports.UI.DetailReportBand Maklumat_Institusi_Pengajian;
        private DevExpress.XtraReports.UI.DetailBand Detail6;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel156;
        private DevExpress.XtraReports.UI.XRLine xrLine9;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.DetailReportBand Maklumat_Badan_Profesional;
        private DevExpress.XtraReports.UI.DetailBand Detail7;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel157;
        private DevExpress.XtraReports.UI.XRLine xrLine10;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.DetailReportBand Maklumat_KPSL;
        private DevExpress.XtraReports.UI.DetailBand Detail8;
        private DevExpress.XtraReports.UI.XRTable xrTable10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader8;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRLabel xrLabel158;
        private DevExpress.XtraReports.UI.XRLine xrLine11;
        private DevExpress.XtraReports.UI.DetailReportBand Maklumat_Sijil_Kemahiran;
        private DevExpress.XtraReports.UI.DetailBand Detail9;
        private DevExpress.XtraReports.UI.XRTable xrTable12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader9;
        private DevExpress.XtraReports.UI.XRTable xrTable11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRLabel xrLabel159;
        private DevExpress.XtraReports.UI.XRLine xrLine12;
        private DevExpress.XtraReports.UI.DetailReportBand Maklumat_Kemahiran_Komputer;
        private DevExpress.XtraReports.UI.DetailBand Detail10;
        private DevExpress.XtraReports.UI.XRTable xrTable14;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader10;
        private DevExpress.XtraReports.UI.XRTable xrTable13;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRLabel xrLabel160;
        private DevExpress.XtraReports.UI.XRLine xrLine13;
        private DevExpress.XtraReports.UI.DetailReportBand Maklumat_Kegiatan_Luar;
        private DevExpress.XtraReports.UI.DetailBand Detail11;
        private DevExpress.XtraReports.UI.XRTable xrTable16;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader11;
        private DevExpress.XtraReports.UI.XRLine xrLine14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel161;
        private DevExpress.XtraReports.UI.XRTable xrTable15;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.DetailReportBand Maklumat_Pekerjaan;
        private DevExpress.XtraReports.UI.DetailBand Detail12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel190;
        private DevExpress.XtraReports.UI.XRLabel xrLabel189;
        private DevExpress.XtraReports.UI.XRLabel xrLabel192;
        private DevExpress.XtraReports.UI.XRLabel xrLabel191;
        private DevExpress.XtraReports.UI.XRLabel xrLabel188;
        private DevExpress.XtraReports.UI.XRLabel xrLabel185;
        private DevExpress.XtraReports.UI.XRLabel xrLabel184;
        private DevExpress.XtraReports.UI.XRLabel xrLabel187;
        private DevExpress.XtraReports.UI.XRLabel xrLabel186;
        private DevExpress.XtraReports.UI.XRLabel xrLabel182;
        private DevExpress.XtraReports.UI.XRLabel xrLabel175;
        private DevExpress.XtraReports.UI.XRLabel xrLabel177;
        private DevExpress.XtraReports.UI.XRLabel xrLabel174;
        private DevExpress.XtraReports.UI.XRLabel xrLabel173;
        private DevExpress.XtraReports.UI.XRLabel xrLabel172;
        private DevExpress.XtraReports.UI.XRLabel xrLabel168;
        private DevExpress.XtraReports.UI.XRLabel xrLabel167;
        private DevExpress.XtraReports.UI.XRLabel xrLabel166;
        private DevExpress.XtraReports.UI.XRLabel xrLabel165;
        private DevExpress.XtraReports.UI.XRLabel xrLabel164;
        private DevExpress.XtraReports.UI.XRLabel xrLabel163;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader12;
        private DevExpress.XtraReports.UI.XRLine xrLine15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel162;
        private DevExpress.XtraReports.UI.XRLabel xrLabel208;
        private DevExpress.XtraReports.UI.XRLabel xrLabel209;
        private DevExpress.XtraReports.UI.XRLabel xrLabel210;
        private DevExpress.XtraReports.UI.XRLabel xrLabel205;
        private DevExpress.XtraReports.UI.XRLabel xrLabel206;
        private DevExpress.XtraReports.UI.XRLabel xrLabel207;
        private DevExpress.XtraReports.UI.XRLabel xrLabel204;
        private DevExpress.XtraReports.UI.XRLabel xrLabel203;
        private DevExpress.XtraReports.UI.XRLabel xrLabel202;
        private DevExpress.XtraReports.UI.XRLabel xrLabel199;
        private DevExpress.XtraReports.UI.XRLabel xrLabel198;
        private DevExpress.XtraReports.UI.XRLabel xrLabel201;
        private DevExpress.XtraReports.UI.XRLabel xrLabel200;
        private DevExpress.XtraReports.UI.XRLabel xrLabel197;
        private DevExpress.XtraReports.UI.XRLabel xrLabel194;
        private DevExpress.XtraReports.UI.XRLabel xrLabel193;
        private DevExpress.XtraReports.UI.XRLabel xrLabel196;
        private DevExpress.XtraReports.UI.XRLabel xrLabel195;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter4;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter5;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter6;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter7;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter8;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter9;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter10;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter11;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter12;
        private DevExpress.XtraReports.UI.DetailReportBand LainLain_Maklumat;
        private DevExpress.XtraReports.UI.DetailBand Detail13;
        private DevExpress.XtraReports.UI.XRCheckBox xrCheckBox11;
        private DevExpress.XtraReports.UI.XRCheckBox xrCheckBox10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel214;
        private DevExpress.XtraReports.UI.XRLabel xrLabel213;
        private DevExpress.XtraReports.UI.XRLabel xrLabel212;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader13;
        private DevExpress.XtraReports.UI.XRLine xrLine16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel211;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter13;
        private DevExpress.XtraReports.UI.DetailReportBand Rujukan;
        private DevExpress.XtraReports.UI.DetailBand Detail14;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader14;
        private DevExpress.XtraReports.UI.XRLine xrLine17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel225;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel261;
        private DevExpress.XtraReports.UI.XRLabel xrLabel260;
        private DevExpress.XtraReports.UI.XRLabel xrLabel259;
        private DevExpress.XtraReports.UI.XRLabel xrLabel256;
        private DevExpress.XtraReports.UI.XRLabel xrLabel257;
        private DevExpress.XtraReports.UI.XRLabel xrLabel258;
        private DevExpress.XtraReports.UI.XRLabel xrLabel253;
        private DevExpress.XtraReports.UI.XRLabel xrLabel254;
        private DevExpress.XtraReports.UI.XRLabel xrLabel255;
        private DevExpress.XtraReports.UI.XRLabel xrLabel252;
        private DevExpress.XtraReports.UI.XRLabel xrLabel251;
        private DevExpress.XtraReports.UI.XRLabel xrLabel250;
        private DevExpress.XtraReports.UI.XRLabel xrLabel234;
        private DevExpress.XtraReports.UI.XRLabel xrLabel233;
        private DevExpress.XtraReports.UI.XRLabel xrLabel232;
        private DevExpress.XtraReports.UI.XRLabel xrLabel249;
        private DevExpress.XtraReports.UI.XRLabel xrLabel248;
        private DevExpress.XtraReports.UI.XRLabel xrLabel247;
        private DevExpress.XtraReports.UI.XRLabel xrLabel231;
        private DevExpress.XtraReports.UI.XRLabel xrLabel230;
        private DevExpress.XtraReports.UI.XRLabel xrLabel228;
        private DevExpress.XtraReports.UI.XRLabel xrLabel237;
        private DevExpress.XtraReports.UI.XRLabel xrLabel236;
        private DevExpress.XtraReports.UI.XRLabel xrLabel229;
        private DevExpress.XtraReports.UI.XRLabel xrLabel226;
        private DevExpress.XtraReports.UI.XRLabel xrLabel227;
        private DevExpress.XtraReports.UI.XRLabel xrLabel235;
        private DevExpress.XtraReports.UI.DetailBand Detail15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel300;
        private DevExpress.XtraReports.UI.XRLabel xrLabel299;
        private DevExpress.XtraReports.UI.XRLabel xrLabel298;
        private DevExpress.XtraReports.UI.XRLabel xrLabel297;
        private DevExpress.XtraReports.UI.XRLabel xrLabel296;
        private DevExpress.XtraReports.UI.XRLabel xrLabel295;
        private DevExpress.XtraReports.UI.XRLabel xrLabel294;
        private DevExpress.XtraReports.UI.XRLabel xrLabel293;
        private DevExpress.XtraReports.UI.XRLabel xrLabel292;
        private DevExpress.XtraReports.UI.XRLabel xrLabel291;
        private DevExpress.XtraReports.UI.XRLabel xrLabel290;
        private DevExpress.XtraReports.UI.XRLabel xrLabel289;
        private DevExpress.XtraReports.UI.XRLabel xrLabel288;
        private DevExpress.XtraReports.UI.XRLabel xrLabel287;
        private DevExpress.XtraReports.UI.XRLabel xrLabel286;
        private DevExpress.XtraReports.UI.XRLabel xrLabel285;
        private DevExpress.XtraReports.UI.XRLabel xrLabel284;
        private DevExpress.XtraReports.UI.XRLabel xrLabel283;
        private DevExpress.XtraReports.UI.XRLabel xrLabel282;
        private DevExpress.XtraReports.UI.XRLabel xrLabel281;
        private DevExpress.XtraReports.UI.XRLabel xrLabel280;
        private DevExpress.XtraReports.UI.XRLabel xrLabel279;
        private DevExpress.XtraReports.UI.XRLabel xrLabel278;
        private DevExpress.XtraReports.UI.XRLabel xrLabel277;
        private DevExpress.XtraReports.UI.XRLabel xrLabel276;
        private DevExpress.XtraReports.UI.XRLabel xrLabel275;
        private DevExpress.XtraReports.UI.XRLabel xrLabel274;
        private DevExpress.XtraReports.UI.XRLabel xrLabel273;
        private DevExpress.XtraReports.UI.XRLabel xrLabel272;
        private DevExpress.XtraReports.UI.XRLabel xrLabel271;
        private DevExpress.XtraReports.UI.XRLabel xrLabel270;
        private DevExpress.XtraReports.UI.XRLabel xrLabel269;
        private DevExpress.XtraReports.UI.XRLabel xrLabel268;
        private DevExpress.XtraReports.UI.XRLabel xrLabel267;
        private DevExpress.XtraReports.UI.XRLabel xrLabel266;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader15;
        private DevExpress.XtraReports.UI.XRLine xrLine18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel265;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter15;
        private DevExpress.XtraReports.UI.XRTable xrTable17;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.DetailReportBand Maklumat_Kecacatan;
        private DevExpress.XtraReports.UI.DetailBand Detail16;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader16;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel305;
        private DevExpress.XtraReports.UI.XRLabel xrLabel306;
        private DevExpress.XtraReports.UI.XRLabel xrLabel301;
        private DevExpress.XtraReports.UI.XRCheckBox xrCheckBox22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRCheckBox xrCheckBox21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel319;
        private DevExpress.XtraReports.UI.XRLabel xrLabel318;
        private DevExpress.XtraReports.UI.XRLabel xrLabel317;
        private DevExpress.XtraReports.UI.XRLabel xrLabel316;
        private DevExpress.XtraReports.UI.XRLabel xrLabel315;
        private DevExpress.XtraReports.UI.XRLabel xrLabel314;
        private DevExpress.XtraReports.UI.XRLabel xrLabel313;
        private DevExpress.XtraReports.UI.XRLabel xrLabel312;
        private DevExpress.XtraReports.UI.XRCheckBox xrCheckBox27;
        private DevExpress.XtraReports.UI.XRLabel xrLabel311;
        private DevExpress.XtraReports.UI.XRCheckBox xrCheckBox26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel310;
        private DevExpress.XtraReports.UI.DetailReportBand Maklumat_Pasangan;
        private DevExpress.XtraReports.UI.DetailBand Detail17;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader17;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter17;
        private DevExpress.XtraReports.UI.CalculatedField FullAddress;
        private DevExpress.XtraReports.UI.CalculatedField OtherFullAddress;
        private DevExpress.XtraReports.UI.XRLabel xrLabel104;
        private DevExpress.XtraReports.UI.CalculatedField Tanda;
        private DevExpress.XtraReports.UI.XRLabel xrLabel106;
        private DevExpress.XtraReports.UI.CalculatedField NonSmoker;
        private RepDatasetTableAdapters.MaklumatWarisTableAdapter maklumatWarisTableAdapter;
        private RepDataset repDataset1;
        private RepDatasetTableAdapters.MaklumatPasanganTableAdapter maklumatPasanganTableAdapter;
        private RepDatasetTableAdapters.maklumatkecacatanTableAdapter maklumatkecacatanTableAdapter;
        private RepDatasetTableAdapters.pGetCVPhysicalHealthinfoTableAdapter pGetCVPhysicalHealthinfoTableAdapter;
        private RepDatasetTableAdapters.maklumatfizikalTableAdapter maklumatfizikalTableAdapter;
        private DevExpress.XtraReports.UI.CalculatedField TandaKecacatan;
        private RepDatasetTableAdapters.NoStaffbasedongrpTableAdapter noStaffbasedongrpTableAdapter;
        private RepDatasetTableAdapters.maklumatKemahiranBahasaTableAdapter maklumatKemahiranBahasaTableAdapter;
        private DevExpress.XtraReports.UI.CalculatedField BahasaLemah;
        private DevExpress.XtraReports.UI.CalculatedField BahasaSederhana;
        private DevExpress.XtraReports.UI.CalculatedField BahasaBaik;
        private RepDatasetTableAdapters.maklumatSekolahTableAdapter maklumatSekolahTableAdapter;
        private DevExpress.XtraReports.UI.CalculatedField TahunSekolah;
        private DevExpress.XtraReports.UI.XRLabel xrLabel124;
        private DevExpress.XtraReports.UI.XRLabel xrLabel123;
        private DevExpress.XtraReports.UI.XRLabel xrLabel125;
        private DevExpress.XtraReports.UI.XRLabel xrLabel127;
        private DevExpress.XtraReports.UI.XRLabel xrLabel126;
        private DevExpress.XtraReports.UI.XRLabel xrLabel119;
        private DevExpress.XtraReports.UI.XRLabel xrLabel112;
        private DevExpress.XtraReports.UI.XRLabel xrLabel120;
        private DevExpress.XtraReports.UI.XRLabel xrLabel122;
        private DevExpress.XtraReports.UI.XRLabel xrLabel121;
        private RepDatasetTableAdapters.maklumatIPTTableAdapter maklumatIPTTableAdapter;
        private RepDatasetTableAdapters.maklumatPeperiksaanTableAdapter maklumatPeperiksaanTableAdapter;
        private RepDatasetTableAdapters.maklumatBadanProTableAdapter maklumatBadanProTableAdapter;
        private RepDatasetTableAdapters.maklumatsijilkemahiranTableAdapter maklumatsijilkemahiranTableAdapter;
        private RepDatasetTableAdapters.maklumatKemahiranKomputerTableAdapter maklumatKemahiranKomputerTableAdapter;
        private RepDatasetTableAdapters.maklumatKegiatanLuarTableAdapter maklumatKegiatanLuarTableAdapter;
        private RepDatasetTableAdapters.maklumatHobiTableAdapter maklumatHobiTableAdapter;
        private DevExpress.XtraReports.UI.XRLabel xrLabel176;
        private DevExpress.XtraReports.UI.XRLabel xrLabel143;
        private RepDatasetTableAdapters.maklumatPekerjaanTableAdapter maklumatPekerjaanTableAdapter;
        private DevExpress.XtraReports.UI.XRLabel xrLabel170;
        private DevExpress.XtraReports.UI.XRLabel xrLabel169;
        private RepDatasetTableAdapters.LainlainMaklumatTableAdapter lainlainMaklumatTableAdapter;
        private DevExpress.XtraReports.UI.CalculatedField LainYa;
        private DevExpress.XtraReports.UI.CalculatedField LainTidak;
        private RepDatasetTableAdapters.maklumatrujukanTableAdapter maklumatrujukanTableAdapter;
        public DevExpress.XtraReports.UI.XRLabel XRLjawatandata;
        public DevExpress.XtraReports.UI.XRLabel XRLtarikhiklandata;
        public DevExpress.XtraReports.UI.XRLabel XRLjawatan2;
        public DevExpress.XtraReports.UI.XRLabel XRLtarikhiklan2;
        public DevExpress.XtraReports.UI.XRLabel XRLjawatan;
        public DevExpress.XtraReports.UI.XRLabel XRLtarikhiklan;
        public DevExpress.XtraReports.UI.DetailReportBand Pengakuan_Pemohon;
        private RepDatasetTableAdapters.pGetCVPersonalInfoTableAdapter pGetCVPersonalInfoTableAdapter;
    }
}
