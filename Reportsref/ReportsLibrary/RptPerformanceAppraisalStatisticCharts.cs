﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace ReportsLibrary
{
    public partial class RptPerformanceAppraisalStatisticCharts : DevExpress.XtraReports.UI.XtraReport
    {
        public RptPerformanceAppraisalStatisticCharts(string PrdApprsl, string TypMark, int lvl1, int lvl2, int lvl3, int lvl4, int lvl5, string DTAnalyse)
        {
            InitializeComponent();

            fGetPfmBranchHCTableAdapter.Connection.ConnectionString = RefLib.Repconnstr;
           
            fGetPfmBranchHCTableAdapter.Fill(repDataset1.fGetPfmBranchHC, PrdApprsl, TypMark, lvl1, lvl2, lvl3, lvl4, lvl5, DTAnalyse);
        }

    }
}
