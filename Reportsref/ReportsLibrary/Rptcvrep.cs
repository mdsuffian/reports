﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace ReportsLibrary
{
    public partial class Rptcvrep : DevExpress.XtraReports.UI.XtraReport
    {

        public void setcustomvisible(bool visibilitybool)
        {
            XRLtarikhiklan.Visible = visibilitybool;
            XRLtarikhiklan2.Visible = visibilitybool;
            XRLtarikhiklandata.Visible = visibilitybool;
            
            XRLjawatan.Visible = visibilitybool;
            XRLjawatan2.Visible = visibilitybool;
            XRLjawatandata.Visible = visibilitybool;

            Pengakuan_Pemohon.Visible = visibilitybool;
        }

        public Rptcvrep(string ESSDBNAME , string RefID)
        {
            InitializeComponent();

            pGetCVPersonalInfoTableAdapter.Connection.ConnectionString = RefLib.JobPortalconnstr;
            maklumatWarisTableAdapter.Connection.ConnectionString = RefLib.JobPortalconnstr;
            maklumatPasanganTableAdapter.Connection.ConnectionString = RefLib.JobPortalconnstr;
            pGetCVPhysicalHealthinfoTableAdapter.Connection.ConnectionString = RefLib.JobPortalconnstr;
            maklumatkecacatanTableAdapter.Connection.ConnectionString = RefLib.JobPortalconnstr;
            maklumatfizikalTableAdapter.Connection.ConnectionString = RefLib.JobPortalconnstr;
            maklumatKemahiranBahasaTableAdapter.Connection.ConnectionString = RefLib.JobPortalconnstr;
            maklumatSekolahTableAdapter.Connection.ConnectionString = RefLib.JobPortalconnstr;
            maklumatIPTTableAdapter.Connection.ConnectionString = RefLib.JobPortalconnstr;
            maklumatBadanProTableAdapter.Connection.ConnectionString = RefLib.JobPortalconnstr;
            maklumatKemahiranKomputerTableAdapter.Connection.ConnectionString = RefLib.JobPortalconnstr;
            maklumatKegiatanLuarTableAdapter.Connection.ConnectionString = RefLib.JobPortalconnstr;
            maklumatHobiTableAdapter.Connection.ConnectionString = RefLib.JobPortalconnstr;
            maklumatPekerjaanTableAdapter.Connection.ConnectionString = RefLib.JobPortalconnstr;
            lainlainMaklumatTableAdapter.Connection.ConnectionString = RefLib.JobPortalconnstr;
            maklumatrujukanTableAdapter.Connection.ConnectionString = RefLib.JobPortalconnstr;
            maklumatPeperiksaanTableAdapter.Connection.ConnectionString = RefLib.JobPortalconnstr;
            maklumatsijilkemahiranTableAdapter.Connection.ConnectionString = RefLib.JobPortalconnstr;

            pGetCVPersonalInfoTableAdapter.Fill(repDataset1.pGetCVPersonalInfo, ESSDBNAME, RefID ,RefLib.imghostpath );
            maklumatWarisTableAdapter.Fill(repDataset1.MaklumatWaris, ESSDBNAME, RefID);
            maklumatPasanganTableAdapter.Fill(repDataset1.MaklumatPasangan, ESSDBNAME, RefID);
            pGetCVPhysicalHealthinfoTableAdapter.Fill(repDataset1.pGetCVPhysicalHealthinfo, RefID);
            maklumatkecacatanTableAdapter.Fill(repDataset1.maklumatkecacatan, RefID);
            maklumatfizikalTableAdapter.Fill(repDataset1.maklumatfizikal, RefID);
            maklumatKemahiranBahasaTableAdapter.Fill(repDataset1.maklumatKemahiranBahasa, RefID);
            maklumatSekolahTableAdapter.Fill(repDataset1.maklumatSekolah, RefID);
            maklumatIPTTableAdapter.Fill(repDataset1.maklumatIPT, ESSDBNAME, RefID);
            maklumatBadanProTableAdapter.Fill(repDataset1.maklumatBadanPro, ESSDBNAME, RefID);
            maklumatKemahiranKomputerTableAdapter.Fill(repDataset1.maklumatKemahiranKomputer, RefID);
            maklumatKegiatanLuarTableAdapter.Fill(repDataset1.maklumatKegiatanLuar, RefID);
            maklumatHobiTableAdapter.Fill(repDataset1.maklumatHobi, RefID);
            maklumatPekerjaanTableAdapter.Fill(repDataset1.maklumatPekerjaan, RefID);
            lainlainMaklumatTableAdapter.Fill(repDataset1.LainlainMaklumat, RefID);
            maklumatrujukanTableAdapter.Fill(repDataset1.maklumatrujukan, RefID);
            maklumatPeperiksaanTableAdapter.Fill(repDataset1.maklumatPeperiksaan, RefID);
            maklumatsijilkemahiranTableAdapter.Fill(repDataset1.maklumatsijilkemahiran, RefID);

        }

        private void Rptcvrep_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

    }
}
