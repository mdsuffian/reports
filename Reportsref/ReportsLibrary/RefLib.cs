﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Web.Configuration;

namespace ReportsLibrary
{
    class RefLib
    {
        public static string  Repconnstr = ConfigurationManager.ConnectionStrings["MyXtraReport"].ToString();
        public static string ESSconnstr = ConfigurationManager.ConnectionStrings["MyESSConnection"].ToString();
        public static string JobPortalconnstr = ConfigurationManager.ConnectionStrings["MyJobApplyConnection"].ToString();

        public static string imghostpath = ConfigurationManager.AppSettings["JobportalPhoto"];
    }
}
