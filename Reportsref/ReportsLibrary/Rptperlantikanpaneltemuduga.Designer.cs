﻿namespace ReportsLibrary
{
    partial class Rptperlantikanpaneltemuduga
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Rptperlantikanpaneltemuduga));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.maklumatWarisTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.MaklumatWarisTableAdapter();
            this.pGetCVPhysicalHealthinfoTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.pGetCVPhysicalHealthinfoTableAdapter();
            this.maklumatKemahiranBahasaTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.maklumatKemahiranBahasaTableAdapter();
            this.maklumatSekolahTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.maklumatSekolahTableAdapter();
            this.maklumatPeperiksaanTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.maklumatPeperiksaanTableAdapter();
            this.maklumatIPTTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.maklumatIPTTableAdapter();
            this.maklumatBadanProTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.maklumatBadanProTableAdapter();
            this.noStaffbasedongrpTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.NoStaffbasedongrpTableAdapter();
            this.maklumatsijilkemahiranTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.maklumatsijilkemahiranTableAdapter();
            this.maklumatKemahiranKomputerTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.maklumatKemahiranKomputerTableAdapter();
            this.maklumatKegiatanLuarTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.maklumatKegiatanLuarTableAdapter();
            this.maklumatPekerjaanTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.maklumatPekerjaanTableAdapter();
            this.lainlainMaklumatTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.LainlainMaklumatTableAdapter();
            this.maklumatrujukanTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.maklumatrujukanTableAdapter();
            this.maklumatkecacatanTableAdapter = new ReportsLibrary.RepDatasetTableAdapters.maklumatkecacatanTableAdapter();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel4,
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel1,
            this.xrLabel5});
            this.Detail.HeightF = 608.2917F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(23.00004F, 460.4167F);
            this.xrLabel4.Multiline = true;
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(616.6666F, 98.91672F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.Text = "Pengarah,\r\nBahagian Pengurusan Sumber Manusia,\r\nJabatan Sumber Manusia,\r\nb.p Ketu" +
    "a Pegawai Eksekutif/Ketua Pengarah Hasil Dalam Negeri,\r\nLembaga Hasil Dalam Nege" +
    "ri Malaysia.\r\n";
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(22.99997F, 410.4167F);
            this.xrLabel3.Multiline = true;
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(605.8333F, 28.2084F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.Text = "[LATIFAH BINTI ZABIDI]";
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(22.99997F, 335.375F);
            this.xrLabel2.Multiline = true;
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(605.8333F, 62.58339F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "“BERKHIDMAT UNTUK NEGARA”\r\n“BERSAMA MEMBANGUNKAN NEGARA”\r\n“ORGANISASI BERIKTIRAF " +
    "MS ISO 9001:2008”\r\n";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(22.99997F, 10.00001F);
            this.xrLabel1.Multiline = true;
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(616.6666F, 308.125F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = resources.GetString("xrLabel1.Text");
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Italic);
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(22.99997F, 572.9167F);
            this.xrLabel5.Multiline = true;
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(616.6666F, 26.00006F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.Text = "s.k. Pengarah Jabatan Penyelidikan Percukaian";
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 24.99997F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(22.99995F, 106.0417F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(625.9583F, 12.08333F);
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(22.99995F, 83.04167F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(441.6666F, 23F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.Text = "LEMBAGA HASIL DALAM NEGERI MALAYSIA";
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(22.99995F, 0F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(100F, 72.70834F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 138F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // maklumatWarisTableAdapter
            // 
            this.maklumatWarisTableAdapter.ClearBeforeFill = true;
            // 
            // pGetCVPhysicalHealthinfoTableAdapter
            // 
            this.pGetCVPhysicalHealthinfoTableAdapter.ClearBeforeFill = true;
            // 
            // maklumatKemahiranBahasaTableAdapter
            // 
            this.maklumatKemahiranBahasaTableAdapter.ClearBeforeFill = true;
            // 
            // maklumatSekolahTableAdapter
            // 
            this.maklumatSekolahTableAdapter.ClearBeforeFill = true;
            // 
            // maklumatPeperiksaanTableAdapter
            // 
            this.maklumatPeperiksaanTableAdapter.ClearBeforeFill = true;
            // 
            // maklumatIPTTableAdapter
            // 
            this.maklumatIPTTableAdapter.ClearBeforeFill = true;
            // 
            // maklumatBadanProTableAdapter
            // 
            this.maklumatBadanProTableAdapter.ClearBeforeFill = true;
            // 
            // noStaffbasedongrpTableAdapter
            // 
            this.noStaffbasedongrpTableAdapter.ClearBeforeFill = true;
            // 
            // maklumatsijilkemahiranTableAdapter
            // 
            this.maklumatsijilkemahiranTableAdapter.ClearBeforeFill = true;
            // 
            // maklumatKemahiranKomputerTableAdapter
            // 
            this.maklumatKemahiranKomputerTableAdapter.ClearBeforeFill = true;
            // 
            // maklumatKegiatanLuarTableAdapter
            // 
            this.maklumatKegiatanLuarTableAdapter.ClearBeforeFill = true;
            // 
            // maklumatPekerjaanTableAdapter
            // 
            this.maklumatPekerjaanTableAdapter.ClearBeforeFill = true;
            // 
            // lainlainMaklumatTableAdapter
            // 
            this.lainlainMaklumatTableAdapter.ClearBeforeFill = true;
            // 
            // maklumatrujukanTableAdapter
            // 
            this.maklumatrujukanTableAdapter.ClearBeforeFill = true;
            // 
            // maklumatkecacatanTableAdapter
            // 
            this.maklumatkecacatanTableAdapter.ClearBeforeFill = true;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox1,
            this.xrLabel6,
            this.xrLine1});
            this.ReportHeader.HeightF = 118.125F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // Rptperlantikanpaneltemuduga
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.TopMargin,
            this.Detail,
            this.BottomMargin,
            this.ReportHeader});
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 25, 138);
            this.Version = "11.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private RepDatasetTableAdapters.MaklumatWarisTableAdapter maklumatWarisTableAdapter;
        private RepDatasetTableAdapters.pGetCVPhysicalHealthinfoTableAdapter pGetCVPhysicalHealthinfoTableAdapter;
        private RepDatasetTableAdapters.maklumatKemahiranBahasaTableAdapter maklumatKemahiranBahasaTableAdapter;
        private RepDatasetTableAdapters.maklumatSekolahTableAdapter maklumatSekolahTableAdapter;
        private RepDatasetTableAdapters.maklumatPeperiksaanTableAdapter maklumatPeperiksaanTableAdapter;
        private RepDatasetTableAdapters.maklumatIPTTableAdapter maklumatIPTTableAdapter;
        private RepDatasetTableAdapters.maklumatBadanProTableAdapter maklumatBadanProTableAdapter;
        private RepDatasetTableAdapters.NoStaffbasedongrpTableAdapter noStaffbasedongrpTableAdapter;
        private RepDatasetTableAdapters.maklumatsijilkemahiranTableAdapter maklumatsijilkemahiranTableAdapter;
        private RepDatasetTableAdapters.maklumatKemahiranKomputerTableAdapter maklumatKemahiranKomputerTableAdapter;
        private RepDatasetTableAdapters.maklumatKegiatanLuarTableAdapter maklumatKegiatanLuarTableAdapter;
        private RepDatasetTableAdapters.maklumatPekerjaanTableAdapter maklumatPekerjaanTableAdapter;
        private RepDatasetTableAdapters.LainlainMaklumatTableAdapter lainlainMaklumatTableAdapter;
        private RepDatasetTableAdapters.maklumatrujukanTableAdapter maklumatrujukanTableAdapter;
        private RepDatasetTableAdapters.maklumatkecacatanTableAdapter maklumatkecacatanTableAdapter;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
    }
}
