﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace ReportsLibrary
{
    public partial class rptPerformanceAppraisal : DevExpress.XtraReports.UI.XtraReport
    {
        public rptPerformanceAppraisal(string PrdApprsl, string TypMark, string DTAnalyse)
        {
            InitializeComponent();
            fGetPfmBrnStatTableAdapter.Connection.ConnectionString = RefLib.Repconnstr;
            fGetPfmBrnStatTableAdapter.Fill(repDataset1.fGetPfmBrnStat, PrdApprsl, TypMark, DTAnalyse);
            
        }

    }
}
