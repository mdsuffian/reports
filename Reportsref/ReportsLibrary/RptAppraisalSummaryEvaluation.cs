﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;


namespace ReportsLibrary
{
    public partial class RptAppraisalSummaryEvaluation : DevExpress.XtraReports.UI.XtraReport
    {
        public RptAppraisalSummaryEvaluation(string CdBranch, string PrdApprsl, string TypMark, string DTAnalyse)
        {
            InitializeComponent();


            fGetPfmDtlBrnTableAdapter.Connection.ConnectionString = RefLib.Repconnstr;
            fGetPfmDtlBrnTableAdapter.Fill(repDataset1.fGetPfmDtlBrn, CdBranch, PrdApprsl, TypMark, DTAnalyse);
        }

    }
}
